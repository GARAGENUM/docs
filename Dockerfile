FROM python:3.11-slim

WORKDIR /app

COPY . /app

RUN apt-get update && \
    apt-get install -y git && \
    git config --global --add safe.directory /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN mkdocs build

CMD ["python", "-m", "http.server", "--directory", "public", "5000"]
