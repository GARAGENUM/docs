# Utiliser MkDocs pour la documentation du Garage

MkDocs permet de générer la documentation du Garage à partir de simples fichiers Markdown.

Vous pouvez accéder au site à `https://garagenum.gitlab.io/doc/`

## Travailler en local

Pour participer à la documentation et tester vos modifications avant de les mettre en ligne, il vous faut installer mkdocs.

```
pip install mkdocs
pip install -r requirements.txt
mkdocs serve
```  

Open up `http://127.0.0.1:8000/` in your browser.  

## Ajouter du contenu

Chaque page de contenu correspond à un fichier markdown (.md)  

La documentation est divisée en sections, et en sous-sections.  

Pour chaque section / sous-section, il y a un fichier index.md pour lister et mettre des liens vers les différents contenus  de la section.  

Il faut aussi penser à ajouter le lien vers le nouveau fichier de contenu dans la partie `nav` de `mkdocs.yaml`   

## Astuces de mise en page  

### Insérer du code  

Utilisez la suite de caractères suivants ` ``` `  pour encadrer votre code:  

```  
#```  
Ceci est du   
code   
#```  
```   

Utilisez un seul **`** pour surligner un mot ou une phrase comme du code   

### Insérer une note  

Utilisez la syntaxe suivante   

```  
!!! note "Lisez bien cette note"   
    En indentant vos lignes à partir de cet tag `!!! note`,  
    vous obtenez un encadré dans le texte.   
```  
??? note "Vous pouvez en faire un bloc déroulable"   
    En indentant vos lignes à partir de cet tag `??? note`,  
    vous obtenez un bloc déroulable.  

Plus d'informations sur [la doc](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)

### Insérer un groupe d'onglets

#### Code

```
=== "Python"
    ```
    print(something)
    ```

=== "Bash"
    ```
    echo something
    ```
```

#### Résultat

=== "Python"
    ```python
    print(something)
    ```

=== "Bash"
    ```bash
    echo something
    ```

### Échapper des caractères spéciaux

Certains caractères peuvent être mal intêrprétés. Il faut parfois les échapper en les encadrant avec les directives `{% raw %}` et `{% endraw %}`.

### Aligner une image sur la droite

![image d'une flèche orientée vers la gauche](../../assets/divers/internal/mkdocs_image-left.png){ width="150", align=right }Grâce à l'extension `md_in_html`, vous pouvez ajouter des classes css à vos images pour en modifier l'affichage. Il suffit d'ajouter la classe entre accolades à la suite de l'image: `![description de mon image](chemin-de-mon-image.png){width="150", align=right}`