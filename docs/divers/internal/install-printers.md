# Installer l'imprimante du Garage

## Installer l'imprimante KONICA du Garage (petit local)

1. aller dans le navigateur sur [http://localhost:631](http://localhost:631)
2. Administration > Add printer
3. Saisir son identifiant sudoer et le mot de passe
4. Choisir le protocole AppSocket/HP JetDirect
5. Donner l'adresse socket://192.168.1.100
6. Fournir le driver ditribué par [openprinting](http://www.openprinting.org/ppd-o-matic.php?driver=Postscript-KONICA_MINOLTA&printer=KONICA_MINOLTA-bizhub_360&show=0)
7. Dans les réglages, mettre le format du papier par défaut à A4

## Installer l'imprimante Browser du Garage (petit local)

###  Step1. Download the tool (linux-brprinter-installer-*.*.*-*.gz)

1. Go to [Brother Website](https://support.brother.com/g/b/downloadend.aspx?c=fr&lang=fr&prod=mfcl2730dw_us_eu_as&os=128&dlid=dlf006893_000&flang=4&type3=625&dlang=true)
2. Accept Licence Contract:  
![CLUF](../../assets/divers/internal/cluf-brother.png)
3. The tool will be downloaded into the default "Download" directory.  
(The directory location varies depending on your Linux distribution.)  
e.g. /home/(LoginName)/Download

### Step2. Open a terminal window.

### Step3. Go to the directory you downloaded the file to in the last step.

``` 
cd Downloads
```

### Step4. Extract the downloaded file and delete the archive:

```
gunzip linux-brprinter-installer-*.gz && rm linux-brprinter-installer-*.gz 
```

### Step5. Run the tool:

```
sudo bash linux-brprinter-installer-* MFC-J880DW
```

### Step6. The driver installation will start. Follow the installation screen directions.

When you see the message "Will you specify the DeviceURI ?",

```
 For USB Users: Choose N(No)
 For Network Users: Choose Y(Yes) and DeviceURI number.
```

Mettre Yes.
Lorsque l'installateur demande l'URI de l'imprimante, choisir le mode "Auto" en tapant le code correspondant ("14" a priori). 

Lorsque l'installeur demainde l'IP de l'imprimante pour la paramétrage du scan, vérifier l'IP sur l'imprimante. Pour l'instant, l'imrpimante est en 192.168.1.210


The install process may take some time. Please wait until it is complete.


### Step7. Post-install
Par défaut, il semble que la taille du papier soit réglé sur A3. Impossible du coup d'imprimer. Régler le paper size sur A4 et ça marche.

# Installer les scanners

Assurez-vous d'être dans le groupe saned (client et serveur).

Pour tester un scanner, utilisez scanimage -L.

Pour partager un scanner en réseau, il suffit de modifier le serveur de la manière suivante:

!!! note "/etc/default/saned"
    ```
    RUN_AS_USER=saned
    RUN=yes
    ```

!!! note "/etc/sane.d/saned.conf"
    ```
    # List of clients authorized
    192.168.1.0/24
    ```

Activez-ensuite le socket avec :   
```
systemctl enable saned.socket
```

Sur le client, effectuez la modification suivante:  

!!! note "/etc/sane.d/net.conf"
    ```
    connect_timeout = 60
    # Server Adress
    192.168.1.100
    ```
    
Et voilà, le scanner est accessible depuis le client!
