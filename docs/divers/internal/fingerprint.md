![fingerprint](../../assets/divers/internal/fingerprint.png)

# Faire fonctionner le lecteur d'empreinte sur Debian

Par défaut, fprintd et libpam-fprintd sont les paquets faisant fonctionner les lecteurs d'empreintes sur Debian.
Le support est très limité, mais il faut déjà commencer par là
``` { .bash .copy }
sudo apt-get install fprintd libpam-fprintd
```
Pour enregistrer l'empreinte :
``` { .bash .copy }
fprintd-enroll
```
Si on a de la chance, on a le message
```
Enrolling right-index-finger finger.
```
Si on a pas de chance :
```
Impossible to enroll: GDBus.Error:net.reactivated.Fprint.Error.NoSuchDevice: No devices available
```

## Installation de python-validity
Python-validity est un projet pour élargir le support des lecteurs d'empreintes sur Linux. Pour l'installer sur Debian on passe par le PPA d'Ubuntu
``` { .bash .copy }
sudo add-apt-repository ppa:uunicorn/open-fprintd
```
Il va falloir modifier les dépôts, car ceux par défauts sont ceux de la toute dernière version d'Ubuntu  
``` { .bash .copy }
#  /etc/apt/sources.list.d/uunicorn-ubuntu-open-fprintd-mantic.list
deb http://ppa.launchpad.net/uunicorn/open-fprintd/ubuntu focal main
```
JE choisis "focal" parce que c'est la version LTS la plus proche de la version de Debian actuelle (Debian 11).  
Pour Debian 12, il faudrait sans doute mettre la version LTS la plus proche  
Pour en savoir plus sur les noms de code d'Ubuntu
[https://wiki.ubuntu.com/Releases](https://wiki.ubuntu.com/Releases)  
(le nom de code est le premier mot, ex: Focal Fossa : focal)

:warning: Keyserver fonctionnelle sur Debian 12.5 
``` { .bash .copy }
sudo apt update
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8D4C774BA6D18F90
apt update
sudo apt-get install open-fprintd fprintd-clients python3-validity
```
Une fois installé on réessaye
``` { .bash .copy }
fprintd-enroll
```
Il faudra appuyer son doigt contre le capteur et le relever, jusqu'au message 
```
Enroll result: enroll-completed
```
Si jamais l'erreur "No devices available" persiste, il n'y a quasiment aucune solution et il va falloir abandonner ce mode d'authentification.  

## Authentification pour sudo
Par défaut, seulement la session est déblocable avec l'empreinte, pour utiliser sudo
``` { .bash .copy }
pam-auth-update
```
Il faudra cocher "Fingerprint authentification" et valider
