# Utiliser l'API de Gitlab et Github

Ce guide permet d''utiliser l'API de Gitlab / Gitlab avec Bash pour récupérer la dernière version d'un projet.

## Récupérer la dernière version d'un projet Github

``` { .bash .copy }
curl -s https://api.github.com/repos/[author]/[repository]/releases/latest |\
grep "browser_download_url.*deb" |\
cut -d : -f 2,3 |\
tr -d \" |\
wget -qi -
```

## Récupérer la dernière version d'un projet Gitlab

> A faire
