# Keycloak configuration

## Customize theme

### Duplicate existing theme

Duplicate keycloak theme and rename it to `my-theme`.

Put the folder `my-theme` inside `themes` folder. I you are using Docker, you could simply bind a local folder to `/opt/jboss/keycloak/themes`.

### Change logos

Copy your logo `my-brand-logo.png` and your background `my-brand-bg.png` to the following destinations:  
  - `my-theme/welcome/resources/`  
  - `my-theme/login/resources/img/`  
  - `my-theme/admin/resources/img/`  
  - `my-theme/account/resources/img/`  

Put also a new `favicon.ico` in `my-teme/welcome/resources/`

Now modify html and css calls to catch your logo name:
```
find my-theme/ -type f -exec sed -i 's/keycloak-logo-text.png/my-brand-logo.png/g' {} +
find my-theme/ -type f -exec sed -i 's/keyclok-logo.png/my-brand-logo.png/g' {} +
find my-theme/ -type f -exec sed -i 's,/logo.png,/my-brand-logo.png,g' {} +
find my-theme/ -type f -exec sed -i 's,/bg.png,/my-theme-bg.png,g' {} +
find my-theme/ -type f -exec sed -i 's/keycloak-bg.png/my-theme-bg.png/g' {} +
```

If your logo doesn't appear totally in login page, you can change `width` and `height`
in `div.kc-logo-text` (file `my-theme/login/resources/css/login.css`).

If your logo is distorted in nav bar, you can change `background-size` 
in `.navbar-pf .navbar-brand` (file `admin/resources/css/styles.css` ). 

From that last `.navbar-pf .navbar-brand` bloc copy the values `height`, 
`width` and `background-size` and report them to `.navbar-title` 
in `account/resources/css/account.css`.

### Add terms and conditions

#### Enable in Keycloak

In Keycloak console admin, go to `Authentication > Required Actions` and enable `Terms and conditions`:

![A screenshot of the Required Actions tab in Authentication, with Terms and conditions enabled](../../../assets/divers/devops/sso/sso-keycloak-enable-terms.png)

#### Modify message template

If you want to modify actual page title (`Terms and Conditions`), you can modify the following:  
- `termsTitle`   
- `termsTitleHtml`  
- `termsText`  
- `termsPlainText`

in `${KEYCLOAK_THEMES}/base/login/messages/messages_fr.properties`.

#### Modify 'execute actions' email

This email is sent when administrator triggers it, asking for acoount / password / policy update from the user. 

You can use that trigger at registration, asking for Password Update, so user can access password reset form.  
Go in in `Ùsers --> Credentials --> Credential reset `: 

![Screenshot showing page for user password reset email sending](../../../assets/divers/devops/sso/sso-keycloak-askpwdreset.png)

This email is generated using template at `${KEYCLOAK_THEMES}/base/email/html/password-reset.ftl`.

The message content is in :  
- `executeActionsBodyHtmlSubject`  
- `executeActionsBodyHtmlBody`  
- `executeActionsBodyHtmlBodyHtml`
  
in `${KEYCLOAK_THEMES}/base/email/messages/messages_fr.properties` 
and `${KEYCLOAK_THEMES}/base/email/text/password-reset.ftl`

Copy those three files in your own theme directory and custom it as you want.

#### Modify after execution message

When user executes the actions asked by mail, he ends on a simple page without no link nor redirection.

You may want to add some links in template taken from `base/login/info.ftl`:

```
<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        <#if messageHeader??>
        ${messageHeader}
        <#else>
        ${message.summary}
        </#if>
    <#elseif section = "form">
      <div id="kc-info-message">
        <p class="instruction">${message.summary}<#if requiredActions??><#list requiredActions>: <b><#items as reqActionItem>${msg("requiredAction.${reqActionItem}")}<#sep>, </#items></b></#list><#else></#if></$
        <#if skipLink??>
                <p><a href="https://id.pnpro.paris/account">Cliquez ici pour gérer votre compte <strong>Keycloak Skiplink</strong></a></p>
                <p><a href="https://nextcloud.pnpro.paris">Cliquez ici pour accéder à <strong>Nextcloud</strong></a></p>
                <p><a href="https://chat.pnpro.paris">Cliquez ici pour accéder à <strong>Mattermost</strong></a></p>
        <#else>
            <#if pageRedirectUri?has_content>
                <p><a href="${pageRedirectUri}">${kcSanitize(msg("backToApplication"))?no_esc}</a></p>
            <#elseif actionUri?has_content>
                <p><a href="${actionUri}">${kcSanitize(msg("proceedWithAction"))?no_esc}</a></p>
            <#elseif (client.baseUrl)?has_content>
                <p><a href="${client.baseUrl}">${kcSanitize(msg("backToApplication"))?no_esc}</a></p>
            </#if>
         </#if>
       </div>
    </#if>
</@layout.registrationLayout>
```

#### Define available languages

For each sub-theme you need to define which locales are implemented (translated), 
with a comma-separated list of supported locales in `${KEYCLOAK_THEMES}/${SUBTHEME}/theme.properties` like this:
```
locales=fr,en
```
