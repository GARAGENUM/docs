# Configure OpenLDAP

We will just need a very basic configuration of Ldap, as Keycloak will 
populate it.

You can use docker-compose to mount an OpenLdap server basically populated at startup.

## Install OpenLdap with docker-compose

Here's the necessary `docker-compose.yml` file, deploying OpenLdap server along phpldapmyadmin web interface:

```
version: '3'
services:
  openldap:
      image: osixia/openldap
      volumes:
        - ./admin.ldif:/container/service/slapd/assets/config/bootstrap/ldif/admin.ldif
      #args: [ "--copy-service" ]
      command: "/bin/sh -c '/container/tool/run --copy-service --loglevel debug'"
      env_file:
        - .env
      tty: true
      stdin_open: true
      # For replication to work correctly, domainname and hostname must be
      # set correctly so that "hostname"."domainname" equates to the
      # fully-qualified domain name for the host.
      domainname: "pnpro.paris"
      hostname: "ldap"
  phpldapadmin:
      image: osixia/phpldapadmin
      ports:
        - 127.0.0.1:8092:80
      depends_on:
        - openldap
      environment:
        PHPLDAPADMIN_LDAP_HOSTS: "openldap"
        PHPLDAPADMIN_HTTPS: "false"
```

You will need to create a `.env` file with following elements (replace the variables):
```
LDAP_ORGANIZATION=${ORGANIZATION}
LDAP_DOMAIN=domain.org
LDAP_ADMIN_PASSWORD=${LDAP_ADMIN_PASSWORD}
LDAP_CONFIG_PASSWORD=${LDAP_CONFIG_PASSWORD}  
```

## Add a custom `.ldif` file

We can use the following `admin.ldif` file, considering that root cn already exists:

```
dn: cn=admin, dc=domain, dc=org
changetype: modify
replace: o
o: Complete Organization Name

dn: ou=people, dc=domain, dc=org
changetype: add
objectClass: organizationalUnit
ou: people

dn: uid=superadmin, ou=people, dc=domain, dc=org
changetype: add
objectClass: inetOrgPerson
uid: superadmin
cn: Super
sn: Admin
mail: contact@domain.org
employeeNumber: 1

dn: ou=groups, dc=domain, dc=org
changetype: add
objectClass: organizationalUnit
ou: groups


dn: cn=admin, ou=groups, dc=domain, dc=org
changetype: add
objectClass: groupofNames
cn: admin
ou: Admin Group
member: uid=superadmin, ou=people, dc=domain, dc=org

```

The file, binded to `/container/service/slapd/assets/config/bootstrap/ldif/admin.ldif`
will be used when running server within docker-compose via command `/container/tool/run --copy-service`. 

## Deploy the stack

Run `docker-compose up -d` to deploy the server.

Access phpmyadmin at `http://127.0.0.1:8092` and connect with user 
`cn=admin, dn=domain, dn=org` and password set in `env`.

## Create domain under master

You don't want to work in master, instead, you want to keep master as a top-level Realm and create a realm inside 
for your organisation.

Click on master in the upper-left, then `Add Realm`.

## Set keycloak to federate users from openldap server

### Create new User Federation

Go to your newly created realm (not master), and click on `Configure > User Federation`, then `Add provider`

![screenshot showing user federation creation](../../../assets/divers/devops/sso/create-user-federation.png)

### Configure Openldap user federation

#### Basic settings

Use following settings for User federation :  

![screenshot showing basic settings for user federation creation](../../../assets/divers/devops/sso/basic-settings-user-federation.png)

#### Add mapper for group

Go to `Mappers` tab, and click on `Create` on the upper-right.

![screenshot showing mapper settings for group mapping](../../../assets/divers/devops/sso/group-mapper-settings-user-federation.png)