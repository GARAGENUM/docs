# Introduction

This guide will cover the configuration of Keycloak as SSO for different services, like Nextcloud or Mattermost.

The list of users users and groups is mapped with a LDAP server.

We assume you already have a reachable LDAP server. 

```mermaid
graph LR;
    A(LDAP server)<-->B(Keycloak SSO);
    B(Keycloak SSO)<-->A(LDAP server);
    B(Keycloak SSO)<-->C(Nextcloud);
    B(Keycloak SSO)<-->D(Mattermost);
    B(Keycloak SSO)<-->E(Wordpress);
```

The sequences happens this way, using Nextcloud as an example.

First, Nextcloud redirects login request to Keycloak, and keycloak checks 
in his internal database if user exists and is connected to this client.

```mermaid
sequenceDiagram;
    participant L as LDAP;
    participant N as Nextcloud;
    participant K  as Keycloak;
    participant U as User;
    activate U;
    U->>N: Login;
    Note over U,N: User asks for Nextcloud <br/> login page url;
    activate N;
    N->>K: Redirect Login Page;
    Note over N,K: Nextcloud redirects to <br/> Keycloak login page;
    activate K;
    K->>U: Serve Login Page;
    U->>K: Enter credentials;
    Note over K,U: User enters credentials <br/> in Keycloak login page;
    loop Internal; 
        Note left of K: Check internal Db <br/> if user exists <br/> and is linked <br/> to this client;
    end;
    deactivate K;
    deactivate N;
```

If user exists in Keycloak database, keycloak updates Ldap data and let 
Ldap Server in charge for password authentication.

```mermaid
sequenceDiagram;
    participant L as LDAP;
    participant N as Nextcloud;
    participant K  as Keycloak;
    participant U as User;
    activate U;
    activate K;
    activate N;
    K->>L: Final Auth;
    Note over K,L: Keycloak delegates password <br/> authentication to Ldap Server
    activate L;
    alt auth succed;
        L->>K: success;
        Note over L,K: Ldap tells Keycloak that auth succeeded;
        K->>L: update;
        Note over L,K: Keycloak updates groups and user atribute in Ldap;
        deactivate L;
        K->>N: user creation;
        Note over N,K: Keycloak creates user <br/> in Nextcloud if not exists;
        K->>N: authentication;
        Note over K,N: Keycloak authenticates <br/> User in Nextcloud;
        N->>U: access;
        Note over N,U: Nextcloud is accessible by User
    else auth failed;
        K->>U: Back to login page;
    end;
    deactivate K;
    deactivate N;
```

