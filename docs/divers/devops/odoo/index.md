# Guide de développement pour Odoo

- [ Mettre en place son environnement de développement](devenv.md)
- [Développer un module](devmodule.md)