# Mettre en place son environnement de développement Odoo

## Dossier de travail

### Dossiers

Créer les dossiers suivants:
```
mkdir addons etc postgresql
```

### Configuration d'odoo

Créer le fichier `etc/odoo.conf`
??? note "etc/odoo.conf"
    ```
    [options]
    ; This is the password that allows database operations:
    admin_passwd = Mot_de_passe_pour_protéger_la_base_de_données
    db_host = db
    db_port = 5432
    db_user = odoo
    db_password = odoo
    addons_path = /mnt/extra-addons
    without_demo = True
    proxy_mode = True
    max_cron_threads = 1
    #db_filter = ^odoo$
    #list_db = False
    ```


### Docker-compose

Créer le fichier `docker-compose.yml`:

??? note "docker-compose.yml"
    ```
    version: '2'
    services:
    db:
        image: postgres:13
        user: root
        environment:
        - POSTGRES_PASSWORD=odoo
        - POSTGRES_USER=odoo
        - POSTGRES_DB=postgres
        restart: always             # run as a service
        #volumes:
        #    - ./postgresql:/var/lib/postgresql/data

    odoo16:
        image: odoo:16
        user: root
        depends_on:
        - db
        ports:
        - "10014:8069"
        - "20014:8072" # live chat
        tty: true
        command: --
        #    command: odoo scaffold /mnt/extra-addons/custom_module
        environment:
        - HOST=db
        - USER=odoo
        - PASSWORD=odoo
        volumes:
        #- /etc/timezone:/etc/timezone:ro
        #- /etc/localtime:/etc/localtime:ro
        # - ./entrypoint.sh:/entrypoint.sh   # if you want to install additional Python packages, uncomment this line!
        - ./addons:/mnt/extra-addons
        - ./etc:/etc/odoo
        restart: always             # run as a service
    ```

## Configure vscode

### Install vscode extensions

Go to extensions views by clicking on the square icon in the sidebar or pressing ++ctrl+shift+x++ .

1. Search for "Docker" and install the one published by Microsoft.
2. Search for "Json" and install the one published by ZainChen.

### Configure task

??? note ".vscode/tasks.json"
    ```
    {
        "version": "2.0.0",
        "tasks": [
            {
                "label": "docker-compose up",
                "type": "shell",
                "command": "docker-compose up",
                "problemMatcher": [],
                "presentation": {
                    "reveal": "always",
                    "panel": "new"
                }
            }
        ]
    }
    ```

## Finalize installation

### Access

You can now deploy odoo by clicking on Terminal > Run Task > docker-compose up.

Your instance is now available at http://localhost:10014

### Configure

First time, you are redirected to database configuration.

You need to use the master password defined in `etc/odoo.conf`

![ScreenShot for first access to odoo instance, with a form for db configuration](../../../assets/divers/devops/odoo/odoo-dbconfig.png)

Vous pouvez maintenant décommenter les 2 dernières lignes du fichier __odoo.conf__, et redémarrer votre instance avec `docker-compose restart`.

### Installation des modules

Une fois les modules communautaires téléchargés dans le dossier des addons, il faut entrer dans le conteneur pour lancer l'installation des modules:  
```
user@host:~$ docker-compose exec -u odoo web bash
odoo@odoo-container:/$ find /mnt/extra-addons/ -mindepth 1 -maxdepth 1 -type d -printf "%f," | sed -E 's/(.*),/\1/' |xargs odoo -d odoo -i
```