# Dokku

[Dokku](http://dokku.viewdocs.io/dokku/) is an open-source Heroku-like, that helps you manage and deploy web applications.

## Installation

As of debian user we can install with apt

``` { .bash .copy }
# install prerequisites
sudo apt update -qq >/dev/null
sudo apt install -qq -y apt-transport-https

# install docker
wget -nv -O - https://get.docker.com/ | sh

# install dokku
wget -nv -O - https://packagecloud.io/dokku/dokku/gpgkey | sudo apt-key add -
OS_ID="$(lsb_release -cs 2>/dev/null || echo "bionic")"
echo "xenial bionic focal" | grep -q "$OS_ID" || OS_ID="bionic"
echo "deb https://packagecloud.io/dokku/dokku/ubuntu/ ${OS_ID} main" | sudo tee /etc/apt/sources.list.d/dokku.list
sudo apt update -qq >/dev/null
sudo apt install -qq -y dokku
sudo dokku plugin:install-dependencies --core
```

When asked, you can accept web page front end installation.

### Configure your host

#### Using a regular host on the web

If you are using a domain provider, make sure to create an A record in your DNS table,
pointing to your server's Ip.

#### Using localhost
If you are deploying dokker on your local machine (for testing purpose), you will need to modify `/etc/hosts` AND use `dnsmasq` ( Replace `<dokku.local>` by the domain name of your choice).  

1. modify first line of `/etc/hosts` :   
    ```
    127.0.0.1   localhost   <dokku.local>
    ```   
2. `sudo apt install dnsmasq`  
3. `sudo systemctl enable dnsmasq`  
4. Add `address=/<dokku.local>/127.0.0.1` in `/etc/dnsmasq.conf` 
5. Reboot your computer


### Configure ssh

Configure a public/private key pair for ssh root access.  
Run following commands as root:  
1. As root, run `ssh-keygen`  
2. Copy the key to authorized keys for dokku:  
    ``` { .bash .copy }
    cat ~/.ssh/id_rsa.pub  |sshcommand acl-add dokku superuser
    ```  
3. In setup webpage at `http://<dokku.local>`, paste content from  `/root/.ssh/id_rsa.pub` (only the part with `ssh-rsa`).  

![Screenshot of dokku setup webpage](../../assets/divers/devops/dokku-setup.png)

## Create app

``` { .bash .copy }
git clone https://github.com/heroku/python-getting-started
dokku apps:create python-getting-started
dokku postgres:create djangodatabase
dokku postgres:link djangodatabase python-getting-started
cd python-getting-started/
git checkout -b master
git remote add dokku dokku@dokku.talkie.social:django.dokku.talkie.social
sudo git push dokku master
```

## Wildcard certificates for an app

If you want to deploy an app to a specific domain and all subdomains, you will need to run certbot manually.

### Creation of the certificate

```
admin@web-hosting:~$ sudo certbot certonly --manual --preferred-challenges=dns
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Please enter in your domain name(s) (comma and/or space separated)  (Enter 'c'
to cancel): *.myappdomain.fr myappdomain.fr
Obtaining a new certificate
Performing the following challenges:
dns-01 challenge for myappdomain.fr

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
NOTE: The IP of this machine will be publicly logged as having requested this
certificate. If you're running certbot in manual mode on a machine that is not
your server, please ensure you're okay with that.

Are you OK with your IP being logged?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: Y

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please deploy a DNS TXT record under the name
_acme-challenge.myappdomain.fr with the following value:

_N15nw-EdbpNgrj_lYeH5UL61IL9MBwrEUZ0x9_4FXU

Before continuing, verify the record is deployed.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue
Waiting for verification...
Cleaning up challenges

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/myappdomain.fr/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/myappdomain.fr/privkey.pem
   Your cert will expire on 2024-02-27. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

### Integration in dokku

```
admin@web-hosting:~$ sudo su
root@web-hosting:/home/admin# cd /home/dokku/.ssl-certs/myappdomain.fr/
root@web-hosting:/home/dokku/.ssl-certs/myappdomain.fr/# cp /etc/letsencrypt/live/myappdomain.fr/fullchain.pem server.crt
root@web-hosting:/home/dokku/.ssl-certs/myappdomain.fr/# ls
server.crt
root@web-hosting:/home/dokku/.ssl-certs/myappdomain.fr/# cp /etc/letsencrypt/live/myappdomain.fr/privkey.pem server.key
root@web-hosting:/home/dokku/.ssl-certs/myappdomain.fr/# tar cvf certs.tar server.key server.crt
server.key
server.crt
root@web-hosting:/home/dokku/.ssl-certs/myappdomain.fr/# dokku certs:add myappdomain.fr < certs.tar 
server.key
server.crt
-----> Unsetting DOKKU_PROXY_PORT
-----> Setting config vars
       DOKKU_PROXY_PORT_MAP:  http:80:5000
-----> Setting config vars
       DOKKU_PROXY_PORT_MAP:  http:80:5000 https:443:5000
-----> Overriding default nginx.conf with detected nginx.conf.sigil
-----> Configuring myappdomain.fr...(using app-supplied template)
-----> Creating https nginx.conf
       Enabling HSTS
       Reloading nginx
```