# Déployer son site web automatiquement sur les serveurs du Garage

## Mise en ligne de son site

#### Se rendre à l'adresse correspondant au type de projet :
- [Pour déployer un site web](https://git.legaragenumerique.fr/GARAGENUM/web-deploy)
- [pour déployer un site hugo](https://git.legaragenumerique.fr/GARAGENUM/hugo-deploy)

#### Cliquer sur `Utiliser ce modèle`.

![illustration1](../../assets/divers/devops/cicd1.png)

#### Choisir un nom de dépôt et, dans les `éléments du modèle`, cocher `Contenu Git (branche par défaut)`.

![illustration1](../../assets/divers/devops/cicd2.png)

:warning: Bien choisir le nom de votre dépôt, car celui-ci figurera dans le nom de domaine pour accéder au site déployé.  

:skull: les majuscules et les caractères spéciaux rendront le projet non déployable (exemple: mon_projet).

#### Cliquer sur `Créer un dépôt`.

#### Une fois le dépôt créé, le cloner sur votre machine locale :
``` { .bash .copy }
git clone git@git.legaragenumerique.fr:gitea_username/nom_du_projet.git
```

> Le git clone via SSH nécessite d'ajouter sa clé SSH sur le serveur GIT, sinon on peux le cloner en utilisant le lien HTTPS

#### Ajouter votre contenu en respectant les structure décrite ci-dessous:

!!! note info "Pour déployer"
    === "un site web"
        ```bash
        ├── Dockerfile
        ├── html
        │   ├── content
        │   │   ├── page1.html
        │   │   ├── page2.html
        │   │   └── page3.html
        │   ├── css
        │   │   └── styles.css
        │   └── index.html
        └── README.md
        ```  
    === "un site hugo"
        ```bash
        ├── Dockerfile
        ├── hugo
        │   ├── content
        │   │   ├── page1.html
        │   │   ├── page2.html
        │   │   └── page3.html
        │   └── config.toml
        └── README.md
        ```

#### créer la branch deploy et la pousser:
``` { .bash .copy }
git checkout -b deploy
git push -u origin deploy
```

!!! tip "Le site est mis en ligne à l'adresse gitea_username-nom_du_projet.legaragenumerique.xyz"

## Mettre à jour son site

- Une fois les modifications éffectuée sur vos fichiers, simplement pousser sur le serveur:
``` { .bash .copy }
git add * 
git commit -m "maj fichier example.html"
git push
```

:bulb: S'assurer d'être bien sur la branche `deploy`

## Retirer son site

#### changer de branche (ne plus être sur la branche deploy):
``` { .bash .copy }
git checkout main
```

#### supprimer la branch `deploy` locale:
``` { .bash .copy }
git branch -d deploy
```

#### supprimer la branch `deploy` distante:
``` { .bash .copy }
git push --delete origin deploy
```

!!! tip "Le site n'est plus en ligne"