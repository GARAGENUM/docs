# Guides Devops

- [Utiliser Hugo, un Générateur de Sites Statiques (SGG) en Go](hugo/index.md)
- [Keycloak as SSO for Nextcloud](sso/index.md)
- [Utiliser Gitpod avec Gitlab](gitpod.md)
- [Utiliser Dokku pour déployer des applis en mode PaaS](dokku.md)
- [Utiliser l'API de Gitlab / Github avec Bash](git-api.md)
- [Développer des modules pour Odoo](odoo/index.md)
- [Déployer son site web / hugo via le garage CICD](cicd.md)
