
## Installer les dépendances

```
sudo apt update
sudo apt install curl wget git
```

## Installer hugo

### Download the last version

```
curl -s https://api.github.com/repos/gohugoio/hugo/releases/latest \
 | grep  browser_download_url \
 | grep linux-amd64.deb \
 | grep extended \
 | cut -d '"' -f 4 \
 | wget -i -  
```

### Install it
```
sudo dpkg -i hugo*_linux-amd64.deb
```

## Run hugo

```
hugo --help
```