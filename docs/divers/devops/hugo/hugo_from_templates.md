# Déployer un site web statique avec Hugo

## Installer Hugo

Utilisez le tutoriel de la même rubrique: [installer hugo](install_hugo.md)

## Création du projet sur Gitlab

### Création d'un nouveau projet

![New Project](../../../assets/divers/devops/hugo/new-project.png)

### Choix du template Hugo

![Choose Template](../../../assets/divers/devops/hugo/choose-hugo-template.png)


-----------------------------


## Installation de l'environnement de travail

### Installer les dépendances

```
sudo apt update
sudo apt install curl wget git
```

### Cloner le projet 


```
git clone https://user.gitlab.io/my-website.git
cd my-website
```

### Installer hugo

#### Download the last version

```
curl -s https://api.github.com/repos/gohugoio/hugo/releases/latest \
 | grep  browser_download_url \
 | grep linux-amd64.deb \
 | grep extended \
 | cut -d '"' -f 4 \
 | wget -i -  
```

#### Install it
```
sudo dpkg -i hugo*_linux-amd64.deb
```

### Run Hugo
```
hugo serve
```
The following results indicates that it works
```
user@domani:~/my-website$ hugo serve
Start building sites … 
WARN 2020/09/30 22:21:44 Page.URL is deprecated and will be removed in a future release. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
WARN 2020/09/30 22:21:44 Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.

                   | EN  
-------------------+-----
  Pages            | 35  
  Paginator pages  |  1  
  Non-page files   |  0  
  Static files     | 23  
  Processed images |  0  
  Aliases          |  9  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 99 ms
Watching for changes in /home/user/my-website/{content,static,themes}
Watching for config changes in /home/user/my-website/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/hugo/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Comme indiqué, allez voir à l'adresse `http://localhost:1313/hugo/` votre site web.

Appuyez ensuite sur ++ctrl+c++ pour stopper le serveur

------------------------------------

## Modifications 

### Le fichier `config.toml`

Ce fichier contient toute la configuration du site déployé par Hugo: les plugins utilisés, l'organisation des dossiers, l'url, la navigation dans le site, etc.

Modifiez au moins les éléments suivants:
```
baseurl = "https://garagenum.gitlab.io/web/my-website"
title = "H.E.E.I."
DefaultContentLanguage = "fr"
  subtitle = "Handicap Enfance Et Insertion"
```

!!! important "Respectez bien l'indentation !"

### Le dossier des images

La configuration dans `config.toml` nous indique les chemins relatifs pour le logo

```
  logo = "img/avatar-icon.png"
  favicon = "img/favicon.ico"
```

Nous devons donc créer le répertoire `img`, à partir du dossier `static` par défaut:
```
mkdir -p static/img
```

et y mettre les fichiers `avatar-icon.png` et `favicon.png`

----------------------------------

## Mettre en ligne

### Mettre en ligne avec gitlab

Il vous suffit de faire un  `commit` pour intégrer vos changements, puis de `push`vers la branche master.
Le fichier `gitlab-ci.yml` se charge en quelques minutes de mettre en ligne votre site à `https://user.gitlab.io/my-website`

### Mettre en ligne sur votre propre serveur

Il vous suffit de lancer la commande `hugo` sans aucune option pour générer les fichiers statiques:

```
user@domani:~/my-website$ hugo
Start building sites … 
WARN 2020/09/30 23:33:56 Page.URL is deprecated and will be removed in a future release. Use .Permalink or .RelPermalink. If what you want is the front matter URL value, use .Params.url
WARN 2020/09/30 23:33:56 Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.

                   | FR  
-------------------+-----
  Pages            | 35  
  Paginator pages  |  1  
  Non-page files   |  0  
  Static files     | 23  
  Processed images |  0  
  Aliases          |  9  
  Sitemaps         |  1  
  Cleaned          |  0  

Total in 144 ms
```
Récupérez le contenu du dossier `public` et faites le servir par n'importe quel serveur web (Apache, Nginx, etc.)

```
user@domani:~/my-website$ ls public/ -l
total 96
-rw-r--r--  1 user user 13067 sept. 30 23:33 404.html
drwxr-xr-x  2 user user  4096 sept. 30 23:33 categories
drwxr-xr-x  2 user user  4096 sept. 30 22:17 css
-rw-r--r--  1 user user 15086 sept. 30 22:17 favicon.ico
drwxr-xr-x  2 user user  4096 sept. 30 23:00 img
-rw-r--r--  1 user user 15773 sept. 30 23:33 index.html
-rw-r--r--  1 user user 15569 sept. 30 23:33 index.xml
drwxr-xr-x  2 user user  4096 sept. 30 22:17 js
drwxr-xr-x  5 user user  4096 sept. 30 23:33 page
drwxr-xr-x 14 user user  4096 sept. 30 23:33 post
-rw-r--r--  1 user user  3307 sept. 30 23:33 sitemap.xml
drwxr-xr-x  8 user user  4096 sept. 30 23:33 tags    
```

------------------------------

## Modifications avancées

### Changer de thème

Vous pouvez trouver de nombreux thèmes, dont certains gratuits, à `https://themes.gohugo.io/`

Il vous faut alors télécharger le plugin dans le dossier `themes`

```
git submodule add https://github.com/janraasch/hugo-scroll.git themes/hugo-scroll
```

Vous trouverez généralement dans le theme un dossier `exampleSite`, dans lequel il y a un fichier `config.toml` dont vous pourrez vous inspirer.

Le paramètre essentiel dans `config.toml` est le nom du thème, par exemple:     
```
theme = "hugo-scroll"
```


