# Workflow pour Hugo avec Dokku

Ce tutoriel présente un workflow complet pour développer un site Web avec Hugo,  
en isolant:  
  - le développement local (avec dokku)  
  - le déploiement du site en développement (avec Gitlab Pages)  
  - le déploiement sur un serveur multi-sites (avec dokku)  

|   | local | dev | prod |
|------|--------|-------|-------|
| deploy tool | dokku  | gitlab Pages | dokku |
| deploy Url  | localhost | gitlab.io | my.server |
| root project | project/public | project | project/public |

## Préparation dans Gitlab

Créez deux projets:  
  - `<my-project>` : contient le projet hugo  
  - `<my-project.public>` : submodule du 1er, contient le site généré.  

Le projet appelé <my-project> sera déployé avec Gitlab Pages.  
Pour choisir le nom du projet en prenant en compte la manière dont Gitlab construit l'Url de déploiement, 
[consultez la documentation](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html)


## Déploiement local

### Mise en place du projet

#### Clonez le projet Hugo

Si vous n'avez pas encore de projet Hugo, vous pouvez vous reporter au [guide de démarrage](create_website.md)

Choisissez de remplacer `<appname>` par le nom que vous voudrez voir apparaître en première partie de 
votre nom de domaine.    
Par exemple, si vous remplacez `<appname>` par `moncv`, votre projet sera déployé sur `moncv.domain.org`

```
git clone https://gitlab.com/<path/to/my-project> <appname>
cd <appname>
```

#### Créez la branche `local`
```
git checkout -b local
```

#### Modifiez `config.toml`
```
baseurl = "http://<appname>.domain.local>
```

### Déploiement

Pour le déploiement local vous avez deux options :  
- [Option 1: déploiement avec Hugo](../deploy_website/#deploiement-local)  
- [Option 2: déploiement avec Dokku (recommandé)](#deploiement-avec-dokku)

#### Déploiement avec Dokku

##### Installation de Dokku

Suivez [ce guide](../dokku.md) et n'oubliez pas de [configurer le DNS](../dokku/#using_localhost).  


##### Appelez le buildpack Hugo

Créez le fichier `.buildpacks` à la racine du projet

!!! note ".buildpacks"
    https://github.com/garagenum/heroku-buildpack-hugo

##### Créez une appli dokku pour le 1er déploiement
```
dokku apps:create <tmp-appname>
```

##### Déployez avec dokku
```
git remote add dokku dokku@<domain.local>:<tmp-appname>
git add config.toml .buildpacks 
git commit -m "<add local deployment>"
sudo git push dokku local:master
```

Vous pouvez voir votre site web à l'url `http://<appname.domain.local>/public`

## Déploiement du site de dev sur le web avec Gitlab Pages

Gitlab Pages va nous permettre de mettre en ligne un site en développement, uniquement accessible avec authentification, pour le montrer aux points d'étape.

### Mise en place du projet

#### Créez la branche `dev`
```
git checkout -b dev
```

#### Modifiez `config.toml`
```
baseurl = "http://<gitlab-user>.gitlab.io/<path/to/my-project>
```

#### Créez le fichier `.gitlab-ci.yml`

!!!note `.gitlab-ci.yml
    # All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
    image: registry.gitlab.com/pages/hugo:latest

    pages:
      script:
      - hugo
      only:
      - dev

### Déploiement en ligne

```
git add .gitlab-ci.yml config.toml
git commit -m "<deploy dev hugo website>"
git push origin master
```

## Déploiement du site en production sur un serveur avec Dokku

### Configurer le dossier `public` pour les futurs déploiements

Si le déploiement local s'est bien déroulé, hugo a compilé le site et le dossier `public` a été créé.

Nous allons utilise ce dossier `public` comme module de notre projet, et c'est ce sous-projet que nous déploierons avec Dokku.

```
cd public
git init
#git remote add origin git@gitlab.com:<path/to/my-project.public>.git
git remote add dokku dokku@<domain.org>:<appname>
touch index.php composer.json
```

!!!note "Modifiez les deux fichiers créés : "
    === "index.php"
        ```php
        <?php header ( 'Location: /index.html' ); ?>
        ```
    === "composer.json"
        ```json
        {
            "name": "<my-project.public>",
            "description": "<Name of my Website>"
        }
        ```

### Premier déploiement

```
git add *
git commit -m "first production deployement"
sudo git push dokku master
```

## Ajout des modifications depuis `local` ou `dev`

### Retour à la racine du projet <my-project>
```
cd ..
```

### Intégration des changements
```
git checkout master
git pull
git checkout dokku
git merge master
# A corriger : git submodule add dokku@<domain.org>:<appname> public
```

### Déploiement
```
cd public
git add -A
git commit -m "some modifications"
sudo git push dokku master
```