# Déploiement du site web

2 méthodes sont présentées:  
  - [déploiement local](#deploiement-local)  
  - [déploiement avec Gitlab](#deploiement-en-ligne-avec-gitlab-pages)

## Déploiement local

Le déploiement local du site est très simple, il suffit d'utiliser la commande `hugo server`:   
```
makayabou@domani:~/dev/garagenum/team/webapps/hugo_from_start$ hugo server
Start building sites … 

                   | EN  
-------------------+-----
  Pages            |  0  
  Paginator pages  |  0  
  Non-page files   |  0  
  Static files     |  0  
  Processed images |  0  
  Aliases          |  0  
  Sitemaps         |  1  
  Cleaned          |  0  

Built in 1 ms
Watching for changes in /home/makayabou/dev/garagenum/team/webapps/hugo_from_start/{archetypes,assets,content,data,layouts,static}
Watching for config changes in /home/makayabou/dev/garagenum/team/webapps/hugo_from_start/config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/team/webapps/hugo_from_start/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```
## Déploiement en ligne avec Gitlab Pages

### Préparation locale en vue du déploiement sur Gitlab

#### Mise en place de la gestion de versions

```
git init
```

!!! tip "Pro tip"
    À partir de maintenant, vous pouvez utilisez git pour gérer les versions successives du code.  
    Après chaque changement significatif, utilisez la commande `git add` pour ajouter les changements,
    et `git commit` pour les valider.  
    Utilisez une [*CheatSheet*](https://rogerdudler.github.io/git-guide/) pour vous aider à mémoriser les commandes. 

#### Création du README

```
echo "#<name-of-project>" > README.md
```
Vous pouvez maintenant modifier ce fichier, qui doit présenter le projet et le fonctionnement du site internet, 
en utilisant le format [Markdown](https://docs.roadiz.io/fr/latest/user/write-in-markdown/)

#### Création du fichier de déploiement

En local, à la racine du projet hugo, on crée un fichier `.gitlab-ci.yml`

??? note "**.gitlab-ci.yml**"
    === "Contenu"
        ```
        # All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
        image: registry.gitlab.com/pages/hugo/hugo_extended:latest

        variables:
          GIT_SUBMODULE_STRATEGY: recursive

        test:
          script:
            - hugo
          except:
            - master

        pages:
          script:
            - hugo
          artifacts:
            paths:
              - public
          only:
          - master
        ```
    === "Explication"
        Ce fichier `.yaml` (**attention aux indentations de 2 ++space++ **), définit des tâches qui sont exécutées automatiquement par Gitlab à chaque nouveau commit.  
        
        Deux tâches sont exécutées:  
          - la tâche `test`, qui exécute hugo sur toutes les branches sauf `master`  
          - la tâche `pages`, qui exécute hugo sur la branche master, et propose au téléchargement (les `artifacts`) le contenu du dossier `public` créé par le déploiement automatique de Gitlab. 

#### Ajout des fichiers au versionning

```
git add README.md .gitlab-ci.yml config.toml content assets layouts archetypes
git commit -m "initial commit"
```

### Création du projet sur Gitlab

Connectez-vous à [gitlab.com](https://gitlab.com)

![New Project](../../../assets/divers/devops/hugo/gitlab-new-project.png)

![Use blank Template](../../../assets/divers/devops/hugo/blank-project.png)

![Set path and description for project](../../../assets/divers/devops/hugo/gitlab-project-desc.png)

Vous êtes redirigés vers la page d'accueil de votre projet, qui vous propose des instructions pour manipuler le projet en ligne de commandes depuis votre ordinateur.  
Exécutez les instructions `Git global setup` seulement. Les autres instructions seront à réaliser plus loin dans ce tutoriel.

![Gitlab instructions for CLI project management](../../../assets/divers/devops/hugo/gitlab-project-cli.png)
 

### Liaison entre le projet local et le projet sur Gitlab

#### Vérification de l'état du projet

Assurez-vous à ce stade d'avoir exécuté `git add` et `git commit` pour tous les fichiers créés.  
Vous pouvez vous en assurer avec la commande `git status`:  
```
makayabou@domani:~/dev/garagenum/team/webapps/hugo_from_start$ git status
Sur la branche master
Votre branche est à jour avec 'origin/master'.

Rien à valider, la copie de travail est propre
```

#### Liaison entre la branche locale et la branche master

!!! important "Remplacer tous les éléments entre `< >`"


```
git remote add origin git@gitlab.com:<username>/<projectpath>.git
git push --set-upstream origin master
```
**Après 2 minutes, votre site est désormais déployé à `https://<username>.gitlab.io/<projectpath>`**