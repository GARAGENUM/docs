# Installer l'agent Fusion Inventory NG pour GLPI

## Dépendances

### Générales

```
sudo apt install -y dmidecode hwdata ucf hdparm \
perl libuniversal-require-perl libwww-perl libparse-edid-perl \
libproc-daemon-perl libfile-which-perl libhttp-daemon-perl \
libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl \
libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl \
libxml-xpath-perl libyaml-tiny-perl
```

### Modules

```
sudo apt install libnet-snmp-perl libcrypt-des-perl libnet-nbname-perl \
libdigest-hmac-perl \
libfile-copy-recursive-perl libparallel-forkmanager-perl \
```

## Installation de l'agent

```
curl -s https://api.github.com/repos/fusioninventory/fusioninventory-agent/releases/latest |\
grep "browser_download_url.*deb" |\
cut -d : -f 2,3 |\
tr -d \" |\
wget -qi - &&\
yes | sudo dpkg -i fusioninventory* &&\
sudo apt install -f -y
```

## Configuration de l'agent

!!! note "`/etc/fusioninventory/agent.cfg` "
    ```
    server = https://glpi.legaragenumerique.fr/plugins/fusioninventory/
    ```

### Démarrage de l'agent

```
sudo systemctl restart fusioninventory-agent
sleep 5
sudo pkill -USR1 -f -P 1 fusioninventory-agent
```

Les informations de votre ordinateur remontent automatiquement sur le serveur GLPI

## Installation automatisée

```
git clone https://gitlab.com/KendaRavenclaw/deploiementfusioninventoryclient.git
cd deploiementfusioninventoryclient/
./FusionDeploiementClient.sh
```

Par défaut, l'adresse de l'instance GLPI du Garage est configuré.
Pour faire remonter les information du client vers une autre instance GLPI, entrer l'adresse de celle-ci.