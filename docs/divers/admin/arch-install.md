# Installation de Arch Linux sur PC gamer, avec environnement GNOME.

## AVANT-GARDE ##

- La partition `swap` se trouve sur `/dev/sda1`
- La partition `/` se trouve sur `/dev/sda2`
- `#` veux dire que la commande doit être executée avec les droits `root` (sudo)
- `$` veux dire que la commande doit être executée avec les droits `utilisateur`
- Avant les étapes de post-install, l'utilisateur est `root` par defaut, pas besoin de `sudo`

## Utilisation de pacman :

| Action | Debian | Archlinux |  
| --- | --- | --- |  
| Installer un paquet | apt install "paquet" | pacman -S "paquet" |  
| Mettre à jour les dépôts | apt update | pacman -Sy |  
| Mettre à jour les paquets | apt upgrade | pacman -Syu |  
| Supprimer un paquet | apt remove "paquet" | pacman -Rs "paquet" | 
| Chercher un paquet | apt search "paquet" | pacman -Ss "paquet" |

## Disposition du clavier :
    
    loadkeys fr

## Connexion cablée :

    dhcpcd

## Paramétrage des partitions :

Création des partitions

    cfdisk /dev/sda
        - supprimer toutes les partitions
        - créer une partition égale à la moitié de la RAM (dans le doute, 4Go)
        - selectionner "type" et la marquer "swap"
        - créer une partition avec le reste de la memoire 
        - selectionner "bootable"
        - selectionner "write" et écrire "yes"

Formatage des partitions

    mkfs.ext4 /dev/sda2
    mkswap /dev/sda1
    
Mise à jour des clés de sécurité

    pacman -Sy archlinux-keyring 
    pacman-key --populate archlinux

Montage de la partition système
    
    mount /dev/sda2 /mnt
    
## Installation du système de base :
    
    pacstrap /mnt base base-devel
    genfstab -U -p /mnt/ > /mnt/etc/fstab

## Chroot :

    arch-chroot /mnt

## Configuration de pacman :
    
Editer `/etc/pacman.conf` et décommenter `multilib` et `Include = /etc/pacman.d/mirrorlist`

## Configuration de variables :

Le nom de l'ordinateur (doit être unique) :

    echo "nom-de-l'ordinateur" > /etc/hostname
        
La langue du système :

    echo LANG=fr_FR.UTF-8 > /etc/locale.conf
    export LANG=fr_FR.UTF-8
    
La disposition du clavier : 
    
    echo KEYMAP=fr > /etc/vconsole.conf
        
Configuration du fuseau horaire :

    ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

## Configuration de la locale

Editer `/etc/locale.gen` et décommenter les lignes `fr_FR.UTF-8 UTF-8` et `en_US.UTF-8 UTF-8`
    
    locale-gen

## Installation des paquets : 
Tout sur une seule ligne  
Appuyer sur entrée (choix par defaut) pour toutes les propositions 
    
    pacman -Sy linux linux-firmware xorg-server  
               xorg-apps ntfs-3g grub os-prober  
               git wget pulseaudio-alsa 
               alsa-utils networkmanager  
               network-manager-applet htop  
               gnome firefox firefox-i18n-fr  
               file-roller dhcpcd ppsspp 
               steam minetest xonotic nano vim
              
## Configuration des utilisateurs:

Creation du compte `root`
    
    passwd
    Indiquer le mot de passe (le même que d'habitude)
    
Creation du compte `bellinuxien`

    useradd -m bellinuxien
    passwd bellinuxien
    Indiquer le mot de passe (le même que d'habitude)
    
Faire de `bellinuxien` un compte `administateur`

    nano /etc/sudoers 
    Ajouter "bellinuxien ALL=(ALL) ALL"` en dessous de `"root ALL=(ALL) ALL"
    
Creation du compte `visiteur`

    useradd -m visiteur
    passwd visiteur
    Indiquer le mot de passe (visiteur)
    
## Configuration de grub :

    grub-install /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg        

Ignorer le message `"grub-probe: error: cannot find a GRUB drive for /deb/sdb1"`

    mkinitcpio -p linux
    exit
    reboot

    
## POST-INSTALLATION ##

- `#` veux dire que la commande doit être executée avec les droits `root` (sudo)
- `$` veux dire que la commande doit être executée avec les droits `utilisateur`

## Log-in

    login: bellinuxien
    Password: bellinux@dm!

## Services :

    # localectl set-keymap fr
    # systemctl enable --now NetworkManager
    # systemctl enable --now gdm
    
## Configuration de yay :

    $ git clone https://aur.archlinux.org/yay
    $ cd yay 
    $ makepkg -si
    
## Carte graphique ##

Creation de xorg.conf

    # touch /etc/xorg.conf

Logigramme de l'installation des pilotes de la carte graphique  
Le texte commençant par `#` ou `$` indique une commande à rentrer dans le terminal

```mermaid
graph LR;
  id1["$ lspci | grep VGA"];
  id2[NVIDIA];
  id3[AMD];
  id4["GeForce 600-900, Quadro/Tesla/Tegra K-series"];
  id5["GeForce 400/500 series"];
  id6["# pacman -S nvidia lib32-nvidia-utils"];
  id7["# pacman -S nvidia-390xx lib32-nvidia-390xx-utils"];
  id8["HD 5xxx to Rx300"];
  id9["HD 2xxx, 3xxx et 4xxx"];
  id10["# pacman -S catalyst lib32-catalyst-utils"];
  id11["# pacman -S catalyst-hd234k lib32-catalyst-utils"];
  id12["# nvidia-xconfig"]
  id13["# catalyst --initial"]
  id14["# reboot"]
  id1-->id2;
  id1-->id3;
  id2-->id4;
  id2-->id5;
  id4-->id6;
  id5-->id7;
  id6-->id12;
  id7-->id12;
  id3-->id8;
  id3-->id9;
  id8-->id10;
  id9-->id11;
  id10-->id13;
  id11-->id13;
  id12-->id14;
  id13-->id14;
```    

## Jeux

Jeux sur le AUR

    $ yay -S worldofpadman urbanterror --noconfirm
    
Minetest

    $ mkdir .minetest/games
    $ git clone https://gitlab.com/garagenum/minetest-lug9000
    $ mv minetest-lug9000 .minetest/games

* WINE / LUTRIS  

`Install complête de wine-staging, avec deps x86`
        
    # pacman -S lutris wine-staging giflib lib32-giflib libpng \
    lib32-libpng libldap lib32-libldap gnutls lib32-gnutls \
    mpg123 lib32-mpg123 openal lib32-openal v4l-utils \
    lib32-v4l-utils libpulse lib32-libpulse libgpg-error \
    lib32-libgpg-error alsa-plugins lib32-alsa-plugins \
    alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo \
    sqlite lib32-sqlite libxcomposite lib32-libxcomposite \
    libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \
    ncurses lib32-ncurses opencl-icd-loader \
    lib32-opencl-icd-loader libxslt lib32-libxslt libva \
    lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs \
    lib32-gst-plugins-base-libs vulkan-icd-loader \ 
    lib32-vulkan-icd-loader
    
## Liens

- [Gitlab Edricus](https://gitlab.com/edricus/edri-archinstall/blob/master/install)
- [Installation Guide](https://wiki.archlinux.org/index.php/installation_guide)
- [Wine Dependencies](https://github.com/lutris/lutris/wiki/Wine-Dependencies)
- [Warframe Fix](https://github.com/GloriousEggroll/proton-ge-custom/releases)

## Optionel / Memo

* Warframe
        
        mkdir $HOME/.steam/root/compatibilitytools.d
        git clone https://github.com/GloriousEggroll/proton-ge-custom
        mv proton-ge-custom $HOME/.steam/root/compatibilitytools.d/
        yay -S xboxdrv --noconfirm
        systemctl enable xboxdrv
        
