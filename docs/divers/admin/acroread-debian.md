# Installer Adobe pdf Reader (acroread) sur Debian 10

Adobe ne fournit une version Linux du lecteur pdf que jusqu'à la version 9.5.

Vous pouvez télécharger le `.deb` à partir du site officiel.

    #
    # Add x86 architecture
    #
    sudo dpkg --add-architecture i386
    sudo apt update
    #
    # Download Adobe Reader 9.5 package
    #
    wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb
    #
    # Install it
    #
    sudo apt install ./AdbeRdr9.5.5-1_i386linux_enu.deb libxml2:i386
    #
    # Have AdobeReader proposed at "open with"
    #
    sudo sed -i 's/acroread/acroread %f/' /usr/share/applications/AdobeReader.desktop
    #
    # Clean
    #
    rm AdbeRdr9.5.5-1_i386linux_enu.deb
    
