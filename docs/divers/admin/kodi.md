# Kodi

Kodi est un lecteur vidéo qui permet d'intégrer des plugins pour ajouter du contenu. Les vidéos sont téléchargées par le réseau Torrent et sont diffusées au fur et à mesure comme pour du streaming classique

!!!important Toutes les données sont transmises en clair! Vous êtes responsables de l'utilisation que vous faites du logiciel.

## Installation

### Installation de kodi sous Debian / Ubuntu

1. Ajout du dépôt
  ```
  sudo add-apt-repository ppa:team-xbmc/ppa
  ```
2. Mise à jour des dépôts
  ```
  sudo apt update
  ```
3. Installation du paquet
  ```
  sudo apt install kodi
  ```
### Installation des addons

Vous trouverez des listes d'add-ons à jour sur [troypoint.com](http://troypoint.com)

  ```
  ```
  ```
  ```
