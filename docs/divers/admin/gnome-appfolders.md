# Use AppFolders in Gnome

In the dash view, you can have applications organized by categories like this:

![ScreenShot](../../assets/divers/admin/gnome-appfolders.png)

Use this simple script for that:

!!! note "Deployment script"
    ```
    gsettings set org.gnome.desktop.app-folders folder-children "['All', 'Game', 'Utilities', 'Graphics', 'AudioVideo', 'Education', 'Network', 'Office', 'Development']"
    APP_FOLDERS=(All Game Utilities Graphics AudioVideo Education Network Office Development)
    for i in "${APP_FOLDERS[@]}"; do
        gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ name $i 
        gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ translate true
        case $i in 
            'All')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['All', 'Game', 'Utilities', 'System', 'Settings', 'Graphics', 'AudioVideo', 'Education', 'Network', 'Office', 'Development']";;
            'Game')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Game']" ;;
            'Utilities')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Utility', 'System', 'Settings']";;
            'Graphics')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Graphics']";;
            'AudioVideo')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['AudioVideo']";;
            'Education')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Education', 'Science']";;
            'Network')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Network']";;
            'Office')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Office']";;
            'Development')
                gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/$i/ categories "['Development']";;
        esac
    done    
    ```


