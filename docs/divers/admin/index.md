# Guides pour l'administration système

- [ Installer Adobe pdf reader on Debian ](acroread-debian.md)
- [ Configurer AppFolders dans Gnome](gnome-appfolders.md)
- [ Réinitialiser un mot de passe Windows](reset-winpasswd.md) 
- [ Installer Lug9000 sur Minetest ](lug9000.md)
- [ Installer Arch Linux ](arch-install.md)
- [ Installer Kodi multimedia player](kodi.md)
- [ Utiliser Fusion Inventory pour GLPI](fusion)  
