
# GIT Source Code Management

Git est un système de gestion de versions partagé qui fut développé en 2005 par le créateur de Linux Linus Thorvalds et publié sous licence libre GNU-GPLv2.
Il permet de stocker son code source en ligne pour y accéder de n'importe où et le partager facilement.

<img src="../../../assets/git/git-branch.png">

## Installation

``` { .bash .copy }
sudo apt install git -y
```

## Les principales commandes

- Cloner un dépôt distant sur sa machine locale:

``` { .bash .copy }
git clone https://gitlab.com/mon-dépôt-distant.git
```

- Initialiser un dossier de projet pour le "pousser" en ligne:

``` { .bash .copy }
cd mon-projet/
git init
```

- Ajouter des fichiers au suivi (pour les synchroniser vers un dépôt distant):

``` { .bash .copy }
git add fichier1 fichier2
```

- Créer un enregistrement (une version) du code avec un commentaire:

``` { .bash .copy }
git commit -m "commentaire (exemple: modification du fichier1, ajout fonction test)"
```

- Pousser les changements vers le dépôt distant (Gitlab ou Github par exemple):

``` { .bash .copy }
git push
```

- Changer de branche dans le projet:

``` { .bash .copy }
git checkout nom-de-la-branche
```

- Créer une nouvelle branche à partir de la branche actuelle:

``` { .bash .copy }
git branch nom-nouvelle-branche
```

- Pousser une branche nouvellement créée en local vers le dépôt distant:

``` { .bash .copy }
git push origin nom-nouvelle-branche
```

- Effacer une branch locale:

``` { .bash .copy }
git branch -d branche-a-effacer
```

- Effacer une branche du dépôt distant:

``` { .bash .copy }
git push origin --delete branche-a-effacer
```

## Workflow

### Cloner un dépôt distant en local, l'éditer puis pousser les changement

- Cloner un dépôt distant sur sa machine locale:

``` { .bash .copy }
git clone https://gitlab.com/mon-dépôt-distant.git
```

- Entrer dans le dossier du dépôt et modifier un fichier:

``` { .bash .copy }
cd mon-dépôt-distant/ # modification sur un fichier
```

- Ajouter le fichier modifié au suivi:

``` { .bash .copy }
git add fichier-modifié
```

- Créer un enregistrement de la modification

``` { .bash .copy }
git commit -m "modif fichier-modifié"
```

- Pousser les changements sur le dépôt en ligne:

``` { .bash .copy }
git push
```

### Initialiser un dépôt git dans un fichier local afin de le pousser sur un dépôt en ligne

- On entre dans le dossier et on l'initialise en tant que dépôt git:

``` { .bash .copy }
cd mon-dépôt-local/
git init
```

- Ajouter tout les fichiers du dossier au suivi:

``` { .bash .copy }
git add *
# ou
git add .
```

- Ajouter l'adresse du dépôt distant qui va recevoir le dépôt local:

``` { .bash .copy }
git remote add origin https://gitlab.com/mon-user/mon-nouveau-dépôt.git
```

- Créer un enregistrement avec un commentaire:

``` { .bash .copy }
git commit -m "premier push"
```

- Pousser le nouveau dépôt vers le dépôt distant en ligne:

``` { .bash .copy }
git push origin main # ou le nom de la branche
```

## Autres commandes utiles

- Récupérer localement les changements du dépôt distant:

``` { .bash .copy }
git pull
```

!!! tip "Si des changements ont été effectués localement depuis le git clone ou depuis le dernier git pull, git vous avertira qu'il faut remiser les changements (annuler) ou les valider (pousser les changements vers le dépôt distant) pour ne pas écraser ceux-ci avec la version du code se trouvant sur le dépôt distant."

- Savoir sur quelle branche on se trouve et les branches disponibles localement:

``` { .bash .copy }
git branch 
```

- Créer une nouvelle branche et basculer vers la nouvelle branche:

``` { .bash .copy }
git checkout -b nom-nouvelle-branche
```

- Connaitre l'état du dépôt local:

``` { .bash .copy }
git status
```

- Revenir à l'état après le git clone en éffaçant les chagements éffectués:

``` { .bash .copy }
git reset --hard HEAD
```

- Visualiser les différences entre les fichiers originaux et les fichiers modifiés:

``` { .bash .copy }
git diff 
```

## Utils

Pour éviter de faire des "git branch" pour savoir dans quelle branche on se trouve, on peut ajouter la référence dans le chemin de notre terminal (Linux) en modifiant les fichier ~/.bashrc:

??? note "~/.bashrc"
    === "Ajouter à la fin"
        ``` { .bash .copy }
        git_branch() {
          git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
        }
        export PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]:\[\033[38;5;5m\]\$(git_branch)\[\033[00m\]\$ "
        ```
    === "Explications"
        Ces lignes ajouteront au prompt le nom de la branche sur laquelle vous vous trouvez lorsque vous êtes dans un dépôt git.

## Référence :books:

[Documentation GIT](https://git-scm.com/)

## Cheatsheet

[Cheatsheet GIT](https://git.guide.legaragenumerique.fr)