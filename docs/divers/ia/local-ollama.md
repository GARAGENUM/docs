# Comment déployer un model d'IA local intégré à VScodium

![ia](../../assets/ia/ia.png)

## Installation

- Installer Ollama:

``` { .bash .copy }
curl -fsSL https://ollama.com/install.sh | sh
```

- Télécharger le modèle:

``` { .bash .copy }
ollama pull llama3:latest
```

- Lancer le modèle:

``` { .bash .copy }
ollama run llama3:latest
```

!!! tip "La console permet d'intéragir avec le modèle directement, pour quitter, appuyer sur ++ctrl+d++"

- Installer le plugin ```continue```:

![continue](../../assets/ia/continue-1.png)

- Cliquer sur le rouage en bas à droite:

![continue](../../assets/ia/continue-2.png)

## Configuration

??? note "Editer le fichier config.json:"
    ``` { .bash .copy }
    {
      "models": [
        {
          "title": "Ollama",
          "provider": "ollama",
          "model": "llama3:latest",
          "apiBase": "http://localhost:11434/"
        }
      ]
    }
    ```

> Il devrait ressembler à ça:

![continue](../../assets/ia/continue-3.png)

## Gérer Ollama

- Stopper le service:

``` { .bash .copy }
sudo systemctl stop ollama
```

- Démarrer le service:

``` { .bash .copy }
sudo systemctl start ollama
```

- Supprimer un modèle:

``` { .bash .copy }
ollama rm <MODEL>
```

- Modèles compatibles:

!!! tip "[Ollama modèles librairie](https://ollama.com/library)"

- Télécharger un modèle:

``` { .bash .copy }
ollama pull <MODEL>
```
