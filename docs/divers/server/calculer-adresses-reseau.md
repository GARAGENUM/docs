# Calculs d'adresses IPv4 et de leurs masques de sous-réseaux

## Méthode pour calculer des adresses IP, leurs masques de sous-réseaux, le nombre de machines adressables sur un sous-réseau, etc.

*Inspirée du cours "Réseaux locaux" organisé par les Cours municipaux d'adultes de la mairie de Paris.*

**Dans chacun des exemples ci-après (un par classe d'adresses), on cherche à adresser un certain nombre de sous-réseaux. On cherche aussi à connaître leur masque de sous-réseau, leurs plages d'adresses disponibles ainsi que le nombre d'hôtes qu'ils pourront contenir.**

- L'exemple de classe A fournit des explications plus détaillées, sachant que le principe est sensiblement le même pour les deux autres classes.
- La classe C est celle pour laquelle les calculs sont les plus aisés.
- Nous avons aussi mis un exemple en classe A qui consiste à déduire le nombre de sous-réseaux et leur masque depuis un nombre d'hôtes. Là aussi le principe de calcul serait peu ou prou le même pour les deux autres classes.
- Enfin, un dernier exemple montre comment proposer un plan d'adressage à partir d'un nombre déterminé d'hôtes.

## Prérequis

Pour calculer rapidement toutes ces données, il faut *a minima* :

- Connaître au moins les huit premières puissances de deux et les principes de base du calcul binaire ;
- Connaître le principe du masquage ;
- Connaître les plages d'adresses des classes A, B et C, leurs masques et CIDR par défaut ;
- Connaître le nombre total d'adresses disponibles pour chacune de ces classes.

Pour rappel :

|Classe|Début|Fin|Masque décimal|Masque binaire|CIDR|Nombre d'adresses disponibles|
|-|-|-|-|-|-|-|
|A|0.0.0.0|127.255.255.255|255.0.0.0|1111 1111. 0000 0000. 0000 0000. 0000 0000|/8|16 777 216 (2^24)|
|B|128.0.0.0|191.255.255.255|255.255.0.0|1111 1111. 1111 1111. 0000 0000. 0000 0000|/16|65 536 (2^16)|
|C|192.0.0.0|223.255.255.255|255.255.255.0|1111 1111. 1111 1111. 1111 1111. 0000 0000|/24|256 (2^8)|

## Remarque préalable

**Il est intéressant de noter que le nombre de sous-réseaux dépend du nombre de bits supplémentaires attribués en plus au réseau. Il est très facile également d'en déduire la valeur décimale de l'octet du masque concerné :**

|Bits|Octet|Sous-réseaux|
|:-:|:-:|:-:|
|1|128|2|
|2|192|4|
|3|224|8|
|4|240|16|
|5|248|32|
|6|252|64|
|7|254|128|
|8|255|256|

Le dernier cas est impossible pour une classe C.

*Source :* Tout sur les Réseaux et Internet 3e édition, *Dunod, Paris, 2012.*

## Classe A : 114.0.0.0, sept sous-réseaux

### Masque par défaut

- On part du masque par défaut : 255.0.0.0/8
- Soit, en binaire : 1111 1111. 0000 0000. 0000 0000. 0000 0000
- On travaille sur le deuxième octet.

### Nouveau masque

- **8 est la puissance de deux immédiatement supérieure à 7** (le nombre de sous-réseaux qu'on cherche à adresser). 4 serait trop peu, 16 serait trop grand.
- **8, c'est donc 2^3. L'exposant 3 nous indique le nombre de bits nécessaires à la création des sous-réseaux, et qu'il faut ajouter au masque par défaut.**
- Le masque de nos sous-réseaux sera donc : 1111 1111. 1110 0000. 0000 0000. 0000 0000
- Soit, en décimal : 255.224.0.0/11 (pour rappel : le CIDR à 11 nous indique que le masque utilise les 11 premiers bits sur les 32 qui lui sont alloués au total)

### Nombre d'adresses disponibles sur chaque sous-réseau

- À partir de là, on peut calculer le nombre d'adresses disponibles sur chacun des sous-réseaux. Ce nombre correspond au nombre de bits encore disponibles une fois le masque de sous-réseau calculé, soit ici 2^21.
- En décimal et pour une adresse de classe A, ce nombre correspond au total des bits disponibles sur le second octet (256) soustrait du nombre de bits utilisés par le masque (224), le tout multiplié par le nombre total de bits disponibles sur chacun des deux derniers octets, soit: (256-224)\*256\*256 = 32\*256\*256 (Pourquoi 256 et pas 255 pour nos calculs ? Parce qu'il ne faut pas oublier qu'en binaire, le 0 compte.)
- **Ce nombre d'adresses correspond aussi au nombre total d'adresses disponibles pour la classe A divisé par 8 (2^3), soit : 16 777 214/8. C'est la méthode de calcul la plus simple quand il n'y a pas un bombre trop élevé de sous-réseaux à adresser.**
- Finalement, nous disposerons de 2 097 152 adresses sur chacun de nos sous-réseaux, **soit 2 097 150 hôtes une fois soustraites les deux adresses réservées au réseau et au broadcast**.

### Plages d'adresses

- Notre premier sous-réseau disposera de la plage 114.0.0.0 à 114.31.255.255
- Notre deuxième sous-réseau disposera de la plage 114.32.0.0 à 114.63.255.255
- **Et ainsi de suite de 32 en 32 sur le deuxième octet.**
- **Le deuxième octet de la dernière adresse du dernier réseau disponible (le 8e) est identique au deuxième octet du masque.**
- Le dernier sous-réseau disponible utilisera donc la plage 114.224.0.0 à 114.255.255.255.

### Nombre d'hôtes disponibles par sous-réseau de classe A :

|Bits|Octet|Sous-réseaux|Hôtes|
|:-:|:-:|:-:|:-:|
|1|128|2|8 388 606|
|2|192|4|4 194 302|
|3|224|8|2 097 150|
|4|240|16|1 048 574| 
|5|248|32|524 286|
|6|252|64|262 142|
|7|254|128|131 070|
|8|255|256|65 534|

*Nota* : Il est aussi possible de travailler en classe A sur le premier octet et donc d'avoir des sous-réseaux encore plus grands, mais ce n'est pas l'objet de ce tuto. Le principe serait ceci dit le même.

## Classe B : 192.10.0.0, quatre-vingt-seize sous-réseaux

### Masque par défaut

- Masque par défaut : 255.255.0.0/16
- Soit, en binaire : 1111 1111. 1111 1111. 0000 0000. 0000 0000
- On travaille sur le troisième octet.

### Nouveau masque

- 128 est la puissance de deux immédiatement supérieure à 96.
- 128, c'est donc 2^7. Sept bits seront donc nécessaires à la création des sous-réseaux.
- Le masque sera donc : 1111 1111. 1111 1111. 1111 1110. 0000 0000
- Soit : 255.255.254.0/23

### Nombre d'adresses disponibles sur chaque sous-réseau

- Nombre d'adresses disponibles sur chacun des sous-réseaux : 2^9
- En décimal et pour une adresse de classe B ce nombre correspond au total des bits disponibles sur le troisième octet (256) soustrait du nombre de bits utilisés par le masque (254), le tout multiplié par le nombre total de bits disponibles sur le dernier octet, soit: (256-254)\*256 = 2*256
- **Ce nombre d'adresses correspond aussi au nombre total d'adresses disponibles pour la classe B divisé par 128 (2^7), soit : 65 536/128**
- Finalement, nous disposerons de 512 adresses sur chacun de nos sous-réseaux, soit 510 hôtes une fois soustraites les deux adresses réservées.

### Plages d'adresses

- Notre premier sous-réseau disposera de la plage 129.10.0.0 à 129.10.1.255
- Notre deuxième sous-réseau disposera de la plage 129.10.2.0 à 129.10.3.255
- etc.

### Nombre d'hôtes disponibles par sous-réseau de classe B :

|Bits|Octet|Sous-réseaux|Hôtes|
|:-:|:-:|:-:|:-:|
|1|128|2|32 766|
|2|192|4|16 382|
|3|224|8|8 190|
|4|240|16|4 094| 
|5|248|32|2 046|
|6|252|64|1 022|
|7|254|128|510|
|8|255|256|254|

## Classe C : 196.80.150.0, dix sous-réseaux

### Masque par défaut

- Masque par défaut : 255.255.255.0/24
- Soit, en binaire : 1111 1111. 1111 1111. 1111 1111. 0000 0000
- On travaille sur le quatrième octet.

### Nouveau masque

- 16 est la puissance de deux immédiatement supérieure à 10.
- 16, c'est donc 2^4. Quatre bits seront donc nécessaires à la création des sous-réseaux.
- Le masque sera donc : 1111 1111. 1111 1111. 1111 1111. 1111 0000
- Soit : 255.255.255.240/28

### Nombre d'adresses disponibles sur chaque sous-réseau

- Nombre d'adresses disponibles sur chacun des sous-réseaux : 2^4
- En décimal et pour une adresse de classe C ce nombre correspond au total des bits disponibles sur le troisième octet (256) soustrait du nombre de bits utilisés par le masque (240), soit: 256-240
- **Ce nombre d'adresses correspond aussi au nombre total d'adresses disponibles pour la classe C divisé par 16 (2^4), soit : 256/16**
- Finalement, nous disposerons de 16 adresses sur chacun de nos sous-réseaux, soit 14 hôtes une fois soustraites les deux adresses réservées.

### Plages d'adresses

- Notre premier sous-réseau disposera de la plage 196.80.150.0 à 196.80.150.15
- Notre deuxième sous-réseau disposera de la plage 196.80.150.16 à 196.80.150.31
- etc.

### Nombre d'hôtes disponibles par sous-réseau de classe B :

|Bits|Octet|Sous-réseaux|Hôtes|
|:-:|:-:|:-:|:-:|
|1|128|2|126|
|2|192|4|62|
|3|224|8|30|
|4|240|16|14| 
|5|248|32|6|
|6|252|64|2|

## Classe A : 80.70.90.30, 262 142 ordinateurs par sous-réseau

- On part du nombre d'ordinateurs pour déduire le nombre de sous-réseaux et leur masque
- On travaille sur le deuxième octet

### Nombre de sous-réseaux : méthode décimale

- Il s'agit d'une adresse de Classe A, or, avec le masque par défaut à 255.0.0.0, on peut adresser jusqu'à 16 777 216 machines (2^24).
- La puissance de 2 située immédiatement au-dessus de 262 142 est 262 144 (2^18), qui correspond au nombre maximal d'adresses disponibles sur notre sous-réseau.
- On obtient donc le nombre de sous-réseaux en divisant le nombre total d'adresses disponibles pour une adresse de classe A avec un masque par défaut par celui du nombre total d'adresses disponibles sur notre sous-réseau : 16 777 216/262 144 = 64

### Nombre de sous-réseaux : méthode binaire

- En binaire, 2^18 s'écrit 0000 0000. 0000 0011. 1111 1111. 1111 1111 (nombre qui correspond aussi à la dernière adresse disponible sur notre sous-réseau)
- On remarque que la dernière adresse disponible occupe les deux derniers bits de notre deuxième octet.
- Cela nous laisse les 6 premiers bit pour le réseau, chiffre dont on peut déduire la puissance de deux qui nous donnera le nombre de sous réseaux disponibles : 2^6, soit 64.

### Nombre de sous-réseaux : division de puissances

- Le nombre de sous-réseaux disponibles est aussi égal à 2^24 (nombre maximal de machines pour une adresse de classe A avec un masque par défaut) - 2^18 (nombre maximal d'adresses pour notre sous-réseau)
- Soit : 2^24/2^18=2^6=64

### Masque de sous-réseau

- Pour calculer le deuxième octet du masque de sous-réseau, on divise le nombre total de bits disponibles (256) par le nombre de réseaux trouvé (64) : 256/64=4
- puis on soustrait ce chiffre au nombre total de bits disponibles : 256-4=252
- Soit un masque valant 255.252.0.0/14
- Il est aussi facile de déduire le masque de part sa notation binaire puiqsu'il occupe les 14 premiers bits non utilisés par l'adressage hôte, soit : 1111 1111. 1111 1100. 0000 0000. 0000 0000
- On retrouve nos 6 premiers bits occupés sur le deuxième octet par l'adressage réseau. 

### Plages d'adresses

- Notre premier sous-réseau dispose de la plage 80.0.0.0 à 80.3.255.255
- Notre deuxième sous-réseau dispose de la plage 80.4.0.0 à 80.7.255.255
- etc.

## Plan d'adressage pour 16 000 hôtes

- La puissance de 2 immédiatement supérieure à 16 000 est 16 384 (2^14). En binaire : 0000 0000. 0000 0000. 0111 1111. 1111 1111
- On peut en déduire le masque nécessaire pour un tel sous-réseau : 1111 1111. 1111 1111. 1000 0000. 0000 0000
- Un sous-réseau de classe B avec un masque à 255.255.128.0/17 serait donc optimal.
- On travaille donc sur le troisième octet.
- Seul le premier bit de cet octet est occupé par notre sous-réseau, ce qui nous permet de déduire le nombre maximal de sous-réseaux possibles: 2^1, soit 2.

*A lire en complément :* [Classe d'adresses IP sur Wikipedia](https://fr.wikipedia.org/wiki/Classe_d%27adresse_IP)
