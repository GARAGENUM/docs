# NEXTCLOUD UPDATE PROCESS

## CHECK

* Avant chaque update checker dans les paramètres de base si des indices de table bdd manques ou autre.
* L'update s'éffectue par palier de versions (20 vers 21 puis 21 vers 22, etc...)

## PROCESS

### ARRET DE LA STACK

``` { .bash .copy }
docker-compose down
```

### modifier l'image docker de Nextcloud

``` { .bash .copy }
nano docker-compose.yml
```

!!! tip "Modifier la version d'image vers la version immédiatement supérieure dans le docker-compose.yml"
    ```
    20 -> 21, 21 -> 22
    ```
    
### Relancer la stack

``` { .bash .copy }
docker compose up -d
```

### Mettre le container nextcloud web en mode maintenance

Mettre Nextcloud en mode maintenance
``` { .bash .copy }
docker exec -u www-data <nextcloud-container> php occ maintenance:mode --on
```

Lancer l'update :coffee:
``` { .bash .copy }
docker exec -u www-data <nextcloud-container> php occ upgrade
```

Désactiver le mode maintenance
``` { .bash .copy }
docker exec -u www-data <nextcloud-container> php occ maintenance:mode --off
```

### Vérifier les logs

Après une mise à jour, vérifier les logs docker pour d'éventuelles erreurs sur des champs de base de donnée ou indices manquants:

``` { .bash .copy }
docker logs <nextcloud-database-container>
```

#### Indice de table manquants (opt)

``` { .bash .copy }
# Commande pour ajouter des colonnes manquantes à certaines tables de la BDD
docker exec --user www-data -it <nextcloud-database-container> /var/www/html/occ db:add-missing-indices
```

#### Modifier le type de champs d'un indice d'une table (opt)

Se connecter dans le conteneur:
``` { .bash .copy }
docker exec -it <nextcloud-database-container> bash
```

se connecter en tant que user nextcloud: 
``` { .bash .copy }
mariadb -u <nextcloud-user> -p
# saisir le password du user nextcloud
```

## CRON JOB

```
crontab -e
```

!!! note "crontab"
    ```
    */5  *  *  *  * docker exec -u www-data <NOM_DU_CONTAINER> php -f cron.php
    ```