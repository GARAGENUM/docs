# Installer un serveur pxe

## Cloner le projet

``` { .bash .copy }
makayabou@domani:~$ git clone https://gitlab.com/garagenum/tools/linux/debian-customizer.git
Clonage dans 'debian-customizer'...
remote: Enumerating objects: 717, done.
remote: Counting objects: 100% (717/717), done.
remote: Compressing objects: 100% (362/362), done.
remote: Total 1800 (delta 409), reused 612 (delta 348), pack-reused 1083
Réception d'objets: 100% (1800/1800), 11.76 MiB | 18.27 MiB/s, fait.
Résolution des deltas: 100% (858/858), fait.
```

## Exécuter le script

``` { .bash .copy }
cd debian-customizer/
sudo bash install-pxe-server.sh 
```

## Configurer les interfaces réseau

Choisissez d'abord l'interface reliée au web:
![Utilisez pour cela l'interface dialog](../../assets/divers/server/pxe-select-wan.png)

Choisissez ensuite l'interface réseau reliée aux ordinateurs clients:
![Utilisez pour cela l'interface dialog](../../assets/divers/server/pxe-select-lan.png)


**Voilà, votre serveur pxe est prêt! **
