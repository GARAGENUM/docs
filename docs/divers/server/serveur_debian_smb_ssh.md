# Création d'un serveur local avec partage de fichiers Samba et backup des données en externe

## Installation de SSH

### Installation

``` { .bash .copy }
sudo apt update
sudo apt install openssh-server
```

### Configuration du serveur ssh

!!! note "/etc/ssh/sshd_config"
    ``` { .copy }
    # Define a diferent port from default port=22      
    Port 43000
    #
    # Opening time  of TCP session 
    LoginGraceTime 20s
    #
    # Never accept root login
    PermitRootLogin no
    #
    # RestrictMode forces config to be respected
    RestrictMode yes
    # 
    # Maximum authentification Tries
    #
    MaxAuthTries 3
    #
    # Maximum sessions in same time
    #
    MaxSessions 1
    #
    # Authentification by public key 
    #
    PubkeyAuthentication yes
    #
    # AuthorizedKeysFile
    # Place for pubkeys from clients
    #
    AuthorizedKeysFile     .ssh/authorized_keys
    #
    # Authentication by Password 
    #
    PasswordAuthentication no
    #
    # Question-Answer between server and client to validate authentication
    #
    ChallengeResponseAuthentication yes
    #
    # Allow only some users
    AllowUsers ktha
    # Message of the day 
    PrintMotd no
    # Language of your local machine
    AcceptEnv LANG LC_*
    ```

### Démarrage du serveur ssh
``` { .bash .copy }
sudo systemctl restart sshd.service
```   
---------------------

## Installation de Samba

### Installation
``` { .bash .copy }
sudo apt install samba smbclient
```

### Configuration du serveur

    !!! note "/etc/samba/smb.conf"
    [global]
        workgroup = KTHA-CIE
        #name resolve order = bcast host
        server string = %h server (Samba, Ubuntu)
        dns proxy = no
        log file = /var/log/samba/log.%m
        max log size = 1000
        syslog = 0
        panic action = /usr/share/samba/panic-action %d
        server role = standalone server
        obey pam restrictions = yes
        unix password sync = yes
        passwd program = /usr/bin/passwd %u
        passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
        pam password change = yes
        map to guest = bad user
        usershare allow guests = yes
        username map = /etc/samba/smbusers
        security = user
    [toncul]
        path = /home/$USER/Documents
        available = yes
        browseable = yes
        writeable = yes
        valid users = ktha
        comment = Serveur de fichiers Ktha Compagnie

#### Création d'un mot de passe sur le serveur

``` { .bash .copy }
sudo smbpassword -a $USER
```

### Configuration des clients

Il est nécessaire de configurer le WORKGROUP pour les clients, afin qu'ils soient autorisés à se connecter au serveur Samba:

``` { .bash .copy }
sudo sed -i 's/WORKGROUP/KTHA-CIE/' /etc/samba/smb.conf
```
    
--------------

## Configuration du Backup

### Installation des dépendances

    sudo apt install tee rsync
    
### Création du script de sauvegarde

#### Step 1. Créer la clé d'authentification

On créé une variable pour le nom de la clé
``` { .bash .copy }
KEY_PATH=~/.ssh/owner-of-key_rsa 
```

On génère la clé dans le dossier par défaut pour ne pas avoir à indiquer le chemin de la clé avec **-i*** lors d'une connexion avec `ssh`
``` { .bash .copy }
ssh-keygen -b 4096 -t rsa -N '' -f $KEY_PATH
```

#### Step 2. Envoyer la clé publique au serveur

``` { .bash .copy }
ssh-copy-id -i $KEY_PATH $USER@$SERVER_IP
```

#### Step 2. Mettre le rsync en cron

Script à exécuter: 

!!! note "/etc/cron.daily/backup"
    ``` { .copy }
    #!/bin/bash
    rsync -ahv --update --delete --info=backup2,copy2,del2,name1,stats2 --log-file="/home/$USER/backup_log/backup.log.$(date +%Y-%m-%d)" /home/$USER/Documents/ -e 'ssh -p 43000 -i $KEY_PATH $USER@176.151.59.186:/home/$USER/Documents'
    ```

N'oubliez pas de rendre le script exécutable:
``` { .bash .copy }
sudo chmod +x /etc/cron.daily/backup
```

Définition de la tâche cron:
``` { .bash .copy }
sudo tee -a /etc/cron.d/backup > /dev/null <<EOT
SHELL=/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
00 04 * * *   ktha    /etc/cron.daily/backup
EOT
```
