# Console postgres

## List databases

``` { .bash .copy }
postgres=# \l
```

## Switch database
``` { .sql .copy }
postgres=# \c db_name
```

List tables
``` { .sql .copy }
db_name=# \dt
```

Show table content
``` { .sql .copy }
db_name=# \d table_name
```

Modify table
``` { .sql .copy }
UPDATE table_name
SET column1 = 'value1',
    column2 = 'value2',
    ...
WHERE condition;
```
