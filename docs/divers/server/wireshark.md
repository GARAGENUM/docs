# WIRESHARK

[![WIRESHARK](../../assets/divers/server/ws-logo.svg)](https://www.wireshark.org/)

Comment installer Wireshark et capturer le traffic réseau d'un serveur distant via SSH.

### Installation

``` { .bash .copy }
sudo apt install wireshark tcpdump -y
```
> Note: A installer sur le serveur ainsi que sur la machine cliente "streamant" le flux réseau.

### Configuration

- Ajouter l'utilisateur au groupe wireshark
``` { .bash .copy }
sudo usermod -aG wireshark $USER
```

- Récupérer l'interface réseau du serveur distant:
``` { .bash .copy }
ssh -p 2222 remote-user@123.45.67.89
# L'interface connectée au réseau est celle qui donne l'adresse IP publique
ip a
```

> Note: Il est préférable d'avoir éffectué l'échange de clefs SSH via la commande:
``` { .bash .copy }
ssh-copy-id remote-user@server-ip
```

#### Interface Wireshark

- Cliquer sur SSH remote capture dans le menu Capture:

![WS](../../assets/divers/server/ws1.png)

- Entrer l'adresse IP publique du serveur ainsi que le port SSH de connection:

![WS](../../assets/divers/server/ws2.png)

- Entrer le nom de l'utilisateur distant du serveur ainsi que le chemin vers la clef SSH privée:

![WS](../../assets/divers/server/ws3.png)

- Renseigner l'interface du serveur distant dans Remote interface ainsi que dans la remote command:

![WS](../../assets/divers/server/ws4.png)

> Note: Il est d'usage d'ajouter l'argument 'not(host server-IP and port SSH)' pour ne pas polluer la capture avec le traffic généré par la connection au serveur.

Le remote filter se configure automatiquement à la première connection pour enlever le traffic du loopback (127.0.0.1) ainsi que de la machine cliente capturant le flux du serveur. 

### References

Pour plus d'informations, consulter la [documentation](https://www.wireshark.org/docs/) de Wireshark ou le [wiki](https://gitlab.com/wireshark/wireshark/-/wikis/home)