# Administration Serveur

- [Installer un serveur PXE](install_pxe.md)
- [Installer Nextcloud avec Docker-Compose](nextcloud_docker-compose_on_debian-10_with_nginx_reverse-proxy.md)
- [Mettre à jour Nextcloud](nextcloud_update.md)
- [Installer Wordpress avec Docker-Compose](wordpress_docker-compose_on_debian-10_with_nginx_reverse-proxy.md)
- [Installer un serveur Debian SSH + Samba](serveur_debian_smb_ssh.md)
- [Un mémo postgres-cli](postgres.md)
- [Un mémo mysql](mysql.md)
- [capturer le traffic réseau avec Wireshark](wireshark.md)
- [calculer les adresses d'un réseau](calculer-adresses-reseau.md)