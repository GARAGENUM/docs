# Installer Wordpress avec docker-compose sur Debian 10 avec Nginx en reverse-proxy

## Installer **nginx**

Nginx est un serveur web, comme Apache.  
Nous allons l'utiliser comme reverse-proxy, c'est-à-dire qu'il renverra les requêtes d'un autre serveur, que nous installerons aux côtés de wordpress.  

Ainsi nous pourrons utiliser le serveur de notre choix pour notre web-application Wordpress (Apache ou Nginx).


On installe Nginx à partir des dépôts Debian, de façon à avoir une version stable, avec l'apport des correctifs de sécurité, et qui ne sera jamais cassé par une modification du système.

``` { .bash .copy }
sudo apt update
sudo apt upgrade
sudo apt install nginx
```

Dans votre navigateur, en tapant dabs la barre d'adresse `http://IP_DU_SERVEUR`, vous devriez accéder à la page d'accueil de nginx
![Welcome To Nginx! ](../../assets/divers/server/welcome_to_nginx.png)


## Installation de docker

### Installation des dépendances

``` { .bash .copy }
sudo apt update
sudo apt -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```
### Installation de la clé du dépôt Docker

``` { .bash .copy }
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
```

### Ajout du dépôt docker dans le **sources.list**

``` { .bash .copy }
sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian \
        $(lsb_release -cs) \
        stable"
```

### Mise à jour de la liste des paquets et installation de docker

``` { .bash .copy }
sudo apt update && sudo apt install docker-ce docker-ce-cli containerd.io
```

### Ajout de l'utilisateur dans le groupe **docker** pour utiliser docker sans sudo

``` { .bash .copy }
sudo usermod -aG docker $USER
newgrp docker
```


## Installation de Docker-Compose

### Téléchargement de Docker-Compose
``` { .bash .copy }
curl -s https://api.github.com/repos/docker/compose/releases/latest \
  | grep browser_download_url \
  | grep docker-compose-linux-x86_64 \
  | cut -d '"' -f 4 \
  | wget -qi -
```

### Mise en place de l'exécutable

``` { .bash .copy }
chmod +x docker-compose-linux-x86_64 && sudo mv docker-compose-linux-x86_64 /usr/local/bin/docker-compose
sudo systemctl restart docker
```

### Auto-complétion pour Bash

``` { .bash .copy }
sudo mkdir -p /etc/bash_completion.d
sudo curl -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
source /etc/bash_completion.d/docker-compose
```

**Docker et Docker-compose sont maintenant installés**

## Mise en place de Wordpress avec docker-compose

Créer un dossier pour accueillir le projet, et le fichier **docker-compose.yml**:

``` { .bash .copy }
mkdir wordpress && cd wordpress
touch docker-compose.yml
```

Ce fichier de configuration va déployer une image docker de wordpress et de la base de données mysql.

!!! info "docker-compose.yml"
    ``` { .yaml .copy }
    services:
       db:
         image: mysql:5.7
         volumes:
           - db_data:/var/lib/mysql
         restart: always
         environment:
           MYSQL_ROOT_PASSWORD: somewordpress
           MYSQL_DATABASE: wordpress
           MYSQL_USER: wordpress
           MYSQL_PASSWORD: wordpress

       wordpress:
         depends_on:
           - db
         image: wordpress:latest
         ports:
           - "8000:80"
         restart: always
         environment:
           WORDPRESS_DB_HOST: db:3306
           WORDPRESS_DB_USER: wordpress
           WORDPRESS_DB_PASSWORD: wordpress
           WORDPRESS_DB_NAME: wordpress
           VIRTUAL_HOST: wordpress.mondomaine.fr
    volumes:
        db_data: {}
    ```

## Installation de SSL pour Nginx avec Let's Encrypt

Let's encrypt est un service qui permet de générer gratuitement un certificat SSL pour le domaine de notre choix.
``` { .bash .copy }
sudo apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface
sudo apt install python3-certbot-nginx
sudo certbot -d wordpress.mondomaine.fr 
```
**Les certificats sont alors crés dans `/etc/letsencrypt/wordpress.mondomaine.fr`!**

## Configuration du **Server Bloc Nginx ** pour wordpress.mondomaine.fr

On peut maintenant créer le fichier de configuration nginx  dans /etc/nginx/sites-available/wordpress.mondomaine.fr.conf:

!!! info "wordpress.mondomaine.fr"
    ``` { .copy }
    upstream wpdocker {
        server 127.0.0.1:8000;
    }

    server {

        server_name wordpress.mondomaine.fr;
      

        listen [::]:443 ssl; # managed by Certbot
        listen 443 ssl; # managed by Certbot
        ssl_certificate /etc/letsencrypt/live/wordpress.mondomaine.fr/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/wordpress.mondomaine.fr/privkey.pem; # managed by Certbot
        include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

        location / {
            proxy_pass         http://wpdocker;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
            proxy_set_header   X-Forwarded-Proto $scheme;
        }

    }

    server {
        if ($host = wordpress.mondomaine.fr) {
            return 301 https://$host$request_uri;
        } # managed by Certbot

       location / {
        rewrite ^/(.*)$  https://$host/$1 permanent;
      }
            listen 80;
            listen [::]:80;

            server_name wordpress.mondomaine.fr;
        return 404; # managed by Certbot
    }

    ```
Pour activer ce fichier de configuration, on créé un lien symbolique vers le dossier sites-enabled:

``` { .bash .copy }
sudo ln -s /etc/nginx/sites-available/wordpress.mondomaine.fr.conf /etc/nginx/sites-enabled/
```

On recharge la configuration de Nginx: 

``` { .bash .copy }
sudo systemctl reload nginx.service
```

***Et voilà, votre site est accessible à `https://wordpress.mondomaine.fr` !***

??? info "Plus d'infos sur la configuration de Nginx"
    Plus d'infos sur [le guide Nginx de Linode](https://www.linode.com/docs/web-servers/nginx/)
