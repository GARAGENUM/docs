# Introduction

Dans cette rubrique vous trouverez différentes astuces, mémos, snippets, tips, collectés par l'équipe du Garage Numérique.

Toutes les astuces sont regroupées par catégories:

- [les mémos pour bien utiliser les commandes linux](cmd/index.md)
- [les tutos d'administration du Poste de travail](admin/index.md)
- [les tutos d'administration Serveur](server/index.md)
- [les guides DevOps](devops/index.md)
- [les guides internes](internal/index.md)
- [les astuces de développeur](dev/index.md)
- [le guide Git](git/git.md)
- [le guide IA pour déployer un model localement](ia/local-ollama.md)
