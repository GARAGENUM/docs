# Utiliser Unity avec Codium

## Introduction

Unity est un moteur de jeux vidéos 2D et 3D, avec le langage C# (C Sharp).

Codium est la version open-source de Visual Studio Code.

Le tutoriel vous permet d'installer Codium, Unity, et de relier un projet Unity avec Codium pour bénéficier de l'auto-complétion dans Codium.

## Installer Unity

Utilisez [Appimage](https://docs.unity3d.com/Manual/GettingStartedInstallingHub.html)

## Installer Codium

Installer à partir des dépots officiels avec [apt](https://debian-facile.org/utilisateurs:sushy:tutos:visual-studio-codium)

## Installer .Net

``` { .bash .copy }
wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-3.1
```

## Installer Mono

``` { .bash .copy }
sudo apt install apt-transport-https dirmngr gnupg ca-certificates
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb https://download.mono-project.com/repo/debian stable-buster main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
sudo apt update
sudo apt install mono-complete
```

## Credits

Made with [Carlo Hamalainen's blog](https://carlo-hamalainen.net/2020/07/11/unity3d-setup-debian10-intellisense/)

