# Créer un mod sous Minetest


## Installer Minetest
### Partie 1: En tant qu'admin

#### Pour passer à un utilisateur admin : 

``` { .bash .copy }
su bellinuxien
```
#### installer flatpak pour avoir la dernière version de Minetest

``` { .bash .copy }
sudo apt update
sudo apt install flatpak geany
```

#### On quitte l'admin avec ++ctrl+d++


### Partie 2: en tant qu'utilisateur  

``` { .bash .copy }
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub net.minetest.Minetest
export MT=.var/app/net.minetest.Minetest/.minetest
```


#### Installer le jeu du Garage Numérique  
```
``` { .bash .copy }cd $MT
mkdir games
cd games
git clone https://gitlab.com/garagenum/minetest-lug9000
```

#### Créer un projet de mod Minetest
1. Créer un compte sur gitlab.com
2. cliquer sur Nouveau Projet


#### Cloner le projet sur son ordinateur  
Revenir dans le terminal et écrire:

``` { .bash .copy }
cd $MT 
mkdir mods && cd mods 
git clone https://gitlab.com/mon-pseudo/monprojet.git
cd monprojet
```

#### Créer le fichier init.lua

``` { .bash .copy }
geany init.lua&
``` 

***


Tu peux éditer le fichier init.lua en t'aidant de :  
- [le livre écrit par RubenWardy](https://rubenwardy.com/minetest_modding_book/en/index.html)  
- [la doc officielle](https://github.com/minetest/minetest/blob/master/doc/lua_api.txt)  
