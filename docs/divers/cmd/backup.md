# Sauvegarder des données

1. Lister les partitions: 
    ``` { .copy }
    lsblk
    ```
2. Identifier la partition contenant les fichiers à sauvegarder
3. Monter la partition
    ``` { .copy }
    mkdir /mnt/nom_du_dossier
    mount /dev/sdXy /mnt/nom_du_dossier
    ```
4. Cibler le dossier à sauvegarder
    ``` { .copy }
    ls /mnt/nom_du_dossier
    ```
5. Créer le dossier de réception sur le serveur de backup:
    ``` { .copy }
    ssh -p 5555 bellinuxien@192.168.1.75
    mkdir /home/bellinuxien/BACKUPS/nom-de-dossier
    exit
    ```
6. Copier le dossier utilisateur vers le dossier sur le serveur de backup:
    ``` { .copy }
    rsync -ah -e "ssh -p 5555" /mnt/nom_du_dossier bellinuxien@192.168.1.75:/home/bellinuxien/BACKUPS/nom_du_dossier/ --info=progress2 && sync
    ```
7. Rapatrier les fichiers du serveur de backup vers une machine:
    ``` { .copy }
    rsync -ah -e "ssh -p 5555" bellinuxien@192.168.1.75:/home/bellinuxien/BACKUPS/nom_du_dossier/ /mnt/nom_du_dossier --info=progress2 && sync
    ```
