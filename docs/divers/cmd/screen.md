# Screen

1.Installation  
``` { .bash .copy }
sudo apt install screen  
```

2.Pour démarrer une nouvelle session:  
``` { .bash .copy }
screen -S nom-de-la-session  
```

3.Pour laisser la session en arrière plan  
``` { .bash .copy }
++ctrl+a+d++  
```

4.Pour lister les sessions existantes:  
``` { .bash .copy }
screen -ls   
```

5.Pour se reconnecter :  
``` { .bash .copy }
screen -r nom-de-la-session   
```

**See [https://linuxize.com/post/how-to-use-linux-screen/](https://linuxize.com/post/how-to-use-linux-screen/)**  

** Pour splitter l'écran: [https://tomlee.co/2011/10/gnu-screen-splitting/](https://tomlee.co/2011/10/gnu-screen-splitting/)**