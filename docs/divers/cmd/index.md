# Mémos sur les commandes Linux

- [ssh](ssh.md)
- [screen](screen.md)
- [backup](backup.md)
- [rsync](backup.md)
- [nmcli](nmcli.md)
- [chroot](chroot.md)
- [lvm](lvm.md)
- [Monter une partition](mount.md)
- [read](read.md), pour permettre à l'utilisateur d'interagir avec un script (user prompt / user input)
- [heredoc](heredoc.md), le fameux `EOF`, pour afficher du contenu sur plusieurs lignes
- [listes et dictionnaires avec Bash](bash-lists.md)
- [Mise en place d'un raid](raid.md)