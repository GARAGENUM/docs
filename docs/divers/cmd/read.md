# Read

`read` est le programme Bash qui permet à l'utilisateur d'interagir avec un script (user prompt / user input).

C'est ce qui est utilisé par exemple, quand la commande `apt` nous demande de confirmer l'installation d'un logiciel.

Le principe c'est d'afficher un message à l'écran (avec `echo` par exemple), puis de lancer la commande pour capter la saisie de l'utilisateur.
On utilise ensuite la valeur saisie stockée dans une variable.

!!! tip
    Utilisez [`HereDoc`](heredoc.md) pour afficher du contenu sur plusieurs lignes!

#### Usage minimal
``` { .bash .copy }
visiteur@domani:~$ read
mot
visiteur@domani:~$ echo $REPLY
mot
```

#### Définissez la variable qui contient le résultat, au lieu du défaut `$REPLY`
``` { .bash .copy }
visiteur@domani:~$ read MAREPONSE
truc
visiteur@domani:~$ echo $MAREPONSE
truc
```

#### Utilisez ++space++, ou n'importe quel caractère pour délimiter la fin de la réponse 
``` { .bash .copy }
visiteur@domani:~$ read '-d '
truc visiteur@domani:~$

visiteur@domani:~$ read '-dA'
trucAvisiteur@domani:~$ 
```

#### Utiliser `read -s` pour garder secrète la saisie de l'utilisateur
``` { .bash .copy }
visiteur@domani:~$ read -s
visiteur@domani:~$ echo $REPLY
secret
```

#### Utilisez `read -p` pour afficher un message avant la saisie (pas de retour à la ligne)
``` { .bash .copy }
visiteur@domani:~$ read -p "Continuez ? (y/n) : "
Continuez ? (y/n) : y
```

#### Répéter l'invite de saisie tant que la valeur n'est pas correcte  
``` { .bash .copy }
    until [[ $VALUE ]] ; do
        read -p "Enter value for $var : " VALUE;
    done ; 
```

#### Avec une itération dans une liste

``` { .bash .copy }
for var in ${VAR_LIST[@]}; do
    VALUE=""
    until [[ $VALUE ]] ; do
        read -p "Enter value for $var : " VALUE;
    done ;
    echo "$VALUE" ;                                                                                                
done           