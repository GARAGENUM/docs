# nmcli

Scanner le reseau
``` { .bash .copy }
nmcli d wifi
``` 
Se connecter au reseau pour la première fois
``` { .bash .copy }   
nmcli d wifi connect <SSID> -a
```   
Se connection à un reseau existant
``` { .bash .copy }
nmcli con <SSID>
```
Changer les serveurs DNS  
Méthode degeulasse  
/!\ Peut poser problème pour les reseaux necessitant un portail de connection /!\
    
    # vim /etc/resolv.conf
    ----------
    nameserver 80.67.169.12
    nameserver 80.67.169.40
    ----------
    # chattr +i /etc/resolv.conf
    
Méthode clean  
/!\ À faire pour chaque point d'accès /!\
``` { .bash .copy }
nmcli con mod <SSID> ipv4.dns "80.67.169.12 80.67.169.40" ipv4.ignore-auto-dns yes
```
