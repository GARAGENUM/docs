# CHROOT

![illustration](../../assets/divers/server/chroot.png)

## Définition de chroot

CHROOT permet de travailler depuis un système en cours d'installation, alors que celui-ci ne dispose pas encore de composants indispensables comme le noyau (c'est alors le noyau du système hôte qui est utilisé, ce qui requiert certaines compatibilités minimales entre les deux systèmes). 

De même, la commande peut permettre de travailler sur un système devenu inaccessible par la voie classique (lancement du noyau par un chargeur d'amorçage au démarrage de la machine) à la suite d'un problème technique afin de tenter de le remettre en état de fonctionnement. 

## Utilisation

:bulb: Repérer le disque du système d'exploitation que l'on souhaite "chrooter"
``` { .bash .copy }
lsblk
```

- Monter les partitions nécessaires
``` { .bash .copy }
sudo mount /dev/sda2 /mnt # exemple pour un disque sda2

sudo mount --bind /dev /mnt/dev
sudo mount --bind /proc /mnt/proc
sudo mount --bind /sys /mnt/sys
sudo mount --bind /run /mnt/run
```

- Connexion:
``` { .bash .copy }
sudo chroot /mnt
```

:warning: A partir de cette commande, toutes les autres commandes saisies s'éffectuent sur le système cible

- Réparer le GRUB:
``` { .bash .copy }
grub-install /dev/sda
```

- Réparer le système d'exploitation:
``` { .bash .copy }
apt --fix-broken install
dpkg --configure -a
apt install -f
apt install --reinstall apt
```

- Pour se déconnecter:
``` { .bash .copy }
exit
```

- Démonter le système de fichier:
``` { .bash .copy }
sudo umount -a /mnt
```

## Documentation supplémentaire :books:

[ubuntu wiki](https://doc.ubuntu-fr.org/chroot)  

[Debian facile](https://debian-facile.org/doc:systeme:chroot)