# Unifier deux disques durs avec LVM

![disks](../../assets/divers/cmd/combine-hard-drives.png)

Pour unifier deux disques durs en un seul espace logique avec [LVM](https://doc.ubuntu-fr.org/lvm) (Logical Volume Manager)

## Prérequis

1. S'assurer que les deux disques (par exemple, `/dev/sdb` et `/dev/sdc`) ne contiennent pas de données importantes (ils seront formatés).
2. Installer les outils LVM:

```bash
sudo apt update
sudo apt install lvm2
```

## Workflow

#### Initialiser les disques en tant que volumes physiques

Convertir les deux disques en volumes physiques pour LVM :

```bash
sudo pvcreate /dev/sdb /dev/sdc
```

Vérifier l'état avec:

```bash
sudo pvdisplay
```

#### Créer un groupe de volumes

Combiner les deux volumes physiques dans un groupe de volumes:

```bash
sudo vgcreate vg_data /dev/sdb /dev/sdc
```

(Il est possible de remplacer `vg_data` par un autre nom pour le groupe de volumes)

Vérifier le groupe de volumes:

```bash
sudo vgdisplay
```

#### Créer un volume logique

Créer un volume logique dans le groupe de volumes. Par exemple, pour utiliser tout l'espace disponible:

```bash
sudo lvcreate -l 100%FREE -n lv_data vg_data
```

??? info "Infos sur la commande"
    === "`-l 100%FREE`"
        Pour utiliser tout l'espace disponible
    === "`-n lv_data`"
        c'est le nom du volume logique (dans cet exemple `vg_data`)

Vérifier le volume logique :

```bash
sudo lvdisplay
```

#### Formater et monter le volume logique

Formater le volume logique avec un système de fichiers:

```bash
sudo mkfs.ext4 /dev/vg_data/lv_data # pour créer une partition ext4
```

Créer un point de montage, puis monter le volume:

```bash
sudo mkdir /mnt/data
sudo mount /dev/vg_data/lv_data /mnt/data
```


#### Configurer le montage automatique au démarrage

Ajouter le volume dans le fichier `/etc/fstab` pour qu'il soit monté automatiquement :

```bash
sudo nano /etc/fstab
```

Ajouter la ligne:

```
/dev/vg_data/lv_data /mnt/data ext4 defaults 0 0
```

## Vérification

Pour vérifier que tout fonctionne, redémarrer le système et s'assurer que le volume est monté:

```bash
df -h
```
