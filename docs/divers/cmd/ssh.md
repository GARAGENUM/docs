# Mémo SSH

## Remove a key from known hosts:

``` { .bash .copy }
ssh-keygen -f "/home/user/.ssh/known_hosts" -R "key_name"
```

## Create a key:

``` { .bash .copy }
ssh-keygen -o -a 100 -t ed25519 -C user@email #create an ED25519 key
ssh-keygen -b 4096 -t rsa -C user@email #create a RSA key
ssh-add newkey #add newkey to your SSH agent
ssh-add *
ssh-add .
```

## List existing keys:

``` { .bash .copy }
ssh-add -l
``` 
## Display a key:

``` { .bash .copy }
cat /home/user/.ssh/key_name
```

## Display a key fingerprint (with random art image):

``` { .bash .copy }
ssh-keygen -lvf key_name
``` 

## Enable SSH agent:

``` { .bash .copy }
eval $(ssh-agent -s)
```

## Copy a key to a distant server:

``` { .bash .copy }
ssh-copy-id user@server #Copy all keys that are registered in SSH agent
ssh-copy-id -i newkey.pub user@server\n #Copy newkey.pub
```

## Remove a key:

??? note "Removing a key from a server"
    It can be done by editing your `/home/user/.ssh/authorized_keys` file.
    ``` { .bash .copy }
    nano /home/$(whoami)/.ssh/authorized_keys
    ```
    Type ++ctrl+k++ to delete the line
    
## Connect and copy from a different port number:

``` { .bash .copy }
ssh -p port user@server
scp -P port file user@server:way/
rsync -e ssh -avz /source server:/target
rsync -ave ssh server:/source /target
ssh-copy-id -i newkey.pub -p port user@server
```

## Connect a distant server in your file browser under Linux using SFTP:

Type the adress:

``` { .bash .copy }
ssh://user@server:port
```
   
*Voir aussi:* [Enabling a SSH key on a Synology NAS running DSM 6](https://gitlab.com/sakura-lain/ssh-synology-nas-dsm-6/)
