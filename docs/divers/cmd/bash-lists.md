# Les listes et les dictionnaires avec Bash

## Les listes (`indexed array`)

### Création d'une liste

```
declare -a MA_LIST
```

### Modification de la liste

1. Ajout d'éléments
    ``` { .copy }
    MA_LIST=(1er_element 2nd_element)
    MA_LIST+=("3eme élément avec des espaces")
    MA_LIST[3]="4eme élémént"
    ```
2. Changement de valeur d'un élément
    ``` { .copy }
    MA_LIST[0]="la nouvelle valeur qu'on met pour le 1er élément"
    MA_LIST[1]="la valeur pour le 2eme élément"
    ```
3. Traitement sur l'ensemble des éléments
    - en utilisant la liste des index `${!MA_LIST[@]}`:  
        ``` { .copy }
        for i in "${!MA_LIST[@]}" ; do
            echo "index =  $i -  value = ${MA_LIST[i]}"
        done  
        ```  
    - en utilisant la liste des valeurs `${MA_LIST[@]}`
        ``` { .copy }
        for i in "${MA_LIST[@]}" ; do
            echo "value = ${i}"
        done  
        ``` 
    - nombre d'éléments:   
        ``` { .copy }
        echo "nombre d'éléments :  ${MA_LIST[]}
        ```

## Les tableaux (`associative arrays`)

### Création d'un tableau
``` { .copy }
declare -A MY_ARRAY
```

1. Ajout d'éléments
    ``` { .copy }
    MY_ARRAY=([clé1]=1er_element [clé2]=2nd_element)
    MY_ARRAY+=([clé3]="3eme élément avec des espaces")
    MY_ARRAY[dernière_clé]="4eme élément"
    ```
2. Changement de valeur d'un élément
    ``` { .copy }
    MY_ARRAY[clé1]="la nouvelle valeur qu'on met pour l'élément associé à `clé1`"
    ```
3. Traitement sur l'ensemble des éléments
    - en utilisant la liste des clés `${!MY_ARRAY[@]}`:  
        ``` { .copy }
        for i in "${!MY_ARRAY[@]}" ; do
            echo "key =  $i -  value = ${MY_ARRAY[i]}"
        done  
        ```  
    - en utilisant la liste des valeurs `${MY_ARRAY[@]}`
        ``` { .copy }
        for i in "${MY_ARRAY[@]}" ; do
            echo "value = ${i}"
        done  
        ``` 
    - nombre d'éléments:   
        ``` { .copy }
        echo "nombre d'éléments :  ${MY_ARRAY[]}
        ```


!!! info 
    Pour aller plus loin (en anglais): 
      - [linuxjournal.com](https://www.linuxjournal.com/content/bash-arrays)
      - [le manuel bash](https://www.gnu.org/software/bash/manual/html_node/Arrays.html)
      - [opensource.com](https://opensource.com/article/18/5/you-dont-know-bash-intro-bash-arrays)