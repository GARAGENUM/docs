# Le rapprochement bancaire

Pour effectuer le rapprochement bancaire sur Odoo, il est nécessaire de disposer des relevés sous forme de tableur (.xls, .xlsx, .csv, .ods).

Si vous ne possédez les relevés qu'au format pdf, il vous faudra les convertir en tableur.

## Conversion en tableur

Vous pouvez utiliser le site [pdftables.com](https://www.pdftables.com) pour extraire un tableau de votre fichier .pdf .

![Page d'acceuil de pdftables.com, avec le bouton Importer mis en valeur](../../assets/admin/process-odoo/odoo-rapprochement-pdftables-importer.png)

Vous pouvez maintenant télécharger le tableur correspondant à votre pdf.

![Page d'export du document sur pdftables.com, avec le bouton Télécharger mis en valeur](../../assets/admin/process-odoo/odoo-rapprochement-pdftables-exporter.png)

## Formatage du tableur

Vous devez maintenant formater vote tableau pour qu'il soit correctement interprété par odoo.

Le tableau doit comporter les colonnes suivantes:  

| DATE | PAYMENT_REF | PARTNER | MEMO | AMOUNT |
| ------- | ------------ | ------------ | ------------ | ------------ |
| 12/03/2019 |  dzd    |      yt   |   yty   |  yty      |

