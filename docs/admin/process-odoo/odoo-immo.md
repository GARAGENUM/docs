# Saisir une immobilisation avec Odoo 14

On accède à l'application d'enregistrement des immobilisations par le menu __Facturation__ > __Comptabilité__ > __Gestion__ > __Assets__

![Capture d'écran d'odoo14, mettant en valeur le menu d'accès aux immobilisations](../../assets/admin/process-odoo/odoo_access_menu_assets.png)

## Configuration des immobilisations

### Création du journal dédié

![capture d'écran d'odoo 14, montrant le formulaire de création d'un journal, configuré pour les immobilisations](../../assets/admin/process-odoo/odoo_assets_create_journal.png)

### Création du type d'immobilisations

![Capture d'écran d'odoo 14, montrant le menu d'accès au formulaire de création d'un type d'immobilisations]()

![capture d'écran d'odoo 14, montrant le formulaire de création d'un type d'immobilisations, configuré pour l'immobilisation des logiciels]()


## Création de l'immobilisation

![Capture d'écran d'odoo 14, montrant le bouton "créer" dans le menu des immobilisations](../../assets/admin/process-odoo/odoo_create_asset_button.png)

