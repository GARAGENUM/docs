# Rapprochement bancaire avec Odoo 16

## Accéder à l'interface de rapprochement bancaire

À partir du Tableau de Bord de l'application Facturation, cliquer sur "Lettrer x articles"

![Capture d'écran du tableau de bord avec surlignage du bouton de lettrage](../../assets/admin/process-odoo/odoo16-rapprochement-bancaire_dashboard-button.png)

## Sélectionner la ligne de relevé à lettrer

Cliquer dans le panneau de gauche sur la ligne de relevé bancaire à lettrer (encadré rouge), les opérations de la pièce apparaissent dans le bandeau en haut à droite (encadré bleu)

![Capture d'écran de l'interface de lettrage, avec un surlignage sur la ligne de relevé sélectionnée](../../assets/admin/process-odoo/odoo16-rapprochement-bancaire_select-statement-line.png)

## Sélectionner la ligne de facture correspondante

Dans le panneau en bas à droite, sélectionner la ligne de facture correspondante (encadré rouge). La correspondance s'affiche en haut à droite (encadré bleu).

![Capture d'écran de l'interface de lettrage, avec en rouge un encadré autour de la ligne de facture qui correspond à la ligne de relevé, et en bleu un encadré qui montre les lignes en correspondance](../../assets/admin/process-odoo/odoo16-rapprochement-bancaire_select-matching-line.png)

## Valider le lettrage

Cliquer sur le bouton Valider en haut à gauche pour confirmer le lettrage.