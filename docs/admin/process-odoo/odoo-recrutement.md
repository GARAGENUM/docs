# Gérer le recrutement dans Odoo

## État de réalisation

???example "Bandeau à dérouler pour voir l'état de réalisation"

    === "Fait"

    === "En cours"
        - [Configuration de l'envoi d'emails pour les différentes étapes](#21-configuration-des-emails-pour-les-differentes-etapes)
    === "à faire"
        - [Configuration de l'envoi d'emails en cas de refus]()
        - [Configuration d'envoi d'emails pour la prise de rendez-vous]()


## Configuration

### Configuration des emails pour les différentes étapes  

???example "Modifications à réaliser en mode développeur dans __Paramètres__ > __Technique__ > __Séquences et identifiants__ > __Identifiants externes__ " 
    1. Ouvrir la fiche de l'identifiant __hr_recruitment.stage_job1__
        1. Cliquer sur __Initial Qualification__ dans le champ __Enregistrement__
        2. Éditer cet enregistrement et donner la valeur "Applicant: Acknowledgement" dans le champ __email template__
        3. Sauvegarder
        4. Cliquer sur la valeur du champ __Email Template__ pour modifier le modèle d'email.  
    2. Ouvrir la fiche de l'identifiant __hr_recruitment.stage_job2__  
        1. Cliquer sur __First Interview__ dans le champ __Enregistrement__
        2. Éditer cet enregistrement et vider le champ __Email Template__
        3. Sauvegarder
    3. 
