# Créer une note de frais

!!!warning "Pour créer une note de frais, il faut au préalable avoir [créé et configuré un employé](../odoo-setup/#53-creation-dun-employe), ainsi que [des articles insérables dans une note de frais](../odoo-setup/#52-creation-dun-article)."

## Création de la note de frais

1. Utiliser le menu __Dépenses__ > Créer.  
2. ![Capture d'écran de la fihce de création d'une note de frais, centrée sur le champ Payé par Employé à rembourser](../../assets/admin/process-odoo/odoo-expense-create.png){ align=right }Remplir le formulaire en choisissant __Payé par: Employé (à rembourser)__, et en sélectionnant l'employé correspondant.
3. Cliquer sur __Sauvegarder__.
4. Cliquer sur __Joindre le reçu__ pour joindre le ticket de caisse ou la facture.

## Création du rapport

Les notes de frais doivent être soumises à validation par le biais d'un rapport. Celui-ci peut contenir une ou plusieurs notes de frais.

1. Cliquer sur le bouton __Créer le rapport__  
2. En cliquant sur le bouton __Modifier__, il est possible de rajouter d'autres notes de frais existantes au rapport   
3. Cliquer sur __Soumettre au responsable__  

## Validation du responsable

Selon la configuration de la fiche Employé, le responsable de la validation des notes de frais est le supérieur hiérarchique de l'employé, ou un autre employé désigné pour valider les notes de frais de l'employé ayant réalisé des dépenses.  

Une fois que le rapport est en status __Soumis__, le responsable le voit apparaître dans le menu __Dépenses__ > __Notes de frais__ > __Notes de frais à approuver__.  

Dans la fiche du rapport, le validateur a la possibilité de cliquer sur __Approuver__, __Refuser__ ou __Remettre en brouillon__.  

Une fois le rapport approuvé, le validateur peut cliquer sur __Comptabiliser les écritures__.

## Remboursement de la dépense

Le rapport approuvé et comptabilisé (la pièce comptable correspondante est créée), le validateur peut cliquer sur le bouton __Enregistrer un paiement__ pour rembourser le salarié ayant réalisé l'avance.

Choisir le journal __Banque__ ou __Espèces__ selon le moyen de paiement.
