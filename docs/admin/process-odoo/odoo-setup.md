# Paramétrer Odoo

Le paramétrage suivant doit permettre l'utilisation d'odoo pour l'ensemble des travaux comptables standards.

## État de la documentation   
???example "Liste des processus documentés / en cours"

    === "En cours"
        - [x] [Installation des applications officielles](#21-installation-des-applications-officielles)
        - [x] [Installation des modules complémentaires](#22-installation-des-modules-communautaires)
        - [x] [Modification du module de vente pour les Factures à Établir](#31-module-des-ventes)  

    === "Fait"
        - [x] [Configuration des taxes](#41-parametrage-des-taxes)
        - [x] [Paramétrage de la compta dans les paramètres généraux](#42-parametres-generaux)
        - [x] [Paramétrage des journaux](#43-parametrage-des-journaux)
        - [x] [Configuration de la facturation](#44-configuration-de-la-facturation)
        - [x] [Création d'un employé](#45-creation-dun-employe)


## Installation des applications et modules

### Installation des applications officielles
???example "Installer des applications à partir du store officiel"

    #### Utiliser le menu Applications
    Aller dans le menu Applications, et installer les applications et modules listées dans le tableau ci-après.

    ![Page du magasin d'applications dans Odoo, avec les applications Vente et Facturation mises en valeur](../../assets/admin/process-odoo/odoo-setup-apps-vente-facturation.png)

    #### Liste des applications à installer

    === "Odoo 14"
        | Nom de l'application  | Nom technique  | description |
        |---------|----------|---------------------------------|
        | Vente   | sale_management | Gestion des ventes       |
        | Facturation | account     | Gestion des factures     |
        | Achats  | purchase        | Gestion des achats       |
        | Employés | hr             | Gestion des employés et bénévoles |
        | Contacts | contacts       | Gestion des adresses     |
        | Dépenses | hr_expenses    | Gestion des notes de frais |
        | Projet  | project         | Kanban pour le suivi de projets |
        | Inventaire | stock        | Gérer les stocks et les immobilisations |
        | Membre     | member       | Gérer les membres et les adhésions  |

    === "Odoo 15"
        | Nom de l'application  | Nom technique  | description |
        |---------|----------|---------------------------------|
        | Vente   | sale_management | Gestion des ventes       |
        | Facturation | account     | Gestion des factures     |
        | Achats  | purchase        | Gestion des achats       |
        | Employés | hr             | Gestion des employés et bénévoles |
        | Contacts | contacts       | Gestion des adresses     |
        | Notes de frais | hr_expenses    | Gestion des notes de frais |
        | Projet  | project         | Kanban pour le suivi de projets |
        | Stock | stock        | Gérer les stocks et les immobilisations |
        | Membre     | member       | Gérer les membres et les adhésions  |

### Installation des modules communautaires
???example "Installer des modules complémentaires"

    #### Exemple avec le module de comptabilité

    Aller sur [le site web du magasin d'applications communautaires](https://apps.odoo.com/apps/modules/browse?price=Free&series=14.0&search=account) pour installer le module "Odoo 14 Accounting", dont le nom technique est "om_account_accountant".

    ![Page d'accueil du magasin d'applications odoo sur le web, avec les résultats de la recherche incluant le module recherché](../../assets/admin/process-odoo/odoo-setup-webshop.png)

    ![Page de présentation du module om_account_accountant dans le magasin d'applications odoo sur le web, avec le bouton Télécharger mis en valeur](../../assets/admin/process-odoo/odoo-setup-webshop-install-accounting-module.png)

    Après avoir téléchargé le module, il faut dézipper son contenu dans le dossier des modules complémentaires de votre installation Odoo. Ensuite, après avoir activé le mode de développeur, vous pouvez cliquer sur le  bouton "Mettre à jour la liste des applications" dans le menu des Applications.
    Vous pouvez maintenant installer l'application-tierce "om_account_accountant"

    Vous pouvez trouver d'autres modules communautaires sur [le site web du magasin d'applications communautaires](https://odoo-community.org/shop) en utilisant les filtres.
    ![Page d'accueil du magasin d'applications communautaires sur le web, avec les filtres de catégorie et de version mis en valeur](../../assets/admin/process-odoo/odoo-setup-community-shop.png)

    #### Liste des modules à installer


    === "Odoo 14"
        | module  | description | website |
        |---------|----------|------------|
        | bi_import_bank_statement_line | import de relevés csv |
        | account_reconciliation_widget | reconciliation des factures et relevés bancaires | https://odoo-community.org/ |
        | account_partner_reconcile | réconciliation des partenaires | https://odoo-community.org
        | hr_expense_advance_clearing | Faire une avance en vue du paiement de notes de frais | https://apps.odoo.com |
        | hr_expense_analytic_distribution | Éclater une note de frais sur plusieurs comptes analytiques grâce à des clés en pourcentage | https://apps.odoo.com |
        | hr_expense_cancel | Permet d'annuler et défaire le lettrage en cas d'erreur sur les notes de frais | https://odoo-community.org/ |
        | l10n_fr_siret | Sépare SIREN et NIC | https://apps.odoo.com/apps/modules |
        | l10n_fr_siret_lookup | Rechercher les informations sur une société à partir de l'API de la base SIREN | https://apps.odoo.com/apps/modules |
        | om_account_followup | Suivi automatique des impayés et relances clients | https://apps.odoo.com |
        | account_payment_mode | Flux de paiement (nécessaire pour donation)  | https://apps.odoo.com |
        | donation_base | Base du module donation (nécessaire pour donation) | https://odoo-community.org/ |
        | donation  | Gérer les dons d'argent  | https://odoo-community.org |
        | account_payment_credit_card | Plusieurs cartes de crédit pour le même compte en banque | https://apps.odoo.com |
        | hr_expense_journal | Sélectionner le journal en fonction du mode de paiement des notes de frais, permet d'utiliser les cartes de crédit de la société pour régler les notes de frais | https://apps.odoo.com |
        | sale_advance_payment | Permet d'enregistrer des paiements d'avance sur des ventes | https://odoo-community.org |
        | purchase_advance_payment | Permet d'enregistrer des paiements d'avance sur des achats | https://odoo-community.org |
        | membership_variable_period | Période glissante pour les adhésions | https://apps.odoo.com/ |
        | membership_withdrawal  | Gestion des dé-adhésions | https://apps.odoo.com/ |
        | membership_extension | Amélioration du module membership basique | https://apps.odoo.com |
        | account_move_line_purchase_info | créer une écriture comptable dès la commande de manière à enregistrer les biens parvenus non-facturés au cut-off | https://apps.odoo.com |
        | account_move_line_sale_info | Créer des écritures comptables dès la vente de manière à enregistrer les biens vendus non-facturés au cut-off | https://apps.odoo.com |
        | om_account_followup | Suivi automatique des impayés et relances clients | https://apps.odoo.com/ |
        | account_move_force_removal | Permet de supprimer des entrées comptabilisées | https://odoo-community.org/ | 
        | account_cutoff_base | Pour réaliser les écritures de fin d'exercice | https://apps.odoo.com |
        | account_invoice_start_end_dates | Pour avoir une date de début et une date de fin sur les factures | https://apps.odoo.com |
        | account_cutoff_start_end_dates | Utilise la date de début et de fin sur les factures pour réaliser les écritures de clôture | https://apps.odoo.com |
        | account_cutoff_accrual_subscription | Créer des revenus différés à partir d' "abonnements" | https://apps.odoo.com |
        | sale_start_end_dates | Ajoute date de début et date de fin sur les devis | https://apps.odoo.com |
        | purchase_subscription | Abonnements pour le suivi des factures à venir | https://apps.odoo.com |
        |  sale_start_end_dates | Ajoute des lignes de début et de fin sur les devis, qui seront reprises dans les factures | https://apps.odoo.com |
        |  sale_order_invoice_amount | Ajoute les informations "montant facturé" et "montant non-facturé" sur les commandes | https://apps.odoo.com |
        |  hr_expense_invoice  | Permet de saisir les factures fournisseurs qui sont présentes dans une note de frais |  https://apps.odoo.com |
        | account_move_line_sale_info | 

    === "Odoo 15"
        | module  | description |
        |---------|----------|
        | l10n_fr_siret | Sépare SIREN et NIC |
        | account_statement_import | import de relevés csv |
        | account_reconciliation_widget | reconciliation des factures et relevés bancaires |
        | account_move_line_purchase_info | créer une écriture comptable dès la commande de manière à enregistrer les biens parvenus non-facturés au cut-off |
        | account_move_line_sale_info | Créer des écritures comptables dès la vente de manière à enregistrer les biens vendus non-facturés au cut-off |
        | om_account_followup | Suivi automatique des impayés et relances clients |
        | sale_advance_payment | Permet d'enregistrer des paiements d'avance sur des ventes |


## Customisation de modules

### Module des ventes

???example "Une modification du code à la main en attendant de mettre en place un module dédié"  
    Nous avons besoin d'être en mesure de modifier la date de validation de commande, qui se positionne automatiquement à la date du jour où on valide le devis, et qu'on ne peut pas modifier après.
    Modifier la date de commande permettra à la commande d'être prise en compte pour déterminer l'écriture des Factures Non Parvenues en fin d'exercice.

    Pour cela, on modifie le fichier `/usr/lib/python3/dist-packages/odoo/addons/sale/models/sale.py` dans le container odoo.  

    Remplacer la ligne 171:  
    ```
    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")  
    ```  
    par ceci:  
    ```
    172:    date_order = fields.Datetime(string='Order Date', required=True, readonly=True, index=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'sale': [('readonly', False)]}, copy=False, help="Creation date of draft/sent orders,\nConfirmation date of confirmed orders.")
    ```  

    Il faut ensuite relancer odoo avec `docker-compose restart`.

## Configuration d'odoo

### Paramétrage des taxes

#### Créer des taxes
???example "Aller dans le menu __Facturation__ > __Configuration__ > __Comptabilité__ > __Taxes__"
    Sélectionner la taxe "TVA déductible (achat) 20,0%" et cliquer sur Action > Dupliquer.

    Renommer la taxe en "TVA non-déductible 20%" et supprimer dans le tableau de répartition le compte de TVA et la grille de taxe. Modifier l'étiquette dans les paramètres avancés

    Faites de même avec la TVA non-déductible 5,5%, ainsi qu'avec des TVA non-déductibles intégrées (la TVA est calculé en déduction du prix donné). Pour cela, il faut cocher la case "integré dans le prix" dans les options avancées.

    Créer ensuite un taux pour les entrepreneurs et les associations, à 0% avec le libellé "TVA 0%".

#### Créer une position fiscale
???example "Aller dans le menu __Facturation__ > __Configuration__ > __Comptabilité__ > __Positions Fiscales__"
    ![Capture d'écran du formulaire de création d'une nouvelle position fiscale, avec les éléments à remplir mis en valeur: Nom de la position fiscale(Association - France), Notes officielles: TVA non-applicable, article 293B du CGI, Détecter Automatiquement: False](../../assets/admin/process-odoo/odoo-setup-position-fiscale.png)

    !!! warning "Attention"
        Cette étape n'est nécessaire que si la position fiscale doit être définie différement pour chaque client.
        Sinon, il suffit de rajouter la ligne  
        ```
        TVA non-applicable, article 293B du CGI
        ```
        dans les conditions de paiement ([voir la configuration de la facturation](#definition-des-conditions-de-reglement)).

### Paramètres généraux

???example "Dans le menu __Configuration__, allez à l'onglet __Accounting__ et sélectionnez les réglages suivants:"

    1. Choix du Plan Comptable  
    ![Capture d'écran du menu de configuration d'odoo avec le réglage du Plan Comptable mis en valeur](../../assets/admin/process-odoo/odoo-setup-pcgfr.png)   
    2. Activation de la comptabilité analytique
    ![Capture d'écran du menu de configuration d'odoo avec le réglage de la comptabilité analytique mis en valeur](../../assets/admin/process-odoo/odoo-setup-analytic.png)

### Paramétrage des journaux

???example "En commençant par la banque"
    #### Paramétrage de la banque

    ##### Création d'une banque  

    ![Capture d'écran montrant le bouton à cliquer pour ouvrir une vue depuis le menu développeur](../../assets/admin/process-odoo/odoo-setup-open-view.png){width="200", align=right} Après avoir activé les outils développeur, pour créer une banque, il faut aller dans les **outils de déboguage** > **Ouvrir la vue** > Sélectionner la vue **res.bank.form**.  

    Replissez les informations de votre agence (nom, adresse et BIC).

    Vous pouvez voir la liste des banques créées par la vue **res.bank.tree**.

    ##### Création d'un compte bancaire  

    Aller au menu **Facturation > Configuration > Banques > Ajouter un compte bancaire**

    ##### Renommage du journal de Banque

    Un journal portant comme nom le numéro du compte bancaire a été créé. Il est possible de modifier le nom du journal pour plus de lisibilité

    #### Création du journal des chèques

    Pour suivre les chèques reçus, il est nécessaire de créer un journal dédié, de type "Banque" et nommé "Chèques".

    #### Vérification des journaux

    Aller au menu **Facturation > Configuration > Journaux**.
    Vérifier que pour journal, les __comptes d'attente__, __Paiements sortant__ et __Paiements entrant__ sont bien configurés.

### Configuration de la facturation

???example "Créer un fournisseur, un article et des conditions de réglement"
    #### Définition des conditions de réglement

    1. Aller au menu __Facturation__ > __Configuration__ > __Facturation__ > __Conditions de Paiement__
    2. Ouvrir la condition "30 jours" et modifier la description pour obtenir le contenu suivant:  
    ```
    Date limite de réglement: 30 jours nets à compter de la date d'émission de la facture
    ```
    3. Aller dans le menu __Configuration__ > __Facturation__ > __Factures clients__ et remplissez le champ Termes et conditions par défaut avec le contenu suivant: 
    ```
    Escompte en cas de paiement anticipé: aucun
    Taux des pénalités en l'absence de paiement: taux légale en vigueur à la date d'émission de la facture
    En cas de retard de paiement, indemnité forfaitaire légale pour frais de recouvrement: 40,00€
    ```  

    #### Création d'un fournisseur

    1. Aller au menu Facturation > Fournisseurs > Fournisseurs > Créer
    2. Saisssez le nom de la société, puis cliquez sur Sauvegarder.
    3. Cliquez sur le bouton Action > Rechercher API Sirene
    4. Dans la boîte de dialogue, cliquez sur Rechercher ou modifiez le champ "Nom à Chercher". Vous pouvez chercher à partir du nom de l'entreprise, de son Siret, son adresse, etc.  
    5. Cliquez sur la ligne correspondante puis validez avec le bouton Sélectionner.
    6. Cliquez sur Modifier pour compléter ou corriger les informations obtenues sur l'api Siren.

    #### Création d'un article

    Vous pouvez créer des articles pouvant être achetés, vendus, et/ou integrés dans une note de frais.

    Plusieurs menus vous permettent de créer un article:    
    - Facturation > Fournisseurs > Articles > Créer  
    - Facturation > Clients > Articles > Créer  
    - Dépenses > Configuration > Articles de notes de frais  

    1. Définissez d'abord le type d'article: **Service**, **Consommable** ou **Article Stockable**, ainsi que la catégorie: Expenses / Purchases / all.  
    2. Cochez les cases associées: __Peut être acheté__, __Peut être vendu__, __Peut être inséré dans une note de frais__
    2. Définissez le **compte de charges** et/ou le **compte de produits** correspondant à l'article dans l'onglet comptabilité.  
    3. Dans l'onglet Achats, définissez la Politique de contrôle des Factures Fournisseurs comme **"Sur les quantités commandées"**.


### Création d'un employé

???example "L'utilisation de l'application Employés nous permet de gérer la paye, mais aussi les avances et notes de frais."

    Il y a deux manières de créer un employé:  
    - à partir d'un utilisateur existant (bouton "Créer un employé" sur la fiche de l'utilisateur)
    - via le menu Employés > Créer

    Remplir les informations liées à l'employé, puis dans l'onglet Informations personnelles, cliquer sur le champ __Contact privé__ > __Adresse__ et écrire le prénom et le nom du salarié avant de cliquer sur __Créer__.  
    ![Capture d'écran de la fiche Employée, avec le menu de création d'adresse personnelle mis en valeur](../../assets/admin/process-odoo/odoo-setup-create-employee-address.png)

    Vous pouvez sauvegarder la fiche de l'employé.

    Il faut maintenant utiliser l'application __Contacts__ pour définir les informations comptables liées au salarié.

    Trouver la fiche adresse de l'employé nouvellement créé, puis cliquer sur __Modifier__.

    Dans l'onglet __Accounting__, modifier le __Compte fournisseur__ pour le définir à __467xxx Comptes débiteurs et créditeurs divers__. 

    !!!warning "Attention, il faut que le compte 467 utilisé ait été configuré avec le type __Payable__ pour pouvoir être sélectionné."

