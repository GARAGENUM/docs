# Paramétrer la Paye dans Odoo

## Création d'un nouveau taux de cotisation

Prenons l'exemple de la Contribution à la Formation Professionnelle, mensualisée à partir de Janvier 2022.

### Création de la règle pour la cotisation

???example "On crée une règle à partir du menu __Employés__ > __Configuration__ > __Règles de salaire__ " 

    1. On créé une règle nommée __Contribution à la Formation Professionnelle__, avec le code __CFP__, dont le parent est __Contributions Patronales (COTPAT)__.
    2. On coche les cases __Valide__ et __Apparaît dans le bulletin de paye__.
    3. On met le code Python suivant comme condition:
    ```
    if str(payslip.date_from) >="2022-01-01":
        result = True
    else:
        result = False
    ```
    4. On met la valeur à `result = BRUT * CFP / 100`
    
    !!!danger "5. On met un numéro de séquence supérieur à 1."

### Création de la catégorie pour le taux de cotisation

???example "On crée une catégorie à partir du menu __Employés__ > __Configuration__ > __Règles de salaire__"

    On crée une catégorie nommée __Taux de Contribution à la Formation Professionnelle__, avec le code __TXCFP__, dont le parent est __Taux (TX)__.

### Création des règles pour l'évolution du taux de cotisation

???example "On créé une règle à partir du menu __Employés__ > __Configuration__ > __Règles de salaire__"

    ???example "On créé d'abord la règle principale"

        1. On créé une règle nommée __Taux de Contribution à la Formation Professionnelle__, avec le code __TXCFP__.
        2. On sélectionne la catégorie __Taux de Contribution à la Formation Professionnelle (TXCFP)__.
        3. On coche la case __Valide__ et on décoche __Apparaît dans le bulletin de salaire.
        4. On met le code Python suivant comme condition:
        ```
        if str(payslip.date_from) >="2022-01-01":
            result = True
        else:
            result = False
        ```
        5. On met comme valeur:  
        ```
        result = categories.TXCFP
        ```
        !!!danger "6. On met le numéro de séquence à __1__."

    ???example "On crée une règle enfant dans l'onglet __Règles Enfants__"

        1. On créé une règle nommée __Taux de Contribution à la Formation Professionnelle (2022)__, avec le code __TXCFP22__.
        2. On sélectionne la catégorie __Taux de Contribution à la Formation Professionnelle (TXCFP)__.
        3. On coche la case __Valide__ et on décoche __Apparaît dans le bulletin de salaire.
        4. On met le code Python suivant comme condition: 
        ```
        if str(payslip.date_from) >= "2022-01-01" and str(payslip.date_from) < "2023-01-01":
            result = True
        else:
            result = False
        ```
        5. On définit une valeur fixe: __0.55__ .
        !!!danger "6. On met le numéro de séquence à __0__."

### Intégration des règles dans la structure de salaire


!!!warning "Ne pas oublier d'ajouter la règle "Taux de Contribution à la Formation Professionnelle (TXCFP)" dans la liste des règles des structures de salaires concernées."

    On utilise pour cela le menu __Paye__ > __Configuration__ > __Structures de salaires__.