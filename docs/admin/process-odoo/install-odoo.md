# Installation d'odoo

On réalise l'installation d'Odoo avec docker-compose et Nginx en reverse-proxy

## Docker compose

??? note "Voici le contenu du docker-compose"
    ```
    version: '3.7'
    services:
    web:
        image: odoo:14
        depends_on:
        - db
        ports:
        - "8098:8069"
        - "2098:8072" # live chat
        tty: true
        #command: -- --dev=reload
        #command: odoo -u account,base,web -d odoo
        volumes:
        - odoodev-14-data:/var/lib/odoo
        - odoodev-14-config:/etc/odoo
        - odoodev-14-addons:/mnt/extra-addons
        environment:
        - HOST=db
        - PORT=5432
        - USER=odoo
        - PASSWORD=odoo
    db:
        image: postgres:13
        ports:
        - "5498:5432"
        environment:
        - POSTGRES_DB=postgres
        - POSTGRES_PASSWORD=odoo
        - POSTGRES_USER=odoo
        - PGDATA=/var/lib/postgresql/data/pgdata
        volumes:
        - odoodev-14-dbdata:/var/lib/postgresql/data

    volumes:
    odoodev-14-data:
        driver_opts:
        type: none
        device: ./odoodev-14-data
        o: bind
    odoodev-14-config:
        driver_opts:
        type: none
        device: ./odoodev-14-config
        o: bind
    odoodev-14-addons:
        driver_opts:
        type: none
        device: ./odoodev-14-addons
        o: bind
    odoodev-14-dbdata:
        driver_opts:
        type: none
        device: ./odoodev-14-dbdata
        o: bind
    ```

???note "Et voici le contenu du fichier `./odoodev-14-config/odoo.conf`"
    ```
    [options]
    ; This is the password that allows database operations:
    admin_passwd = Mot_de_passe_pour_protéger_la_base_de_données
    db_host = db
    db_port = 5432
    db_user = odoo
    db_password = odoo
    addons_path = /mnt/extra-addons
    without_demo = True
    proxy_mode = True
    max_cron_threads = 1
    #db_filter = ^odoo$
    #list_db = False
    ```

Vous pouvez déployer le container avec la commande `docker-compose up -d`.

## Formulaire d'initialisation

Dans la navigateur, remplissez les informations telles que saisies dans le fichier __odoo.conf__ (mote de passe administrateur = admin_passwd, le nom de la base de données est odoo).

Vous pouvez maintenant décommenter les 2 dernières lignes du fichier __odoo.conf__, et redémarrer votre instance avec `docker-compose restart`.


## Installation des modules

Une fois les modules communautaires téléchargés dans le dossier des addons, il faut entrer dans le conteneur pour lancer l'installation des modules:  
```
user@host:~$ docker-compose exec -u odoo web bash
odoo@odoo-container:/$ find /mnt/extra-addons/ -mindepth 1 -maxdepth 1 -type d -printf "%f," | sed -E 's/(.*),/\1/' |xargs odoo -d odoo -i
```

