# Clôture d'exercice

Pour clôre l'exercice, il faut s'assurer de la justesse de plusieurs types d'opérations.

Au préalable, vous devez vous assurer d'avoir __saisi toutes les factures fournisseurs et factures clients__, et que vous avez __importé et lettré vos relevés bancaires et registres de caisse__.

??? danger "Vérifier les factures fournisseurs non-payés"

    Aller au menu __Facturation__ > __Fournisseurs__ > __Factures__,  
    puis grouper les factures par __Année de facturation__ > __Statut__ > __État du paiement__

??? danger "Vérifier les factures clients non-payées"

??? danger "Chercher les paiements enregistrés dans le mauvais journal"

    Pour cela, il faut aller dans le menu __Facturation__ > __Comptabilité__ > __Journaux__ > __Banque et liquidités__.  
    Vous pouvez alors filtrer les écritures pour ne garder que celles concernant la caisse (chercher __53__ dans __Comptes__).    

    Il faut ensuite grouper les écritures comptables par: __Statut__ > __Lettré__ > __Journal__.  
    Si des écritures lettrées apparaissent dans 2 journaux différents, il faut corriger les pièces correspondantes.    
    Pensez à corriger le paiement et la pièce comptable issue du rapprochement bancaire.  

    Si le paiement a été réalisé en espèces, mais qu'il apparaît en fait sur le relevé bancaire, il faut supprimer le paiement, supprimer le lettrage du relevé bancaire et lettrer ensuite le relevé bancaire directement avec la facture.  
    Faites ensuite de même avec la banque (en cherchant __512__), et les chèques.

??? danger "Réaliser les écritures de clôture"

    On utilise pour cela le menu __Facturation__ > __Comptabilité__ > __Arrêtés__  
    ??? example "Factures à établir"  
        Créer un nouvel arrêté à la date de clôture du dernier exercice.
    ??? example "Factures non parvenues"  
        Créer un nouvel arrêté à la date de clôture du dernier exercice.
    ??? example "Produits constatés d'avance"
        Créer un nouvel arrêté à la date de clôture du dernier exercice.
    ??? example "Charges constatés d'avance"
        Créer un nouvel arrêté à la date de clôture du dernier exercice.