# Saisir une facture avec Odoo14

## Accéder au menu Factures et créer un nouvelle facture

![capture d'écran de la page d'accueil du menu facturation d'Odoo, avec un encadré rouge pour mettre en valeur le sous-menu Fournisseurs / factures](../../assets/admin/process-odoo/odoo-facture-menu.png)

![capture d'écran de la page d'accueil des factures fournisseurs dans Odoo, avec un encadré rouge pour mettre en valeur le bouton "Créer une facture"](../../assets/admin/process-odoo/odoo-facture-bouton-creer.png)

## Enregistrer la facture

![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec des explications sur les champs à remplir](../../assets/admin/process-odoo/odoo-facture-initialiser.png)

![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec des explications sur les champs à saisir pour ajouter un article et calculer le montant](../../assets/admin/process-odoo/odoo-facture-ajouter-article.png)

## Valider la facture

1. On clique sur le bouton sauvegarder  
    ![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec un encadré rouge pour mettre en valeur le bouton "Sauvegarder"](../../assets/admin/process-odoo/odoo-facture-sauvegarder.png)
2. On ajoute comme pièce jointe le pdf de la facture originale
    ![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec un cadre rouge pour mettre en valeur les boutons permettant de joindre un fichier à la pièce comptable](../../assets/admin/process-odoo/odoo-facture-attachment.png)
3. On comptabilise la pièce si elle correspond parfaitement à la facture
    ![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec un cadre rouge pour mettre en avant le bouton "Confirmer", qui permet de comptabiliser une facture](../../assets/admin/process-odoo/odoo-facture-confirm.png)

## Payer la facture
1. On lance la création d'un paiement
    ![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec un cadre rouge pour mettre en avant le bouton "Enregistrer un paiement"](../../assets/admin/process-odoo/odoo-facture-addpayment.png)
2. On valide les informations du paiement    
    ![Capture d'écran du formulaire de saisie d'une facture dans Odoo, avec le fenêtre modale qui permet d'éditer un paiement associé](../../assets/admin/process-odoo/odoo-facture-payment-details.png)