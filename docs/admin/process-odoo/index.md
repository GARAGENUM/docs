# Procédures de comptabilité avec Odoo

!!!important "Nouveau: [Guide de paramétrage pour odoo 16](odoo16-setup)"

!!!important "Nouveau: Autres guides pour Odoo 16"

    - [Le rapprochement bancaire](odoo16-rapprochement-bancaire.md)

Cette rubrique regroupe les procédures à suivre pour saisir la comptabilité sur Odoo

 - [Installation Odoo](install-odoo)
 - [Paramétrage](odoo-setup)
 - [Paie](odoo-paye)
 - [Saisir une facture](odoo-facture)
 - [Rapprochement bancaire](odoo-rapprochement)
 - [Note de frais](odoo-note-de-frais)
 - [Subventions](odoo-subventions)
 - [Clôture d'exercice](odoo-cutoff)
 - [Immobilisations](odoo-immo)
 - [Recrutement](odoo-recrutement)


## Etat de réalisation  

???example "Dérouler ce bandeau pour accéder à la liste des états"  

    === "En cours"
        - [Paramétrage général d'odoo](odoo-setup)
        - [Saisir une facture](odoo-facture)
        - [Effectuer le rapprochement bancaire](odoo-rapprochement)
        - [Créer une note de frais](odoo-note-de-frais)
        - [Enregistrer une subvention](odoo-subventions)
        - [Opérations de clôture](odoo-cutoff)
        - [Enregistrement des immobilisations](odoo-immo)

    === "À faire"  
        - [Gérer l'inventaire](odoo-inventaire)
        - [Gérer les dons](odoo-dons)
        - [Enregistrer les contrats](odoo-contrats)
        - [Gérer le recrutement](odoo-recrutement)