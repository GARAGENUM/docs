# Procédures RH

La rubrique suivante contient des guides pour les démarches administratives liées aux Ressources humaines:  

- [Saisir un arrêt de travail](arret-travail.md)
- [Création d'un employé dans Odoo](creation-employe.md)
- [Remplissage des bordereaux Uniformation](bordereau-uniformation.md)
- [Méthodes calcul arrêt maladie](differents-mode-de-calcul-arret-travail.md)