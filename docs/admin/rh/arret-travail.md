# Saisie d'un arrêt de travail

La saisie d'un arrêt de travail se fait en plusieurs étapes:  

- [Analyse des éléments]()
- [Déclaration de l'arrêt sur Net-entreprise]()
- [Déclaration de l'arrêt sur le chèque emploi associatif]()
- [Déclaration de la reprise sur le chèque emploi associatif]()
- [Création du volet social]()
- [Enregistrement dans Odoo]()

## Analyse des éléments

???example "On vérifie les élements grâce à la documentation fournie par le syndicat employeur sur hexopee.org"

    Les informations liées au calcul des indemnités en cas de maladie non-professionnelle sont décrites dans la [fiche 59](https://www.hexopee.org/publication/59).


## Déclaration de l'arrêt sur Net-entreprises

???example "On déclare l'arrêt sur www.net-entreprises.fr, rubrique __Attestation de salaire__"
    
    1. Choisir __Attestation de salaire pour le versement des indemnités journalières__
    2. Choisir __Saisie des formulaires en ligne EFI__
    3. Choisir les caractéristiques de l'arrêt:  
        - si le jour avant le début de l'arrêt tombe sur un dimanche, mettre le dernier jour réellement travaillé (i.e. le vendredi)

## Récupération du borderau de paiement sur Net-entreprises

???example "On récupère le bordereau de paiement sur www.net-entreprises.fr, rubrique __Attestation de salaire__"  

    1. Choisir __Bordereaux de paiement des indemnités journalières__


## Déclaration de l'arrêt sur le chèque emploi associatif

???example "On déclare l'arrêt sur le site cea.ursaf.fr"

    1. cliquer sur le menu __Arrêts de travail__
    2. Sélectionner "Je souhaite signaler un arrêt de travail"
    3. Remplir le formulaire avec le motif de l'arrêt, le nom du salarié, et les dates (la veille de l'arrêt et le dernier jour de l'arrêt. ex: pour un arrêt du 27/04 au 01/05, les dates à inscrire sont 26/04 et 01/05). On souscrit au régime de la subrogation, et les dates de la subrogation correspondent aux dates de l'arrêt.

## Déclaration de la reprise de travail sur le chèque emploi associatif

???example "On déclare la reprise sur le site cea.ursaf.fr"

    1. Cliquer sur le menu __Arrêts de travail__
    2. Sélectionner "Je souhaite signaler une reprise  suite à un arrêt de travail"
    3. Sélectionner le signalement en cours
    4. Remplir la date et le motif de reprise

## Création du volet social sur le chèque emploi associatif

???example "On déclare le volet social en prenant en compte la période d'absence"

    1. Cliqer sur __Volets sociaux__ > __Créer un volet social__
    2. Sélectionner dans les options: __Déclarer des absences partiellement ou non rémunérées__
    3. Indiquer le montant payé sous forme de montant Net. Pour la périodicité, marquer le nombre d'heures habituellement réalisées moins les heures manquées. Par exemple, pour un salarié à 35h absent 3jours, la périodicité est 130,67h.
    4. Dans le formulaire __Absences__, marquer le motif et le nombre de jours __calendaires__ (y compris le samedi et le dimanche) de l'arrêt. Marquer le montant mensuel brut habituel, et le montant net des indemnités (50% du salaire journalier (3*brut / 91,25) * le nombre de jours calendaires de l'arrêt)