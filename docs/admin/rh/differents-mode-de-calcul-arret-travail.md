# Les différents mode de calcul d'un arrêt de travail

Nous utilisons deux methodes de calcul:  
- en jours calendaires  
- en jours travaillés  


!!! info "Information importante" 
    Il est important de :   
    - s'informer la méthode de calcul qui est privilégiée par la __convention collective__  
    - __comparer__ les deux méthodes   
    - __toujours__ choisir le calcul qui est le __plus avantageux__ pour le salarié.  

Pour les deux méthodes il faut avoir connaissance du brut, des heures prévue au contrat

## Méthodes des jours calendaires
*jours calendaires* = 

Le calcul est le suivant :
```
*(jours dans le mois - le jours d'abscences)/ (nombre de jours dans le mois)*
* brut / nombre de jours dans le mois
* nbre d'heures / nombre de jours dans le mois 
```

## Méthode des jours travaillés
*jours travaillés* = 

Le calcul  est le suivant :


!!! tips " Pour éviter les erreurs"
Vérifier que le taux horaire sur les deux méthodes est le même, en divisant le *brut/ les heures travaillées*

# Exemple, mise en situation
