# Création d'un employé dans odoo

## Création de l'employé

### Accéder au formulaire de création

Pour saisir un nouveau contrat (convention de stage, contrat de service civique ou contrat de travail) dans Odoo, il faut se rendre dans l'application **Employés**

![Capture d'écran montrant le menu **Employés** à sélectionner](../../assets/admin/rh/odoo-employee-menu.png)

Un fois dans l'application **Employés**, on clique sur le bouton **Créer** en haut à gauche.

![Capture d'écran montrant le bouton **Créer** dans l'application Employés](../../assets/admin/rh/odoo-employee-button-create.png)

### Créer l'employé

![Capture d'écran montrant le formulaire de création d'un employé, avec les champs à remplir mis en valeur](../../assets/admin/rh/odoo-employee-saisie1.png)

### Ajouter l'adresse

Après avoir créé la fiche de l'employé, on peut lui ajouter une adresse.

Pour cela, il faut cliquer sur le bouton **Éditer**.
![Capture d'écran montrant le bouton **Éditer** sur la fiche de l'employee](../../assets/admin/rh/odoo-employee-edit.png)

Vous pouvez ensuite modifier l'adresse en cliquant sur le bouton adjacent.

![Capture d'écran montrant le bouton sur lequel appuyer pour modifier l'adresse](../../assets/admin/rh/odoo-employee-modify-address.png)

On peut maintenant saisir l'adresse dans le pop-up qui s'est ouvert.

![Capture d'écran montrant le formulaire de modification de l'adresse](../../assets/admin/rh/odoo-employee-address-form.png)

## Création du contrat

### Accès à l'interface de saisie des contrats

Il faut cliquer sur le bouton qui liste les contrats de l'employé.

![Capture d'écran montrant le bouton listant les contrats de l'employé](../../assets/admin/rh/odoo-employee-list-of-contracts.png)

On peut alors cliquer sur le bouton **Créer**.

![Capture d'écran montrant le bouton Créer dans l'interface listant les contrats de l'employé](../../assets/admin/rh/odoo-employee-create-contract-button.png)

### Remplissage du formulaire

Dans la fenêtre pop-up qui s'est ouverte, on peut saisir les informations du contrat.

![Capture d'écran montrant l'interface de création d'un contrat, avec les champs à remplir mis en valeur](../../assets/admin/rh/odoo-employee-contract-form.png)