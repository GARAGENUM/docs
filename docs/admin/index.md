# Procédures administratives

Cette rubrique répertorie différentes procédures pour réaliser le travail administratif du Garage Numérique.

- [Recadrage des avis de paiement de l'ASP](cadrage-asp)
- [Procédures de compta avec odoo](process-odoo)
- [Procédures comptables](compta)
- [Procédures des Ressources Humaines](rh)