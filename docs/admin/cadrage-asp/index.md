# Cadrage des aides à l'emploi

Voici la procédure à suivre pour le travail de vérification des paiements d'aide à l'emploi.

## Un peu de contexte d'abord: 
Depuis 2014, l'association embauche des salariés, parfois en CDI, parfois en CDD, parfois à temps partiel, ou à temps plein.  
Pour certains de ces salariés, l'association bénéficie d'une subvention, mensuelle, qui permet de payer une partie du salaire et / ou des charges sociales.  
La subvention est attribuée par une institution: l'Etat, ou le Conseil Régional d'Ile de France, etc.  
Le montant de la subvention est variable selon le dispositif ( pourquoi la subvention est attribuée, dans quel cadre), et le salarié.  
La subvention est versée par un organisme qui s'appelle l'ASP (Agence de Services et de Paiements), qui nous fournit une interface en ligne qui s'appelle Sylae.  

## Le but de l'activité: 
Nous voulons intégrer toutes les pièces justificatives des paiements de l'ASP dans le logiciel de comptabilité, et vérifier qu'il n'y a pas d'écart entre ce que nous avons écrit dans la comptabilité et ce que l'ASP a enregistré de son côté.

## Les outils de travail:  
- odoo : c'est le logiciel de comptabilité, dans lequel on va intégrer les pièces justificatives de paiement,
- sylae: c'est l'outil de l'ASP à partir duquel on va télécharger les pièces justificatives,
- nextcloud: l'outil collaboratif du Garage Numérique pour écrire les différences qu'on trouve

## La procédure synthétique:  
1. Se connecter à SYLAE, et télécharger l'ensemble des avis de paiement concernant 1 salarié, 1 mesure, 1 année.  
   Ex: je télécharge les avis de paiement d'adulte relais pour Florian ROGER en 2019.
2. Dans Odoo, aller dans le menu Facturation > Comptabilité > Pièces comptables,   
   puis saisir les mots-clés pour la recherche (voir tableau de correspondance dans Odoo), en regroupant les pièces par journal
3. Pour chaque avis de paiement, le joindre à la pièce comptable correspondante du journal **Emplois Subs**, ainsi que dans le journal **Banque**.

## Correspondances entre les mesures et les mots clés de recherche

|  dispositif       | salarié    | mot-clé odoo  | nom de la mesure dans sylae | dates |
|-------------------|------------|---------------|------|-------------------------|
|  Emploi d'avenir  | MESDOUZE   |  avenir       | EAV  | du 01/2014 au 10/2015   |
|  Emploi d'avenir  | DENECHEAU  |  avenir       | EAV  | du 10/2015 au 08/2016   |
|  Emploi d'avenir  | KOUM       |  avenir       | EAV  | du 11/2016 au 11/2019   |
|  Emploi d'avenir  | TRAORE     |  avenir       | EAV  | du 11/2016 au 11/2019   |
|  CUI              | ROGER      |  CAE          | CUI  | du 04/2015 au 04/2017   |
|  PEC              | GUYET      |  PEC-CUI      | CUI  | du 11/2018 au 11/2019   |
|  PEC              | DIENG      |  PEC-CUI      | CUI  | du 11/2018 au 11/2019   |
|  Adulte Relais    | ROGER      | Adulte relais | AR   | du 04/2017 au 04/2020   |
|  Adulte Relais    | LAGUERODIE | Adulte relais | AR   | du 10/2019 au 12/2021   |
|  Apprentissage    | CHARBI     | apprentis     | AUEA | du 01/2021 au 08/2021   |
|  Apprentissage    | HAMOUD     | apprentis     | AUEA | du 09/2020 au 08/2021   |

## La procédure détaillée

### Récupérer les avis de paiement
1. Je me connecte à Sylae   

    ![capture d'écran de l'écran de connexion du service SYLAE](../../assets/admin/cadrage-asp/cadrage-asp-login-sylae.png)
2. Je vais à la page des avis de paiement   

    ![capture d'écran de l'écran d'accueil du service SYLAE](../../assets/admin/cadrage-asp/cadrage-asp-accueil-sylae.png)
3. Je filtre par année et par mesure et je télécharge les pdf des avis de paiement  

    ![capture d'écran de la liste des avis de paiement dans Sylae, avec une flèche indiquant le bouton de tri, et une flèche indiquant le bouton de téléchargement du pdf](../../assets/admin/cadrage-asp/cadrage-asp-avis-sylae.png)

### Accéder à la liste des enregistrements comptables
1. Je me connecte à **odoo14.legaragenumerique.fr**, et je clique sur le menu **Facturation**  

    ![capture d'écran de l'écran d'accueil d'odoo, avec comme élément actif le menu principal déroulant, avec l'élément Facturation en surbrillance](../../assets/admin/cadrage-asp/cadrage-asp-accueil-odoo.png)
2. Dans la vue **Facturation**, je clique sur le menu **Comptabilité**, puis **Pièces Comptables**  

    ![capture d'écran de la vue Facturation d'odoo, avec des flèches vers le menu actif Comptabilité > Pièces comptables](../../assets/admin/cadrage-asp/cadrage-asp-menu-compta-odoo.png)
3. Je filtre les entrées avec un mot-clé (cf [tableau de correspondance](#5-correspondances-entre-les-mesures-et-les-mots-cles-de-recherche) )  

    ![capture d'écran de la vue en liste des pièces comptables dans odoo, avec une mise en valeur du champ de filtre](../../assets/admin/cadrage-asp/cadrage-asp-keyword-search-odoo.png)
4. Je regroupe les résultats par **Journal**   

    ![capture d'écran de la vue en liste des pièces comptables dans odoo, groupées par journal après tri par mot clé, avec le menu de regroupement actif et mis en valeur dans un cadre rouge](../../assets/admin/cadrage-asp/cadrage-asp-groupe-journal-odoo.png)

### Comparaison des entrées dans odoo avec les avis de paiement  

Les étapes suivantes doivent être réalisées deux fois, une fois dans le journal **Emplois Subs** et une fois dans le journal **Banque**.  

1. Je sélectionne une pièce comptable dans Odoo qui correspond à un avis de paiement ASP  

    ![capture d'écran d'une vue avec odoo à gauche, vue des pièces comptables en liste, avec une ligne surlignée, qui correspond à la ligne surlignée dans la partie droite de la capture d'écran, dans laquelle on voit l'interface de Sylaé avec la liste des avis de paiement](../../assets/admin/cadrage-asp/cadrage-asp-liste-odoo-sylae.png)
2. J'ouvre la pièce comptable dans Odoo et j'ajoute en pièce jointe l'avis de paiement correspondant  

    ![capture d'écran de la vue détaillée d'une pièce comptable dans Odoo, avec un cadre pour mettre en évidence le bouton permettant d'ajouter une pièce jointe](../../assets/admin/cadrage-asp//cadrage-asp-add-attachment-odoo.png)  

3. Je vérifie que le montant au débit, sous le compte **441765**, correspond au montant de l'avis de paiement.  
    **Attention, 2 cas de figures : **  
    - Vous devez vérifier la ligne **TOTAL à PAYER** de l'avis de paiement pour comparer avec la pièce du journal **Emplois Subs**.
    - Vous devez vérifier la ligne **TOTAL à PAYER** de l'avis de paiement pour comparer avec la pièce du journal **Emplois Subs**.

    Si les montants ne correspondent pas entre l'avis de paiement et la pièce comptable, **je ne clique PAS sur Comptabiliser**.  

    ![capture d'écran montrant, à gauche, la vue détaillée d'une pièce comptable dans Odoo avec un cadre autour du montant au débit et du compte de tiers 441, et à droite l'avis de paiement de l'asp avec le montant total à payer encadré](../../assets/admin/cadrage-asp/cadrage-asp-comparaison-montant.png)

4. Je reporte les données dans le [tableau de synthèse "regul_asp.ods" sur Nextcloud](https://nextcloud.legaragenumerique.fr/index.php/f/264438)   
    
    ![capture d'écran du tableur Collabora dans Nextcloud, avec le tableau de synthèse du recadrage ASP, et un cadre de mise en évidence autour des la liste des onglets, un onglet par mesure et par salarié](../../assets/admin/cadrage-asp/cadrage-asp-tableau-synthese-nextcloud.png)