# Factures récurrentes


| Fournisseur | Produit / Service | Source | Date |
|-------------|-------------------|--------|------|
| Freebox petit local 0984224121 | -  Facture Fibre Free  <br/> - Communications Hors-forfait | [free.fr id fbx29518467](https://subscribe.free.fr/login) | début du mois |
| Freebox Pro grand local 0951175219| Facture Fibre FreePro | [Free pro contact@legaragenumerique.fr](https://pro.free.fr/espace-client/connexion) | vers le 10 du mois |
| Free mobile 0769319283 | Facture mobile Free | [mobile.free.fr 49445513](https://mobile.free.fr/account) | début du mois |
| Free mobile 0628451868 | Facture mobile Free | [mobile.free.fr 29472236](https://mobile.free.fr/account) | vers le 15 |
| EDF  | Facture d'électricité | [edf.fr contact@legaragenumerique.fr](https://auth.entreprises-collectivites.edf.fr/) | début du mois |
| Paris Habitat Grand Local 488206/05 | Loyer et Charges | Courrier | Tous les 3 mois |
| Paris Habitat Petit Local | Loyer et Charges | COurrier | Tous les 3 mois |
| Gandi.net | - Création ou renouvellement instance gandi.net <br/> - Utilisation de crédits cloud gandi.net <br/> - Création ou renouvellement nom de domaine Gandi.net <br/> - Création ou renouvellement email | [Admin Gandi.net](https://admin.gandi.net/) | le 25 du mois |


| Client | Produit / service | Source | Date |
|--------|-------------------|--------|------|
| ASP Service civique | - Indemnité Service civique <br/> - Indemnité Service civique Formation Prévention et Secours <br/> - Indemnité Formation Civique et Citoyenne | Courrier et [Elisa](https://elisa.service-civique.gouv.fr/civiqs/accueil.php) |
| ASP Aide aux Employeurs d'apprentis | Subvention aide aux employeurs d'apprentis | [ Sylae ASP](https://sylae.asp-public.fr/sylae) | 1 fois par mois |
| ASP Adulte Relais | Subvention Adulte Relais |  [ Sylae ASP](https://sylae.asp-public.fr/sylae) | 1 fois par mois |