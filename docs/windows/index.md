# Installation et activation de produits Microsoft
## Les sites
Une communauté à privilégier pour l'activation de logiciels microsoft est ==r/Piracy==, sur reddit.  
Il existe aussi un "megathread" qui regroupe tout les outils et logiciels lié au piratage.  
[https://github.com/Shakil-Shahadat/awesome-piracy](https://github.com/Shakil-Shahadat/awesome-piracy).    

Il faut éviter les sites comme ==ThePirateBay== qui contiennent enormement de virus.  
Il faut aussi éviter les ==recherches directes sur Google==.  
Les premiers liens sont souvant des virus, comme KMSPico.  
Les =="builds"== qui sont des logiciels avec une interface pour installer et activer les produits Microsoft sont aussi à éviter, il est très facile d'y inclure des virus et ils ne sont pas opensource.  

**En général, la meilleure méthode est de télécharger le produit tel quel sur les serveurs Microsoft pour ensuite l'activer avec MAS, qui est un script disponible sur GitHub.**

## Outils
#### [Microsoft Activation Scripts (MAS)](https://github.com/massgravel/Microsoft-Activation-Scripts/releases)
Le meilleur activateur, licences Windows 10 et Office, il permet aussi de changer d'edition de Windows.  
Il s'execute dans un terminal en super-utilisateur.  
En plus de ça, grâce à sa nature de script, il est totalement transparent contrairement aux outils avec une interface.  
Il possible de l'executer directement à partir de powershell
```powershell
irm https://massgrave.dev/get | iex
```
#### [tb.rg-adguard.net](https://tb.rg-adguard.net) 
Un site pour télécharger directement les produits Microsoft depuis leurs serveurs.  
On peut aussi aller directement sur la [page de téléchargement de Windows](https://www.microsoft.com/fr-fr/software-download/windows10) mais cette page permet seulement de télécharger un outil de mise à jour quand on est déjà sur Windows et semble donner un lien vers les ISO uniquement ==depuis Linux==, il ne permet pas non plus de choisir une version précise.  
Il est possible de changer l'user-agent de son navigateur pour acceder à la version "linux" de cette page, ce que beaucoup de personne font.  
!!! warning "Avertissement de sécurité"
		Ne pas tenir compte du message d'avertissement au moment tu téléchargement, les serveurs de téléchargement de Microsoft sont toujours en HTTP, ce qui génère des alertes de sécurité...

## Activation
L'activation d'un logiciel Microsoft est plutôt simple, voici une procedure complète en détail, pour Office :
#### Téléchargement de Office
Se rendre sur [tb.rg-adguard.net](https://tb.rg-adguard.net) et sélectionner la version souhaitée de Office.  
!!! warning "On ne peut selectionner qu'un seul logiciel de la suite office à la fois, pour avoir Word, Excel et Powerpoint par exemple, il faudra les télécharger un par un"
![office-1.png](../assets/admin/windows/office-1.png)
#### Installation de Office
Les fichiers téléchargés sont au format ==iso==.  
En double-cliquant dessus Windows les montes dans un lecteur virtuel.  
Une fois montée il faut cliquer sur "  
Suivre les étapes d'installation  
Répeter ces étapes pour chaque logiciel.  
#### Activation de Office
!!! info "Penser à installer tout les logiciels avant de les activer, le script permet de tous les activer en même temps"
Il faut executer le script "MAS_AIO.cmd" qui se trouve dans "All-In-One-Version" ==en administrateur==.  
Une fois ce script éxecuté on a cette interface  
![office-2.png](../assets/admin/windows/office-2.png)  
Selectionner ==Online KMS== (2) puis ==KMS Activation== (1)  
Le script va chercher et activer tout les produits Microsoft sans licence (y compris la licence Windows).  

