# test for jinja templates doc in mkdocs

with raw balises:
{% raw %}
{\{define "main"}}
{% endraw %}


with html coments and raw balises: 
<!-- {% raw %} -->
{\{define "main"}}
<!-- {% endraw %} -->

escaping the 2nd opening curly bracket:
{\{ define "main" }}

no raw balises (breaking local): 
{\{ define "main" }}
