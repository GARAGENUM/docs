# Introduction

Les cours du Garage contiennent:

- [Un cours de scripting Bash](scripting/syntax.md)
- [Un cours de découverte de Python](python)
- [Les guides utilisateurs des outils du Garage Numérique](outils-garage)

Pour plus de ressources, cliquez sur la rubrique Divers dans la barre de navigation en haut
