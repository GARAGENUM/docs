# Syntaxe Bash

## Shebang

Permet de définit l'interpréteur qu'on veut appliquer au script.
Il se place en tout début de script, et indique le chemin complet de l'exécutable.

```
#!/bin/bash
```

## Structures de contrôle

### for  ....  do  .... done

Les actions entre `do` et `done` sont répétées pour chaque élément trouvées dans la liste qui suit `for`.
Chacun des éléments est enregistré dans une variable `i` qui est incrémentée à chaque passage de la boucle.

```
for i in liste ; do
    action1 ;
    action2 ;
done
```

### if .... then .... elif ... else ... fi 

ces variable permete par exemple de faire réagir le script de manière différente, selon la réponse de l'utilisateur à une question (si ... alors .... sinon si .... sinon ....fin)

```
until [[ $ouinon == "y" || $ouinon == "n" ]] ; do
    echo -n "voulez-vous voir le modele de votre carte graphique Y/n : "

    read ouinon
    if [ "$ouinon" = "Y" ] || [ "$ouinon" = "y" ]; then

        GRAPHIC_CARD = $(lspci | grep VGA | cut -d: -f3 |tail -c +2)

        echo "votre carte graphique est une : $GRAPHIC_CARD

    elif [ "$ouinon" = "N" ] || [ "$ouinon" = "n" ]; then
        echo "OK, bye"
    else 
        echo "Il faut écrire "y" or "n" ;
    fi
done
```

### while .... do .... done

while = tant que

### until .... do .... done

until = jusqu'à ce que

## Les outils pour la manipulation du texte

### cut 

### tail

### head

### sed

La commande sed permet d'éditer une chaine de caracteres d'un fichier. Elle comprend des arguments et une subcommand.
L'argument -i permet de faire le changement in place, s ou d pour substitute ou delete et g pour global.
```
sed 's/string1/string2/g'
sed -i 'd/string1/string2/g'
```
### grep

La commande grep permet de filter des lignes, elle est généralement utilisé avec | . On met l'élement (lettre, chiffre ou mot) qu'on veut filtrer juste apres grep.
```
ls | grep ^a
```
### tr

La commande tr permet de supprimer des caracteres identique et consécutifs.
```
tr -s ''
```

### tee

La commande tee nous permet d'écrire les sorties d'une commande (sortie standard et sortie d'erreur) dans un fichier, tout en laissant les sortie s'afficher sur le terminal.

```
LOG=/home/admin/mon-log.txt
ma_commande 2>&1 |tee $LOG
```

### wc

Compteur (de lignes, de mots...)
```
wc -l myfile.txt  # donne le nombre de lignes du fichier
```

### awk

```
#awk -f {"substring"}
```
## Bashism

### string replacement   ${"string"##"substring"}

### Exécution conditionnelle  $$ || 

### comparaisons   [[ "string" == "otherstring" ]] ; [[ number -eq number ]]

## Les fonctions

### Déclaration d'une fonction

Une fonction est définit en lui donnant un nom quelconque avec la syntaxe ci dessous.
```
random_function(){
}
```
### Appel d'une fonction

Dans un script une fonction est simplement appelé par son nom.
```
random_function
``` 

### Les arguments

- `$0` : Contient le nom du script
```
siska@siska-pc:~/cours/doc/docs/cours/scripting$ cat script.sh 
#!/bin/bash

echo $0
siska@siska-pc:~/cours/doc/docs/cours/scripting$ ./script.sh 
./script.sh
siska@siska-pc:~/cours/doc/docs/cours/scripting
```
- `$*` : contient tout les arguments dans une chaine de charactère
```

```
- `$#` : contient le nombre d'arguments
```

```
- `$?` : contient le code de retour de la dernière commande
```

```
- `$$` : contient le PID du shell qui exécute le script
```

```
- `$!` : contient le PID de la dernière commande lancée 
```

### EOT (End Of Transmission)
#awk -f {"substring"}