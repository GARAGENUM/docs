# Configurer l'espace de travail

## Version de python

Python est installé de base dans tout les systèmes Debian.
pour afficher sa version:

```
user@host:~$ python3 --version
Python 3.9.19
```

> Note: Si vous avez une version ultérieure, c'est normal (les versions évolues)

---

## Installer pip

pip (Package installer for python or "Pip Installs Packages") est un outil très utile pour gérer les dépendances dans un projet python.

Là aussi, il y a pip pou python2 et pip pour python3, mais Debian utilisera la version correpondante à la version de python définie avec `update-alternatives`.

```
user@host:~$  sudo apt install python3-pip python-pip
user@host:~$  pip --version
```

#### Mettre à jour pip

```
python3 -m pip install --upgrade pip
```

---

## Utiliser la console python

Pour découvrir python, rien de plus simple, on saisit la commande `python` dans le terminal:

```
user@host:~$ python3
Python 3.11.2 (main, Mar 13 2023, 12:18:29) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
```
Les trois chevrons `>>>` vous indiquent que vous êtes dans la console python.  
Faites ++ctrl+d++ pour en sortir.

Vous pouvez déclarer une variable, la transformer et l'afficher:

```
>>> MA_VARIABLE = " ceci est le contenu de ma variable "
>>>
>>> print(MA_VARIABLE)
 ceci est le contenu de ma variable 
>>> 
>>> VAR_2 = MA_VARIABLE + " et celle-ci est plus longue"
>>> 
>>> print(VAR_2)
 ceci est le contenu de ma variable  et celle-ci est plus longue
```

---

## Créer un environnement virtuel

Depuis Debian 12 (Bookworm), python3 est utilisé par le système et il convient de séparer le binaire Python utilisé par le système et celui utilisé dans un projet (afin de ne pas mélanger les dépendances/librairies nécessaires aux divers projets également).  

Pour installer un environnement virtuel, il faut d'abord installer le package Debian:

```
sudo apt-get install python3-venv
```

#### Installer l'environnement virtuel

```
python3 -m venv venv
```

> Note: un dossier venv va être créé avec un binaire de Python dédié au projet

#### Activer l'environnement virtuel

Pour signifier au système d'exploitation que nous allons utiliser ce binaire Python, nous devons activer cet environnement:

```
source venv/bin/activate
```

Vous devriez obtenir un préfixe `venv` à votre prompt:

```
(venv)user@host:~/mon-projet:$
```

> Note: La commande `python3` appelle désormais le binaire python situé dans votre projet (venv/bin/python3)

#### Installer des librairies avec pip

L'environnement virtuel activé, nous pouvons installer les librairies nécessaires à notre projet:

```
python3 -m pip install requests # exemple pour installer la librairie [requests](https://pypi.org/project/requests/)
```

#### Désactiver l'environnement virtuel

```
deactivate
```

Pour plus d'informations sur les environnements virtuels, visitez la [documentation](https://docs.python.org/3/library/venv.html)
