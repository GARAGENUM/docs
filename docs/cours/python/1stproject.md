# Démarrer un projet

## Création du projet

??? important "Note importante pour les étudiants"
    Ne créez pas un nouveau dossier, 

    1. clonez plutôt le projet d'évaluation 
    2. basculez sur la branche start
    3. Créez une nouvelle branche nommée `start-votre-id-gitlab`
    ```
    git clone https://gitlab.com/devopsp1/python-webscrap.git`  
    cd python-webscrap
    git checkout start
    git checkout -b start-mon-id-gitlab
    ```

On démarre un projet simplement en créant un dossier, dans lequel (par convention), on crée le fichier `main.py`

```
user@host:~$  mkdir mon_projet && cd mon_projet && touch main.py
```

Ce fichier main.py contiendra les instructions principales du programme, et pourra appeler d'autres fichiers python.

On met un **shebang** en 1ère ligne pour indiquer la version de python.

Dans ce script on déclare deux variables, qu'on nomme `api_url`et `endpoint`, auxquelles on donne une valeur.

!!! note "main.py"
    ```
    #!/usr/bin/env python3

    api_url = "https://gitlab.com/api/v4"
    print(api_url)
    endpoint = api_url + "/groups"
    print(endpoint)
    ```

On peut simplement exécuter ce script avec `python main.py` ou l'exécuter en mode **interactif** (-i) pour continuer à manipuler les variables et les fonctions du programme:

``` 
user@host:~$  python -i main.py 
https://gitlab.com/api/v4
/groups
https://gitlab.com/api/v4/groups
>>> 
>>> type(endpoint)
<class 'str'>

```
La fonction `type` nous affiche le type de variable utilisé: il s'agit d'une chaîne de caractères (`str`)



---



## Les types de variables dans python

### Les chaînes de caractères

Les variables que nous avons utilisées sont du type `str` ou `string`, c'est à-dire une **chaîne de caractères**.
On les déclare entre guillemets:

```
>>> VAR_A = " some random string, with 6 words"
>>> type(VAR_A)
<class 'str'>
```

### Autres variables simples

Il y a d'autre variables simples: 

```
>>> VAR_B=True
>>> type(VAR_B)
<class 'bool'>    # VAR_B est un `boolean`, une valeur booléenne, \
            # qui ne peut être que `True` ou `False`

>>> VAR_C = 10
>>> type(VAR_C)
<class 'int'>       # VAR_C est un `integer`, un nombre entier
>>> 
>>> 
>>> VAR_D=3.141592
>>> type(VAR_E)
<class 'float'>     # VAR_E est un `float`, un nombre décimal
```

### Les listes et les tuples

Il y a des variables qui permettent de stocker plusieurs valeurs: les **listes** ( `list`), les **tuples**, et les **dictionnaires** (`dictionary`).

Les listes sont déclarées entre crochets ( `brackets [ ] `) et contiennent plusieurs éléments (int, str, list...) séparés par une virgule.

```
>>> VAR_F = ["object",  150,  "proc",  "package",  2000]
>>> type(VAR_F)
<class 'list'>
```

On peut modifier les éléments de la liste en les indexant entre crochets:

```
>>> print(VAR_F[0:2])
['object', 150]
>>> 
>>> VAR_F[1] = 250
>>> print(VAR_F[0:2])
['object', 250]
```

Les **tuples** sont des listes qu'on ne peut pas modifier. On les appelle entre parenthèses:
```
>>> VAR_F = ( "elem1", "elem2", "elem3")
>>> 
>>> print(type(VAR_F))
<class 'tuple'>
```

### Les dictionnaires

Les dictionnaires sont des séries de paires clé-valeur, déclarées entre accolades ( `curly brackets { } `)

```
>>> VAR_G = { "name": "python",   "version_number": 3.8,   "use": "coding"}
>>> type(VAR_G)
<class 'dict'>
```

Comme pour les listes, on peut accéder aux valeurs et les modifier en indexant avec les crochets:
```
>>> VAR_G["use"] = VAR_G["use"] + " and playing"
>>> 
>>> print(VAR_G["use"])
coding and playing
```

??? important "Note importante pour les étudiants"
    Pour l'évaluation, vous devez demander une merge request depuis votre branche vers la branche start.
    ![Faire une merge request](../../assets/cours/python/creating-merge-request.png)

