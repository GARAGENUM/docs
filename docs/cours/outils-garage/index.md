# Introduction

Des informations sur la configuration et l'usage des outils du garage:  

- [le Tchat avec le client Element sur le réseau décentralisé Matrix](element.md)
- [la mise en place d'un environnement de développement](devenv)
