# Element

Après avoir validé votre profil et créé votre mot de passe pour votre compte legaragenumerique.fr, vous pouvez accéder au tchat des équipes du garage.  

Le tchat du Garage Numérique se base sur le réseau Matrix, qui est un protocole réseau décentralisé pour le tchat et le partage de fichiers.  

Le Garage Numérique a déployé un noeud autonome utilisant le protocole Matrix, sur son propre serveur hébergé en France par Gandi.net .  

Pour accéder au tchat, nous vous proposons d'utiliser l'application Element.  

Element est **l'application cliente** qui va se connecter au **serveur Matrix** du Garage Numérique.  

## Installation de l'application  

Installez l'application Element depuis le PlayStore.   

![capture d'écran de l'application Element à installer](../../assets/cours/outils-garage/element-play-store.png)  

## Connexion au serveur du Garage Numérique  

1. Connectez-vous en prenant l'option "Serveur personnalisé".  
  ![capture d'écran de l'écran d'accueil de l'application](../../assets/cours/outils-garage/element-welcome-page.png)

2. Donnez l'adresse du serveur du Garage Numérique  
  ![capture d'écran de l'écran de choix du mode de connexion dans Element](../../assets/cours/outils-garage/element-connection-mode.png)   

3. Continuer avec l'authentification unique  
  ![capture d'écran de l'écran de confirmation de la connection par SSO](../../assets/cours/outils-garage/element-sso-auth.png)   

4. Vous accédez à l'écran de connexion centralisé du Garage Numérique  
  ![capture d'écran de l'écran de login SSO du Garage Numérique avec Keycloak](../../assets/cours/outils-garage/element-login-keycloak.png)  

5. Il ne vous reste plus qu'à admettre l'adresse de login comme adresse de confiance pour Element  
  ![capture d'écran de l'écran de confirmation de l'url de login sso comme adresse de confiance pour Element](../../assets/cours/outils-garage/element-sso-trust-url.png)


## Sécurisation de l'application  

Les données sur le Tchat du Garage Numérique sont chiffrées de bout en bout, ce qui implique que vous devez créer des passphrases pour sécuriser le chiffrement et être en mesure de récupérer les données (en cas de perte de mot de passe, d'ouverture d'une nouvelle session, etc.).  

Dès le premier démarrage de l'application, réalisez les opérations suivantes:  

### Accès à l'outil de sécurisation

1. Accédez au menu principal  
  ![capture d'écran de la page d'accueil de l'application Element, avec une flèche indiquant l'emplacement du menu, en bas à gauche](../../assets/cours/outils-garage/element-main-menu-arrow.png)  

2. Accédez aux paramètres  
  ![capture d'écran du panneau latéral gauche déployé, avec une flèche pour indiquer en bas l'emplacement du bouton d'accès aux paramètres](../../assets/cours/outils-garage/element-settings-arrow.png)  

3. Accédez aux paramètres de sécurité  
  ![capture d'écran des catégories de paramètres, avec une flèche pour indiquer au milieu l'emplacement de la catégorie Sécurité et vie privée](../../assets/cours/outils-garage/element-security-arrow.png)  

4. Accédez à l'outil de récupération des messages chiffrés  
  ![capture d'écran de la liste des paramètres de la catégorie Sécurité et vie privée, avec une flèche indiquant l'emplacement de l'outil Récupération des messages chiffrés](../../assets/cours/outils-garage/element-secure-tool.png)  


## Création des clés  

1. Commencez à utiliser la sauvegarde des clés  
  ![capture d'écran de l'écran de confirmation de la mise en place des clés de récupération](../../assets/cours/outils-garage/element-confirm-keys-creation.png)
   
2. Créez votre passphrase pour chiffrer vos données   
  Pour cela, utilisez la carte Clipperz dans laquelle vous avez stocké les informations de votre compte legaragenumerique.fr, et créez une passphrase dans un nouveau champ, que vous copierez ensuite dans le formulaire.  
  ![capture d'écran de l'écran de saisie et de confirmation de la passphrase](../../assets/cours/outils-garage/element-keys-creation-form.png)  
  
3. Enregistrez aussi dans votre carte clipperz la clé de récupération générée automatiquement     
  ![capture d'écran de l'écran de confirmation de la création de la passphrase, avec des flèches indiquant avec insistance l'emplacement du bouton permettant de sauvegarder la clé de récupération générée automatiquement](../../assets/cours/outils-garage/element-save-restore-key.png)  
  
4. Vous devriez maintenant obtenir un message de confirmation de la sécurisation de l'application  
  ![capture d'écran de l'écran de confirmation de la configuration de la sauvegarde de clés](../../assets/cours/outils-garage/element-final-secure-ok.png)  
  

Done!