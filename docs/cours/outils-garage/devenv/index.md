# Environnement de développement

Pour monter votre environnement de travail, assurez-vous de bien suivre les étapes suivantes.

Pour chaque étape, vous devez m'envoyer en mp dans le tchat les éléments demandés **en gras** à la fin du bloc déroulable.

??? note "Étape 1: Mettre à jour son compte legaragenumerique.fr et les outils associés"
    1. Accéder à votre compte à l'adresse [https://id.legaragenumerique.fr/account](https://id.legaragenumerique.fr/account) pour mettre à jour vos informations et définir un nouveau mot de passe.
    2. Installer et sécuriser [Element](/doc/cours/outils-garage/element/), le client de tchat sur le réseau [Matrix](https://matrix.org/) du Garage Numérique
    3. Dans Element, s'assurer d'avoir rejoint le groupe TEAM, ainsi que les salons gn-general, gn-dev et DevProjets

    **Envoyez-moi un message privé (mon pseudo c'est makayabou)**

??? note "Étape 2: Avoir un système à jour"

    Dans Debian, s'assurer qu'on a bien le fichier `/etc/apt/sources.list` qui correspond bien à Debian 11 Bullseye:   
    ```conf
    deb http://deb.debian.org/debian/ bookworm main non-free-firmware contrib non-free
    deb-src http://deb.debian.org/debian/ bookworm main non-free-firmware contrib non-free
    deb http://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib non-free
    deb-src http://deb.debian.org/debian/ bookworm-updates main non-free-firmware contrib non-free
    deb http://security.debian.org/debian-security bookworm-security main non-free-firmware contrib non-free
    deb-src http://security.debian.org/debian-security bookworm-security main non-free-firmware contrib non-free
    deb http://deb.debian.org/debian/ bookworm-backports main non-free-firmware contrib non-free
    deb-src http://deb.debian.org/debian bookworm-backports main non-free-firmware contrib non-free
    ```

    Faire les mises à jour complètes, redémarrer l'ordinateur, et faire une deuxième fois les mises à jour sur le nouveau noyau:  
    ```bash
    sudo apt update
    sudo apt upgrade
    sudo apt dist-upgrade
    ```

    **Envoyez-moi en mp le résultat de la commande suivante: `uname -a`**

??? note "Étape 3: Installer les outils de virtualisation et de gestion de paquets"  
    1. Installer quelques outils fondamentaux: 
        ```bash
        sudo apt install curl git wget screen
        ```
    1. Installer [flatpak](https://flatpak.org/setup/Debian/)
    2. Installer [snap](https://snapcraft.io/docs/installing-snap-on-debian)
    3. Installer [docker & doker-compose](https://docs.docker.com/engine/install/debian/)

    **Envoyez-moi en mp le résultat des commandes suivantes:  **
    ```bash
    flatpak --version
    snap --version
    docker version
    docker compose version
    ```

??? note "Étape 4: Installer python3 et le gestionnaire pypi"
    ```bash
    sudo apt install python3
    sudo apt install python3-pip
    ```
    **Expliquez-moi en mp à quoi sert pip, et envoyez-moi le résultat des commandes suivantes:  **
    ```
    pip --version
    ```

??? note "Étape 5: Initialiser un projet Git"
    1. Créer un dossier en choisissant le nom du projet
    ```bash
    mkdir mon-projet
    cd mon-projet
    ```
    2. Si vous utilisez git sur cet ordinateur pour la 1ère fois, c  onfigurer la liaison avec votre compte gitlab
    ```bash
    git config --global user.email "adresse-mail-de-votre-compte-gitlab"
    ```
    2.  Initialiser un projet git
    ```bash
    git init .
    ```
    3. Créer un fichier `README.md` et l'ajouter au projet
    ```bash
    touch README.md
    echo "#Nom de mon projet" > README.md
    git add README.md
    git commit -m "1st commit for project 'nom de mon projet'"
    ```
    3.  Pousser le projet vers [gitlab](https://zapier.com/blog/how-to-push-to-gitlab/)  

    **Expliquez-moi en mp à quoi sert git, et envoyez moi le résultat de la commande suivante en mp:**
    ```bash
    git log
    ```

??? note "Étape 6. Créer un environnement virtuel Django pour votre projet"
    1. Créer l'environnement virtuel
    ```bash
    python3 -m venv venv
    ```
    2. Excluez l'environnement virtuel de votre projet
    ```bash
    touch .gitignore
    echo venv >> .gitignore
    git add .gitignore
    git commit -m "add venv in gitignore"
    ```
    3. Activez l'environnement virtuel
    ```bash
    source venv/bin/activate
    ```

    **Expliquez-moi en mp à quoi sert de créer un environnement virtuel, et dites-moi ce que a changé dans l'invite de commandes après l'activation**

??? note "Étape 7. Installer Django et démarrer un projet"
    1. Installer les librairies nécessaires (l'environnement virtuel doit être activé)
    ```bash
    pip install django
    ```
    2. Créer le projet
    ```bash
    django-admin startproject nom-du-site-web .
    ```
    3. Ajouter les nouveaux fichiers au projet
    ```bash
    git add -A
    git commit -m "start django project"
    ```

    **Montrez le résultat de la commande suivante: **
    ```bash
    python -m django --version
    ```

??? note "Étape 8. Installer et configurer VsCodium"
    1. [Installer codium](https://vscodium.com/)
    2. Vous pouvez essayer de suivre [ce tutoriel](https://code.visualstudio.com/docs/python/tutorial-django) pour configurer votre projet django dans codium et vous lancer dans un premier projet de single page


