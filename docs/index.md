
<center><img src="assets/logo-web-garage_numerique.png"></center>

# Introduction

Ce site répertorie la documentation du Garage Numérique, grâce au logiciel mkdocs avec le theme material.

La documentation est divisée ainsi :

- [la présentation du garage](presentation-garage)
- [les cours](cours) 
- [les procédures administratives](admin)
- [les autres ressources](divers)
    - [les mémos pour bien utiliser les commandes linux](divers/cmd/index.md)
    - [les tutos d'administration système](divers/admin/index.md)
    - [les guides pour l'administration de serveurs](divers/server/index.md)
    - [les guides DevOps](divers/devops/index.md)
    - [les guides internes](divers/internal/index.md) 
    - [les astuces de développeur](divers/dev/index.md)

