# Bac + 1 Technicien DevOps

## Présentation 

### Nomenclature

| Titre | Diplôme d'établissement Bac+1 CNAM Informatique Développement et exploitation | 
|-------|-------------------------------------------------------------------------------|
| Code  | DIE7600A  |
| NSF   |           |
| ROME  |           |

### Publics

La formation s'adresse à des jeunes titulaires du bac, toutes sections confondues, 
qui voudraient s'orienter vers les métiers du numérique.  
Aucun pré-requis académique n'est exigé pour l'entrée en formation. cette dernière 
est réglée par un entretien, centré sur la détection de la motivation et du potentiel du candidat. 
Des connaissances de base en informatique sont également vérifiées (outils bureautiques, usages des services courants de l’Internet).

### Objectif

La formation Bac+1 Informatique: Développement et exploitation vise à former des techniciens de maintenance informatique
en s'appuyant sur les outils et les méthodes DevOps.

À l'issue de la formation, les étudiants seront capables de:  

- installer et administrer un poste de travail serveur et client en prenant en compte les contraintes de sécurité
- utiliser les languages de balisage (markdown, json)
- utiliser les languages de script (shell, bash)
- utiliser la programmation objet (Python3)
- diagnostiquer des pannes et assister les utilisateurs
- déployer un environnement virtualisé dans le cloud

## Enseignements

### Enseignements du tronc commun

| Code   | Intitulé   | Crédits |
|--------|------------|---------|
| USAL3V | Ouverture aux cultures numériques 1 | 6 ECTS |
| USAL3C | Anglais | 2 ECTS |
| USAL3D | Outils mathématiques | 2 ECTS |
| USAL3E | Méthodes et outils de la communication écrite 1  | 3 ECTS |
| USAL3F | Économie, Gestion et organisation de l'entreprise | 3 ECTS |
| USAL3G | Démarches de projet 1 | 2 ECTS |
| USAL33 | HTML - CSS | 6 ECTS |
| USAL39 | Réseaux et Systèmes | 2 ECTS |

Plus d'informations sur [la brochure de la formation](bacplusun-devops.pdf)

### Enseignements de spécialité

| Code   | Intitulé   | Crédits |
|--------|------------|---------|
| USOP3V | [Installation et Administration du Poste de Travail Windows et Linux](#installation-et-administration-du-poste-de-travail-windows-et-linux) | 4 ECTS |
| USOP3W | [Installation et Maintenance du Poste de Travail en réseau](#installation-et-maintenance-du-poste-de-travail-en-reseau) | 4 ECTS |
| USOP3X | [Outils internet: installation et utilisation](#outils-internet-installation-et-utilisation) | 4 ECTS |
| USOP3Y | [Technologie informatique et réseaux](#technologie-informatique-et-reseaux) | 4 ECTS |
| USOP3Z | [Maintenance du Poste de Travail en Réseau](#maintenance-du-poste-de-travail-en-reseau) | 4 ECTS |
| USAL4J | [Cloud computing](#cloud-computing) | 4 ECTS |
| USAL4K | [Administration Système](#administration-systeme) | 4 ECTS |
| USAL4L | [Cybersécurité](#cybersecurite) | 4 ECTS |
| USAL4M | [Programmation](#programmation) | 4 ECTS |


####   Installation et Administration du Poste de Travail Windows et Linux

| Description | Ce module est une introduction à la maintenance des systèmes informatiques |
|-------------|----------------------------------------------------------------------------|
| Compétences | <li>[x] Réaliser le branchement d'un poste de travail |
|             | <li>[x] Assembler les composants internes d'un ordinateur |
|             | <li>[~] Réaliser l'installation d'un système Linux  |
|             | <li>[~] Réaliser l'installation d'un système Windows / OsX  |

#### Installation et Maintenance du Poste de Travail en réseau 

| Description | Ce module fait découvrir la ligne de commandes pour l'administration du système |
|-------------|---------------------------------------------------------------------------------|
| Compétences | <li>[x] Utiliser le shell pour l'administration des systèmes UNIX                       |
|             | <li>[~] Utiliser la console windows (Dos Shell) et Power Shell pour l'administration des systèmes Windows |
|             | <li>[~] Écrire des scripts pour automatiser les tâches de maintenance                           |

#### Outils internet: installation et utilisation 

| Description | Ce module permet d'apprendre à utiliser les outils nécessaires au travail en équipe |
|-------------|-------------------------------------------------|
| Compétences | <li>[x] Concevoir une politique de sécurité des données |
|             | <li>[x] Utiliser git pour la gestion des versions du code informatique et la gestion de tickets |
|             | <li>[x] Utiliser un groupware (Nextcloud)  |


#### Technologie informatique et réseaux 

| Description | Ce module vise à l'acquisition de bases en programmation-objet |
|-------------|----------------------------------------------------------------|
| Compétences | <li> [~] Programmer avec les outils de base du language Python: la syntaxe, les bibliothèques, les fonctions |
|             | <li> [ ] Manipuler la librairie `requests` (REST API) |


#### Maintenance du Poste de Travail en Réseau 

| Description | Ce module permet d'apprendre à configurer un réseau local et des applications client-serveur |
|-------------|------------------------------|
| Compétences | <li> [~] Configurer un réseau local |
|             | <li> [~] Configurer un serveur: DHCP, SSH, Samba, Nginx |

#### Cloud computing 

| Description | Ce module forme les étudiants à l'utilisation des plate-formes cloud de micro-services |
|-------------|----------------------------------------------------------------------------------------|
| Compétences | <li>Comprendre la distinction entre Iaas, Paas, Saas, ...  |
|             | <li>Comprendre et utiliser les plate-formes de Cloud |

#### Administration Système

| Description | Ce module est consacré aux outils avancés d'administration système |
|-------------|----------------------------|
| Compétences | <li>Utiliser `cron` et `systemd` pour automatiser les tâches |
|             | <li>Mettre en oeuvre une architecture client -serveur - base de données |

#### Cybersécurité

| Description | Ce module vise à sécuriser les installations |
|-------------|----------------------------------------------|
| Compétences | <li>Sécuriser ssh avec une clé de chiffrement | 
|             | <li>Sécuriser un site web avec `hsts`|

#### Programmation

| Description | Module d'approfondissement Python |
|-------------|-----------------------------------|
| Compétences | <li> Instancier une classe        |
|             | <li> Interagir avec une api       |
|             | <li> Concevoir une interface de diagnostic |


## Compétences par blocs et outils


| Domaine de compétences |                        Compétences | Outils |
|----------------------------------------------------|--------|--------|
|**Synthèse des compétences**                        | | 
| |<li>installer et administrer un parc informatique |linux |
| |<li>programmation script et orienté-objet         |python |
| |<li>utiliser les outils devops                    |docker |
|**Travailler dans un environnement collaboratif**   | |
| |<li>Concevoir une politique de sécurité des données |
| |<li>utiliser un groupware                         | nextcloud |
| |<li>utiliser les versions de code                 | gitlab |
| |<li>utiliser un IDE                               | vscode |
|**Installation du poste de travail**                | |
| |<li>assembler les composants internes du pc       | tournevis
| |<li>réaliser le branchement des périphériques  
| |<li>cablâge réseau d’un parc informatique
|**Installation du système d’exploitation**
| |<li>partitionner un disque dur                   | partitionnement
| |<li>réaliser une clé usb d’installation          | live-system
| |<li>installer un système d’exploitation          | usb-boot
| |<li>utiliser le démarrage et l’installation par le réseau | pxe
|**Administration du poste de travail**
| |<li>utiliser la ligne de commandes                | bash, shell, powershell
| |<li>installer des applications natives            | apt
| |<li>mettre en place une machine virtuelle         | virtualbox, qemu
|**Programmation Python 1**
| |<li>connaitre les structures élémentaires: variables, fonctions, libraires | Python
| |<li>utiliser une API simple (REST Api)            | requests
|**Programmation Python 2 (bac +1 )**
| |<li>savoir instancier une classe
| |<li>connaîtres les librairies d’administration système | psutils
| |<li>réaliser un programme graphique de diagnostic | tkinter
|**Administration avancée du poste de travail (bac +1)**
| |<li>utiliser la syntaxe de bash
| |<li>automatiser les tâches                        | cron, systemd
| |<li>installer des applications ‘bac à sable’      | flatpak, snap
|**Administration du serveur**
| |<li>Mettre en place les services réseau           |ssh, smb, nfs
| |<li>Déployer une architecture serveur-base de données | nginx, sql, wordpress
|**Sécurité des serveurs (bac +1)**
| |<li>implémenter une clé de chiffrement            | openssl
| |<li>sécuriser un serveur                          | let's encrypt
| |<li>utiliser un outil de supervision              | nagios-like
|**Cloud Computing (bac +1)**
| |<li>Utiliser les conteneurs                       | docker 
| |<li>Utiliser les orchestrateurs                   | docker-compose, gitpod        
| |<li>Utiliser les plate-formes de Cloud            | google cloud platform
|**Déploiement d’un parc informatique**
| |<li>Configurer un réseau local et un pare-feu     | gns3, ufw
| |<li>Réaliser le câblage réseau                    | cisco
| |<li>Utiliser un outil de gestion de parc          | glpi
| |<li>Accompagner les utilisateurs à la prise en mains
