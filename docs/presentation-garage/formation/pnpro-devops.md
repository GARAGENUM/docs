# Passe Numérique Pro Technicien DevOps

## Présentation 

### Nomenclature

| Titre | Diplôme d'établissement PASSE Numérique Pro parcours Maintenance et Sécurité Informatique |
|-------|-------------------------------------------------------------------------------------------| 
| Code  | DIE6302A  |
| NSF   | Assistance Informatique, Maintenance de Logiciels et Réseaux (326r) |
| ROME  | Gestionnaire du Parc Informatique (M1810) |

### Publics 

Le Passe numérique pro s’adresse à deux types de public, souhaitant reprendre des études et intéressés par les métiers du numérique :  

- Les jeunes en transition difficile entre formation et emploi (décrochés du secondaire ou désorientés, prioritairement).   
- Les personnes peu ou pas qualifiées en recherche d’emploi Les personnes peu ou pas qualifiées dans l’emploi et en souhait d’une réorientation professionnelle.   

Aucun pré-requis académique n’est exigé pour l’entrée en formation. Cette dernière est réglée par un entretien, centré sur la détection de la motivation et du potentiel du candidat. Des connaissances de base en informatique sont également vérifiées (outils bureautiques, usages des services courants de l’Internet).

### Objectif

Les principes directeurs qui ont présidé à la création du Passe numérique sont les suivants :  

- Concevoir une formation adaptée aux profils des publics accueillis. Pour cela, capitaliser à partir des acquis expérientiels des personnes en formation et les faire progresser à la fois dans leur dimension sociale et professionnelle et dans la maîtrise de méthodes et d’outils intellectuels de haut niveau.
- Bâtir un diagnostic personnalisé et partagé de la situation individuelle de chaque personne en formation, et favoriser l’adhésion aux valeurs de citoyenneté, de solidarité, d’ouverture sur le monde et d’adhésion aux préceptes de la République.   
- Respecter les manières d’apprendre des personnes en formation, et fonder la pédagogie sur une tension permanente entre les savoirs construits dans l’action et les savoirs méthodologiques et théoriques. Le diplôme d’établissement est donc organisé en alternance.  
- Favoriser la prise d’initiative des personnes en formation dans la conduite de leurs apprentissages en développant le plus systématiquement une pédagogie par projets qui leur permet d’acquérir les bases dont ils ont besoin pour progresser par eux-mêmes (individuellement et en collectif) mais aussi de se confronter progressivement à des formes de pensée qui ne leur sont pas coutumières.

### Compétences

#### Compétences transverses

##### Compétence de conduite et gestion de projet

- Participer à la conduite d’un projet de l’idée à sa concrétisation en faisant preuve de créativité, innovation et prise de risques
- Comprendre une commande, l’analyser et la reformuler
- Participer à la définition des objectifs d’un projet
- Programmer et gérer un projet en vue de la réalisation d'objectifs
- Travailler en équipe
- Rechercher l’information, l’analyser et la synthétiser, la restituer, en rendre compte.

##### Compétence relationnelle et communicationnelle

- Exprimer, argumenter et interpréter des concepts, pensées, sentiments, faits et opinions oralement et par écrit
- Interagir et avoir des interactions linguistiques appropriées et créatives dans toutes les situations de la vie sociale et professionnelle.
- Compétence de communication en anglais
- Comprendre, lire et s’exprimer en anglais
- Rédiger une documentation technique en anglais
- Interagir en anglais dans un contexte professionnel et dans une relation client

##### Compétence mathématique : 

- développer et appliquer un raisonnement mathématique en vue de résoudre divers problèmes de la vie quotidienne au travail.

##### Compétences sociales: 

- Adopter les comportements et conduites adaptés pour participer de manière efficace et constructive à la vie sociale et professionnelle
- Maîtriser les codes sociaux et culturels du monde du travail,
- Adopter une ligne de conduite face à la noblesse du métier quant aux missions sensibles, en réponse à des enjeux sociétaux de sécurité nationale

##### Compétence en expression culturelles  

- prendre conscience de l'importance de l'expression créatrice d'idées, d'expériences et d'émotions sous diverses formes (musique, arts du spectacle, littérature et arts visuels).

##### Compétence méta-cognitive   

- Apprendre à apprendre
- Entreprendre et organiser soi-même un apprentissage individuellement et en groupe.
- Compétences de base en culture générale du numérique
- Connaître une histoire du numérique (émergences et convergences techniques et socio-techniques de l’informatique, des télécoms, de l’Internet, des supports médiatiques.
- Conduire des enquêtes et les restituer sur des grands thèmes du numérique : arts et médias du numérique, big data, objets connectés,
économie numérique, cybersécurité, etc.
- Conduire et animer des revues de presse sur l’actualité du numérique, traitée d’un point de vue socio-économique.
- Maîtriser les outils et méthodes de recherche d’information sur le web.

#### Compétences « cœur de métier » en techniques de maintenance informatique

##### Compétences de base en informatique

- Savoir utiliser de façon sûre et critique les technologies de l'information et communication
- Savoir mettre en œuvre les exigences techniques liées au domaine de la cybersécurité
- Savoir appliquer les bonnes pratiques de sécurité informatique pour les utilisateurs, les postes de travail et les serveurs
- Connaître les bases de la programmation (algorithmique)
- Savoir développer et appliquer un raisonnement algorithmique
- Maîtriser des savoirs de base en informatique :
- Maîtriser les logiciels spécifiques à un domaine technique

##### Compétences sociales liées à la sécurité informatique

- Intégrer les comportements d’intelligence collective afin de résoudre un problème complexe, en coopération avec d’autres équipes, leurs managers ou les gestionnaires de crises,
- Mettre en pratique des règles éthiques fortes sur l’usage « à bonne fin » des connaissances détenues, tant dans le domaine informatique que cyber.  
- Transmettre cet esprit de corps auprès de l'ensemble des promotions, des partenaires et des acteurs impliqués.

##### Compétences d’administration d’un système d’information  

- Administrer des infrastructures informatiques et le patrimoine applicatif en vue de leur maintien en condition opérationnelle
- Installer, paramétrer et configurer des ressources informatiques, postes de travail et serveurs
- Interconnecter en réseau les postes de travail, pouvoir relier un tel réseau à l'Internet en mettant en place les services Internet habituels, ainsi que les sécurités et protections souhaitées
- Réaliser le support de niveau 1 aux utilisateurs
- Réaliser le support de niveau 1 aux utilisateurs
- Changer ou réparer un élément ou un ensemble défectueux
- Réaliser les opérations de nettoyage et de réglage sur les matériels et équipements
- Mettre en place et superviser les processus de sauvegarde
- Compétences de gestion des contrôles, tests et diagnostics
- Ecrire formellement un dysfonctionnement, incident ou accident
- Analyser et comprendre l'origine d'un dysfonctionnement, incident de sécurité ou accident réseau, système ou matériel (spécifications physiques du produit, processus...)
- Guider l'utilisateur pour résoudre le dysfonctionnement ou prendre le contrôle du système à distance
- Proposer une solution permettant de résoudre le problème de sécurité dans le respect des mesures de sécurité.

##### Compétence de formation et transmission de connaissances  

- Former des collaborateurs, des clients ou des utilisateurs
- Réaliser la documentation de prise en main de logiciels ou système d’information
- Renseigner les supports de suivi d'intervention et transmettre les informations au service concerné
- Sensibiliser les utilisateurs aux bonnes pratiques en matière de sécurité : montrer l'intérêt de bonnes pratiques du poste de travail en réseau,
- énumérer les principales bonnes pratiques

##### Compétence en cybersécurité

- Comprendre et analyser une vulnérabilité, une attaque informatique
- Comprendre et mettre en place différents types de dispositifs de sécurité : sondes, bastions
- Gestion des utilisateurs et des droits sous Linux
- Définition et mise en œuvre de stratégies de mots de passe et de groupes
- Vérification des problèmes de sécurité courants sur le poste de travail : activation/désactivation de l'antivirus
- Configuration d'un firewall local
- Configuration d'un IDS

## Enseignements

### Ouverture aux cultures numériques (USOP1L)

### Méthodes et Outils de la Communication Écrite (USOP1N)

### Organisation du travail et de l'entreprise (USOP1V)

### Revue de presse socio-économique (USOP1Q)

### Projet personnel et professionnel - métiers du numérique (USOP1S)

### installation et Administration du Poste de Travail Windows et Linux (USOP3V - 4 ECTS)

#### Description

Ce module est une introduction à la maintenance des systèmes informatiques

#### Compétences

- Réaliser le branchement d'un poste de travail
- Assembler les composants internes d'un ordinateur
- Réaliser l'installation d'un système Linux
- Réaliser l'installation d'un système Windows / OsX

### Installation et Maintenance du Poste de Travail en réseau (USOP3W - 4 ECTS)

#### Description

Ce module fait découvrir la ligne de commandes pour l'administration du système

#### Compétences

- Utiliser le shell pour l'administration des systèmes UNIX
- Utiliser la console windows et windows Shell pour l'administration des systèmes Windows
- Écrire des scripts pour automatiser les tâches de maintenance

### Outils internet: installation et utilisation (USOP3X - 4 ECTS)

#### Description

Ce module permet d'apprendre à utiliser les outils nécessaires au travail en équipe

#### Compétences

- Concevoir une politique de sécurité des données
- Utiliser git pour la gestion des versions du code informatique et la gestion de tickets
- Utiliser un groupware (Nextcloud)

### Technologie informatique et réseaux (USOP3Y - 4 ECTS)

#### Description

Ce module vise à l'acquisition de bases en programmation-objet

#### Compétences

- Programmer avec les outils de base du language Python: la syntaxe, les bibliothèques, les fonctions.

### Maintenance du Poste de Travail en Réseau (USOP3Z - 4 ECTS)

#### Description 

Ce module permet d'apprendre à configurer un réseau local et des applications client-serveur

#### Compétences

- Configurer un réseau local
- Configurer un serveur: DHCP, SSH, Samba, Nginx 

### Projet de Développement Numérique (USOP45 - 4 ECTS)

#### Description

Le projet de Développement Numérique est un projet de groupe (3-4 étudiants) dans lequel il est demandé de réaliser une installation complète de parc informatique en réseau: 

- Définition du cahier des charges
- installation des postes de travail, installation d'un serveur, branchements, configuration
- Assistance aux utilisateurs

#### Compétences

- Établir les besoins et proposer une solution globale
- Adapter le niveau de technicité de son language aux utilisateurs

### Expérience professionnelle (UAOP1A)
