# Introduction

Le Garage Numérique anime des formations aux métiers de l'informatique.  
Cette rubrique propose de recenser les formations animées et leur contenu.

Vous pouvez accéder à:  

- la formation du [Passe Numérique Pro](pnpro-devops.md)
- le [Bac+1 Technicien Devops](bacplusun-devops.md)
