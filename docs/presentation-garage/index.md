# Présentation du Garage Numérique

Ces documents présentent le fonctionnement de l'association:

 - Le [réglement intérieur](reglement_interieur.md).  
[Changelog du réglement intérieur](reglement_interieur_CHANGELOG.md)  

 - Les [procédures en vigueur](procedures.md).  

 - L'[équipe](equipe.md).  

 - L'[histoire du Garage](histoire.md).  

 - Nos [projets](projets.md).  

 - Nos [statuts](statut.md).  

 - Les [ressources du Garage](ressources/sites-web.md).  

 - Les [formations DevOps](formation).  
