# Historique des changements du réglement intérieur

## v 0.0.2

| version   | commit            | message  
|-----------|-------------------|--------------|
| 0.0.2     |                   |  proposition alpha  |
|           |**changements:**   |              |
|           |                   | **Mise en place d'une proposition sur : **
|           |                   |  - Dispositions générales
|           |                   |  - Dispositions pour l'équipe
|           |                   |  - Dispositions spécial covid
|           |**à développer:**  | 
|           |                   | relecture par l'équipe

## v 0.0.1

| version   | commit        | changements  
|-----------|---------------|--------------|
| 0.0.1     | 8f8baa7ba     | **création du fichier** |
|           | **à développer**  | 
|           |                   |ébauche de réglement